-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function getClosestSnapPoint(x, y)
	if hasGrid() then
		local type = getGridType();
		local size = getGridSize();

		if type == "hexrow" or type == "hexcolumn" then
			local qw, hh = getGridHexElementDimensions();
			local ox, oy = getGridOffset();

			-- The hex grid separates into a non-square grid of elements sized qw*hh, the location in which dictates corner points
			if type == "hexcolumn" then
				local col = math.floor((x - ox) / qw);
				local row = math.floor((y - oy) * 2 / size);

				local evencol = col % 2 == 0;
				local evenrow = row % 2 == 0;

				local lx = (x - ox) % qw;
				local ly = (y - oy) % hh;

				if (evenrow and evencol) or (not evenrow and not evencol) then
					-- snap to lower right and upper left
					if lx + ly * (qw/hh) < qw then
						return ox + col*qw, oy + math.floor(row*size/2);
					else
						return ox + (col+1)*qw, oy + math.floor((row+1)*size/2);
					end
				else
					-- snap to lower left and upper right
					if (qw-lx) + ly * (qw/hh) < qw then
						return ox + (col+1)*qw, oy + math.floor(row*size/2);
					else
						return ox + col*qw, oy + math.floor((row+1)*size/2);
					end
				end
			else -- "hexrow"
				local col = math.floor((x - ox) * 2 / size);
				local row = math.floor((y - oy) / qw);

				local evencol = col % 2 == 0;
				local evenrow = row % 2 == 0;

				local lx = (x - ox) % hh;
				local ly = (y - oy) % qw;

				if (evenrow and evencol) or (not evenrow and not evencol) then
					-- snap to lower right and upper left
					if lx * (qw/hh) + ly < qw then
						return ox + math.floor(col*size/2), oy + row*qw;
					else
						return ox + math.floor((col+1)*size/2), oy + (row+1)*qw;
					end
				else
					-- snap to lower left and upper right
					if (hh-lx) * (qw/hh) + ly < qw then
						return ox + math.floor((col+1)*size/2), oy + row*qw;
					else
						return ox + math.floor(col*size/2), oy + (row+1)*qw;
					end
				end
			end
		else -- if type == "square" then
			local ox, oy = getGridOffset();
			
			local basex = math.floor((x - (ox + 1))/(size/2))*(size/2) + (ox + 1);
			local basey = math.floor((y - (oy + 1))/(size/2))*(size/2) + (oy + 1);
			
			local newx = basex;
			local newy = basey;
			
			if ((x - basex) > (size / 4)) then
				newx = newx + (size / 2);
			end
			if ((y - basey) > (size / 4)) then
				newy = newy + (size / 2);
			end
			
			return newx, newy;
		end
	end
	
	return x, y;
end

function onTokenSnap(token, x, y)
	if hasGrid() then
		return getClosestSnapPoint(x, y);
	else
		return x, y;
	end
end

function onPointerSnap(startx, starty, endx, endy, pointertype)
	local newstartx = startx;
	local newstarty = starty;
	local newendx = endx;
	local newendy = endy;

	if hasGrid() then
		newstartx, newstarty = getClosestSnapPoint(startx, starty);
		newendx, newendy = getClosestSnapPoint(endx, endy);
	end

	return newstartx, newstarty, newendx, newendy;
end

function measureVector(vx, vy, gridtype, gridsize, qw, hh)
	local dist = 0;
	
	if type == "hexrow" or type == "hexcolumn" then
		local col, row = 0, 0;
		if type == "hexcolumn" then
			col = vx / (qw*3);
			row = (vy / (hh*2)) - (col * 0.5);
		else		
			row = vy / (qw*3);
			col = (vx / (hh*2)) - (row * 0.5);
		end

		if	((row >= 0 and col >= 0) or (row < 0 and col < 0)) then
			dist = math.abs(col) + math.abs(row);
		else
			dist = math.max(math.abs(col), math.abs(row));
		end
	
	else --	if gridtype == "square" then

		-- NOTE: 3.5E Specific Measuring Code - Diagonal squares are counted as 1.5
		local gx = math.abs(vx / gridsize);
		local gy = math.abs(vy / gridsize);
		
		local diagonals = math.min(gx, gy);
		local straights = math.abs(gx - gy);
		
		dist = straights + (diagonals * 1.5);
	end
	
	return dist;
end

function onMeasureVector(token, vector)
	local dist = "";

	if hasGrid() then
		local gridtype = getGridType();
		local gridsize = getGridSize();

		dist = 0;
		if type == "hexrow" or type == "hexcolumn" then
			local qw, hh = getGridHexElementDimensions();
			for i = 1, #vector do
				local nVector = math.floor(measureVector(vector[i].x, vector[i].y, gridtype, gridsize, qw, hh));
				dist = dist + nVector;
			end
		else -- if type == "square" then
			-- NOTE: 3.5E Specific Measuring Code - Diagonal squares are counted as 1.5
			for i = 1, #vector do
				local nVector = measureVector(vector[i].x, vector[i].y, gridtype, gridsize);
				dist = dist + nVector;
			end
			dist = math.ceil(dist);
		end
	end
	
	return "" .. (dist * DataCommon.IMAGE_GRID_MEASURE_UNITS) .. "\'";
end

function onMeasurePointer(length, pointertype, startx, starty, endx, endy)
	local dist = "";

	if hasGrid() then
		local gridtype = getGridType();
		local gridsize = getGridSize();
		
		if type == "hexrow" or type == "hexcolumn" then
			local qw, hh = getGridHexElementDimensions();
			dist = measureVector(endx - startx, endy - starty, gridtype, gridsize, qw, hh);
		else -- if type == "square" then
			dist = measureVector(endx - startx, endy - starty, gridtype, gridsize);
		end

		if pointertype == "circle" or pointertype == "rectangle" then
			dist = math.floor((dist + 0.25) * 2);
		else
			dist = math.floor(dist);
		end
	end
	
	return "" .. (dist * DataCommon.IMAGE_GRID_MEASURE_UNITS) .. "\'";
end

function onMaskingStateChanged(tool)
	window.toolbar_draw.onValueChanged();
end

function onDrawStateChanged(tool)
	window.toolbar_draw.onValueChanged();
end

function onGridStateChanged(type)
	if User.isHost() then
		if type == "hex" then
			setTokenOrientationCount(12);
		else
			setTokenOrientationCount(8);
		end
	end
	
	window.updateDisplay();
end

function onInit()
	onGridStateChanged(getGridType());
end

function onDrop(x, y, draginfo)
	local dragtype = draginfo.getType();
	
	if dragtype == "combattrackerff" then
		-- Grab faction data from drag object
		local sFaction = draginfo.getStringData();

		-- Determine image viewpoint
		-- Handle zoom factor (>100% or <100%) and offset drop coordinates
		local vpx, vpy, vpz = getViewpoint();
		if vpz > 1 then
			x = x / vpz;
			y = y / vpz;
		elseif vpz < 1 then
			x = x + (x * vpz);
			y = y + (y * vpz);
		end
		
		-- If grid, then snap drop point and adjust drop spread
		local nDropSpread = 15;
		if hasGrid() then
			x, y = getClosestSnapPoint(x, y);
			nDropSpread = getGridSize();
		end

		-- Get the CT window
		local ctwnd = Interface.findWindow("combattracker_window", "combattracker");
		if ctwnd then
		    -- Loop through the CT entries
			for k,v in pairs(ctwnd.list.getWindows()) do
				-- Make sure we have the right fields to work with
				if v.token and v.friendfoe then
					-- Look for entries with the same faction
					if v.friendfoe.getStringValue() == sFaction then
						-- Get the entries token image
						local tokenproto = v.token.getPrototype();
						if tokenproto then
						    -- Add it to the image at the drop coordinates 
							local addedtoken = addToken(tokenproto, x, y);

							-- Update the CT entry's token references
							v.token.replace(addedtoken);

							-- Offset drop coordinates for next token = nice spread :)
							if x >= (nDropSpread * 1.5) then
								x = x - nDropSpread;
							end
							if y >= (nDropSpread * 1.5) then
								y = y - nDropSpread;
							end
						end
					end
				end
			end
		end
		
		return true;
	end
end
