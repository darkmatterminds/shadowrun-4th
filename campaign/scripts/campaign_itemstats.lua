-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	updateDisplay();
end

function updateDisplay()
	local bID = isidentified.getState();

	line_nonid.setVisible(not bID);
	
	nonidentified.setVisible(not bID);
	label_nonid.setVisible(not bID);
	
	if User.isHost() then
		bID = true;
	end
	
	line_id.setVisible(bID);
	
	cost.setVisible(bID);
	label_cost.setVisible(bID);
	
	local sType = type.getValue();
	local bWeapon = (sType == "Weapon");
	local bArmor = (sType == "Armor");
		
	line_aw.setVisible(bID and (bWeapon or bArmor));

	damage.setVisible(bID and bWeapon);
	label_dmg.setVisible(bID and bWeapon);
	damagetype.setVisible(bID and bWeapon);
	label_dmgtype.setVisible(bID and bWeapon);
	critical.setVisible(bID and bWeapon);
	label_crit.setVisible(bID and bWeapon);
	range.setVisible(bID and bWeapon);
	label_range.setVisible(bID and bWeapon);

	ac.setVisible(bID and bArmor);
	label_ac.setVisible(bID and bArmor);
	maxstatbonus.setVisible(bID and bArmor);
	label_msb.setVisible(bID and bArmor);
	checkpenalty.setVisible(bID and bArmor);
	label_cp.setVisible(bID and bArmor);
	spellfailure.setVisible(bID and bArmor);
	label_sf.setVisible(bID and bArmor);
	speed30.setVisible(bID and bArmor);
	label_spd30.setVisible(bID and bArmor);
	speed20.setVisible(bID and bArmor);
	label_spd20.setVisible(bID and bArmor);

	properties.setVisible(bID and (bWeapon or bArmor));
	label_prop.setVisible(bID and (bWeapon or bArmor));

	line_magic.setVisible(bID);

	bonus.setVisible(bID and (bWeapon or bArmor));
	label_bonus.setVisible(bID and (bWeapon or bArmor));
	aura.setVisible(bID);
	label_aura.setVisible(bID);
	cl.setVisible(bID);
	label_cl.setVisible(bID);
	prerequisites.setVisible(bID);
	label_prereq.setVisible(bID);
end
