-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

local sSort = "name";

function onSortCompare(w1, w2)
	if w1[sSort] and w2[sSort] then
		local sValue1 = string.lower(w1[sSort].getValue());
		local sValue2 = string.lower(w2[sSort].getValue());
		if sValue1 ~= sValue2 then
			return sValue1 > sValue2;
		end
	end

	return w1.getDatabaseNode().getName() > w2.getDatabaseNode().getName();
end

function addEntry(bFocus)
	local win = NodeManager.createWindow(self);
	if bFocus and win then
		win.count.setFocus();
	end
	return win;
end

function onDrop(x, y, draginfo)
	if draginfo.isType("shortcut") then
		local class, datasource = draginfo.getShortcutData();
		local source = draginfo.getDatabaseNode();

		if source then
			if class == "npc" then
				local win = addEntry(true);
				if win then
					win.name.setValue(NodeManager.get(source, "name", ""));
					win.link.setValue("npc", source.getNodeName());

					local tokenval = NodeManager.get(source, "token", nil);
					if tokenval then
						win.token.setPrototype(tokenval);
					end
				end
			end
		end

		return true;
	end
end
