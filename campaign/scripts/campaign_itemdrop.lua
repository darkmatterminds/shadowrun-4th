-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onDrop(x, y, draginfo)
	local sDragType = draginfo.getType();
	if sDragType ~= "shortcut" then
		return false;
	end
	
	local sDropClass, sDropNodeName = draginfo.getShortcutData();
	if sDropClass ~= "item" then
		return true;
	end
	
	local nodeSource = draginfo.getDatabaseNode();
	local nodeTarget = window.getDatabaseNode();
		
	local sSourceType = NodeManager.get(nodeSource, "type", "");
	local sTargetType = NodeManager.get(nodeTarget, "type", "");

	local sSourceName = NodeManager.get(nodeSource, "name", "");
	sSourceName = string.gsub(sSourceName, " %(" .. sSourceType .. "%)", "");
	local sTargetName = NodeManager.get(nodeTarget, "name", "");
	sTargetName = string.gsub(sTargetName, " %(" .. sTargetType .. "%)", "");
	
	if sSourceType == "Shield" then
		sSourceType = "Armor";
	end
	if sTargetType == "Shield" then
		sTargetType = "Armor";
	end

	if sSourceType == sTargetType and StringManager.contains({ "Weapon", "Armor" }, sSourceType) then
		local sSourceAura = NodeManager.get(nodeSource, "aura", "");
		local sTargetAura = NodeManager.get(nodeTarget, "aura", "");
		
		if ((sSourceAura == "") and (sTargetAura ~= "")) then
			if sSourceName ~= "" then
				local sName = sSourceName .. " (" .. NodeManager.get(nodeTarget, "name", "") .. ")";
				NodeManager.set(nodeTarget, "name", "string", sName);
			end
			
			local sSourceCost = NodeManager.get(nodeSource, "cost", "");
			if sSourceCost ~= "" then
				local sCost = sSourceCost .. " (" .. NodeManager.get(nodeTarget, "cost", "") .. ")";
				NodeManager.set(nodeTarget, "cost", "string", sCost);
			end
			
			local nSourceBonus = NodeManager.get(nodeSource, "bonus", 0);
			local nTargetBonus = NodeManager.get(nodeTarget, "bonus", 0);
			NodeManager.set(nodeTarget, "bonus", "number", nSourceBonus + nTargetBonus);
			
			if sSourceType == "Weapon" then
				NodeManager.set(nodeTarget, "subtype", "string", NodeManager.get(nodeSource, "subtype", ""));
				NodeManager.set(nodeTarget, "weight", "number", NodeManager.get(nodeSource, "weight", 0));
				NodeManager.set(nodeTarget, "damage", "string", NodeManager.get(nodeSource, "damage", ""));
				NodeManager.set(nodeTarget, "damagetype", "string", NodeManager.get(nodeSource, "damagetype", ""));
				NodeManager.set(nodeTarget, "critical", "string", NodeManager.get(nodeSource, "critical", ""));
				NodeManager.set(nodeTarget, "range", "number", NodeManager.get(nodeSource, "range", 0));
				NodeManager.set(nodeTarget, "properties", "string", NodeManager.get(nodeSource, "properties", ""));
			elseif sSourceType == "Armor" then
				NodeManager.set(nodeTarget, "subtype", "string", NodeManager.get(nodeSource, "subtype", ""));
				NodeManager.set(nodeTarget, "weight", "number", NodeManager.get(nodeSource, "weight", 0));
				NodeManager.set(nodeTarget, "ac", "number", NodeManager.get(nodeSource, "ac", 0));
				NodeManager.set(nodeTarget, "maxstatbonus", "number", NodeManager.get(nodeSource, "maxstatbonus", 0));
				NodeManager.set(nodeTarget, "checkpenalty", "number", NodeManager.get(nodeSource, "checkpenalty", 0));
				NodeManager.set(nodeTarget, "spellfailure", "number", NodeManager.get(nodeSource, "spellfailure", 0));
				NodeManager.set(nodeTarget, "speed30", "number", NodeManager.get(nodeSource, "speed30", 0));
				NodeManager.set(nodeTarget, "speed20", "number", NodeManager.get(nodeSource, "speed20", 0));
				NodeManager.set(nodeTarget, "properties", "string", NodeManager.get(nodeSource, "properties", ""));
			end
		elseif ((sSourceAura ~= "") and (sTargetAura == "")) then
			local sName = "";
			if sTargetName ~= "" then
				sName = sTargetName .. " (" .. sSourceName .. ")";
			else
				sName = sSourceName;
			end
			NodeManager.set(nodeTarget, "name", "string", sName);
			
			local sSourceCost = NodeManager.get(nodeSource, "cost", "");
			local sTargetCost = NodeManager.get(nodeTarget, "cost", "");
			local sCost = "";
			if sCost ~= "" then
				sCost = sTargetCost .. " (" .. sSourceCost .. ")";
			else
				sCost = sSourceCost;
			end
			NodeManager.set(nodeTarget, "cost", "string", sCost);

			local nSourceBonus = NodeManager.get(nodeSource, "bonus", 0);
			local nTargetBonus = NodeManager.get(nodeTarget, "bonus", 0);
			NodeManager.set(nodeTarget, "bonus", "number", nSourceBonus + nTargetBonus);
			
			NodeManager.set(nodeTarget, "aura", "string", NodeManager.get(nodeSource, "aura", ""));
			NodeManager.set(nodeTarget, "cl", "string", NodeManager.get(nodeSource, "cl", ""));
			NodeManager.set(nodeTarget, "prerequisites", "string", NodeManager.get(nodeSource, "prerequisites", ""));
			
			if nodeSource and nodeTarget then
				local nodeSourceDesc = nodeSource.getChild("description");
				if nodeSourceDesc then
					local nodeTargetDesc = nodeTarget.createChild("description");
					if nodeTargetDesc then
						DB.copyNode(nodeSourceDesc, nodeTargetDesc);
					end
				end
			end
		end
	end

	return true;
end
