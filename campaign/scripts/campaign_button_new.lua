-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	if gmonly and not User.isHost() then
		setVisible(false);
	end
end

function onButtonPress()
	if User.isHost() then
		local node = NodeManager.createChild(window.getDatabaseNode());
		if node then
			local wnd = Interface.openWindow(class[1], node.getNodeName());
			if wnd and wnd.name then
				wnd.name.setFocus();
			end
		end
	elseif not gmonly then
		local nodeWin = window.getDatabaseNode();
		if nodeWin then
			Interface.requestNewClientWindow(class[1], nodeWin.getNodeName());
		end
	end
end
