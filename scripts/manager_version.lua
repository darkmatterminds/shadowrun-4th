-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	if User.isHost() then
		local nodeRoot = DB.findNode(".");
		if nodeRoot then
			local major, minor = nodeRoot.getRulesetVersion();

			if major < 8 then
				convertCT(nodeRoot);
				convertCampaignData(nodeRoot);
			end

			nodeRoot.updateVersion();
		end
	end
end

function convertCT(nodeRoot)
	local nodeCT = nodeRoot.getChild("combattracker");
	if not nodeCT then
		return;
	end
	
	-- REMOVE OLD GROUPS (3.5E)
	local aCTChildren = nodeCT.getChildren();
	for kGroup, nodeGroup in pairs(aCTChildren) do
		-- EXISTENCE OF ENTRIES LIST INDICATES OLD 3.5E GROUP FORMAT
		local nodeEntries = nodeGroup.getChild("entries");
		if nodeEntries then
			for kEntry, nodeEntry in pairs(nodeEntries.getChildren()) do
				local nodeNewEntry = nodeCT.createChild();
				if nodeNewEntry then
					DB.copyNode(nodeEntry, nodeNewEntry);
				end
			end
			
			nodeGroup.delete();
		end
	end
	
	-- TRANSLATE INDIVIDUAL ENTRIES
	for kEntry, nodeEntry in pairs(nodeCT.getChildren()) do
		-- TYPE (3.5E)
		local nodeType = nodeEntry.getChild("type");
		if not nodeType then
			local sType = "npc";
			
			local nodeLink = nodeEntry.getChild("link");
			if nodeLink then
				local sClass, sRecord = nodeLink.getValue();
				if sRecord and string.match(sRecord, "^charsheet%.") then
					sType = "pc";
				end
			end
			
			NodeManager.set(nodeEntry, "type", "string", sType);
			
			if sType == "npc" then
				NodeManager.set(nodeEntry, "show_npc", "number", 1);
			end
		end
		
		-- AC (BOTH)
		local nodeAC = nodeEntry.getChild("ac");
		if nodeAC then
			local sAC = nodeAC.getValue();
			NodeManager.set(nodeEntry, "ac_final", "number", tonumber(string.match(sAC, "^(%d+)")) or 10);
			NodeManager.set(nodeEntry, "ac_touch", "number", tonumber(string.match(sAC, "touch (%d+)")) or 10);
			local sFlatFooted = string.match(sAC, "flat%-footed (%d+)");
			if not sFlatFooted then
				sFlatFooted = string.match(sAC, "flatfooted (%d+)");
			end
			NodeManager.set(nodeEntry, "ac_flatfooted", "number", tonumber(sFlatFooted) or 10);
			
			nodeAC.delete();
		end
		
		-- ATTACK / FULL ATTACK (BOTH)
		local nodeAttacks = nodeEntry.createChild("attacks");
		if nodeAttacks then
			local nodeAtk = nodeEntry.getChild("atk");
			if nodeAtk then
				local nodeNewAtk = nodeAttacks.createChild();
				if nodeNewAtk then
					NodeManager.set(nodeNewAtk, "value", "string", nodeAtk.getValue());
					
					nodeAtk.delete();
				end
			end
			local nodeFullAtk = nodeEntry.getChild("fullatk");
			if nodeFullAtk then
				local nodeNewAtk = nodeAttacks.createChild();
				if nodeNewAtk then
					NodeManager.set(nodeNewAtk, "value", "string", nodeFullAtk.getValue());
					
					nodeFullAtk.delete();
				end
			end
		end
		
		-- TOKEN (3.5E)
		local nodeOldTokenRefId = nodeEntry.getChild("token_refid");
		if nodeOldTokenRefId then
			NodeManager.set(nodeEntry, "tokenrefid", "string", nodeOldTokenRefId.getValue());
			nodeOldTokenRefId.delete();
		end
		local nodeOldTokenRefNode = nodeEntry.getChild("token_refnode");
		if nodeOldTokenRefNode then
			NodeManager.set(nodeEntry, "tokenrefnode", "string", nodeOldTokenRefNode.getValue());
			nodeOldTokenRefNode.delete();
		end
		local nodeOldTokenRefScale = nodeEntry.getChild("token_scale");
		if nodeOldTokenRefScale then
			NodeManager.set(nodeEntry, "tokenscale", "number", nodeOldTokenRefScale.getValue());
			nodeOldTokenRefScale.delete();
		end
	end
end

function convertCampaignData(nodeRoot)
	local nodeCombat = nodeRoot.getChild("combat");
	local nodeBattle = nodeRoot.createChild("battle");
	if not nodeCombat or not nodeBattle then
		return;
	end
	
	local aCombatChildren = nodeCombat.getChildren();
	for kOldEnc, nodeOldEnc in pairs(aCombatChildren) do
		local nodeNewEnc = nodeBattle.createChild();
		if nodeNewEnc then
			NodeManager.set(nodeNewEnc, "name", "string", NodeManager.get(nodeOldEnc, "name", ""));
		
			local nodeOldActors = nodeOldEnc.getChild("actors");
			local nodeNewActors = nodeNewEnc.createChild("npclist");
			if nodeOldActors and nodeNewActors then
				local aActorChildren = nodeOldActors.getChildren();
				for kOldActor, nodeOldActor in pairs(aActorChildren) do
					local nodeNewActor = nodeNewActors.createChild();
					if nodeNewActor then
						NodeManager.set(nodeNewActor, "name", "string", NodeManager.get(nodeOldActor, "name", ""));
						NodeManager.set(nodeNewActor, "token", "token", NodeManager.get(nodeOldActor, "token", ""));
					end
				end
			end
		end
	end
	
	nodeCombat.delete();
end