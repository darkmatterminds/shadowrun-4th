-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

OOB_MSGTYPE_APPLYSAVE = "applysave";

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_APPLYSAVE, handleApplySave);

	ActionsManager.registerMultiHandler("spellcast", translateSpellCast);
	
	ActionsManager.registerTargetingHandler("spellcast", onCastTargeting);
	ActionsManager.registerTargetingHandler("clc", onTargeting);
	ActionsManager.registerTargetingHandler("spellsave", onTargeting);
	
	ActionsManager.registerModHandler("castattack", modCastAttack);
	ActionsManager.registerModHandler("clc", modCLC);
	ActionsManager.registerModHandler("save", modSave);
	
	ActionsManager.registerResultHandler("cast", onSpellCast);
	ActionsManager.registerResultHandler("castattack", onCastAttack);
	ActionsManager.registerResultHandler("castclc", onCastCLC);
	ActionsManager.registerResultHandler("castsave", onCastSave);
	ActionsManager.registerResultHandler("spellclc", onSpellCLC);
	ActionsManager.registerResultHandler("clc", onCLC);
	ActionsManager.registerResultHandler("spellsave", onSpellSave);
	ActionsManager.registerResultHandler("save", onSave);
end

function handleApplySave(msgOOB)
	-- GET THE TARGET ACTOR
	local rSource = ActorManager.getActor("ct", msgOOB.sSourceCTNode);
	if not rSource then
		rSource = ActorManager.getActor(msgOOB.sSourceType, msgOOB.sSourceCreatureNode);
	end
	local rTarget = ActorManager.getActor("ct", msgOOB.sTargetCTNode);
	if not rTarget then
		rTarget = ActorManager.getActor(msgOOB.sTargetType, msgOOB.sTargetCreatureNode);
	end
	
	local sSaveShort, sSaveDC = string.match(msgOOB.sDesc, "%[(%w+) DC (%d+)%]")
	if sSaveShort then
		local sSave = DataCommon.save_stol[sSaveShort];
		if sSave then
			local isGMOnly = string.match(msgOOB.sDesc, "^%[GM%]");
			local bRemoveOnMiss = string.match(msgOOB.sDesc, "%[MULTI%]");
	
			performSaveRoll(nil, rTarget, sSave, isGMOnly, sSaveDC, rSource, bRemoveOnMiss);
		end
	end
end

function onCastTargeting(rSource, rRolls)
	local aTargets = TargetingManager.getFullTargets(rSource);
	
	if #aTargets > 1 and OptionsManager.isOption("RMMT", "multi") then
		for i = 2, 4 do
			if rRolls[i] then
				rRolls[i].sDesc = rRolls[i].sDesc .. " [MULTI]";
			end
		end
	end
	
	return aTargets, false;
end

function onTargeting(rSource, rRolls)
	local aTargets = TargetingManager.getFullTargets(rSource);
	
	if #aTargets > 1 and OptionsManager.isOption("RMMT", "multi") then
		for kRoll, vRoll in ipairs(rRolls) do
			vRoll.sDesc = vRoll.sDesc .. " [MULTI]";
		end
	end
	
	return aTargets, false;
end

function translateSpellCast(nSlot)
	local sType = "";
	
	if nSlot == 1 then
		sType = "cast";
	elseif nSlot == 2 then
		sType = "castattack";
	elseif nSlot == 3 then
		sType = "castclc";
	elseif nSlot == 4 then
		sType = "castsave";
	end
	
	return sType;
end

function getSpellCastRoll(rActor, rAction)
	local rRoll = {};

	-- Build the basic roll
	rRoll.aDice = {};
	rRoll.nValue = 0;
	
	-- Build the description
	rRoll.sDesc = "[CAST";
	if rAction.order and rAction.order > 1 then
		rRoll.sDesc = rRoll.sDesc .. " #" .. rAction.order;
	end
	rRoll.sDesc = rRoll.sDesc .. "] " .. rAction.label;
	
	return rRoll;
end

function getCLCRoll(rActor, rAction)
	local rRoll = {};

	-- Build the basic roll
	rRoll.aDice = { "d20" };
	rRoll.nValue = rAction.clc or 0;
	
	-- Build the description
	rRoll.sDesc = "[CL CHECK";
	if rAction.order and rAction.order > 1 then
		rRoll.sDesc = rRoll.sDesc .. " #" .. rAction.order;
	end
	rRoll.sDesc = rRoll.sDesc .. "] " .. rAction.label;
	if rAction.sr == "no" then
		rRoll.sDesc = rRoll.sDesc .. " [SR NOT ALLOWED]";
	end
	
	return rRoll;
end

function getSaveVsRoll(rActor, rAction)
	local rRoll = {};

	-- Build the basic roll
	rRoll.aDice = {};
	rRoll.nValue = rAction.savemod;
	
	-- Build the description
	rRoll.sDesc = "[SAVE VS";
	if rAction.order and rAction.order > 1 then
		rRoll.sDesc = rRoll.sDesc .. " #" .. rAction.order;
	end
	rRoll.sDesc = rRoll.sDesc .. "] " .. rAction.label;
	if rAction.save == "fortitude" then
		rRoll.sDesc = rRoll.sDesc .. " [FORT DC " .. rAction.savemod .. "]";
	elseif rAction.save == "reflex" then
		rRoll.sDesc = rRoll.sDesc .. " [REF DC " .. rAction.savemod .. "]";
	elseif rAction.save == "will" then
		rRoll.sDesc = rRoll.sDesc .. " [WILL DC " .. rAction.savemod .. "]";
	end

	return rRoll;
end

function performSaveRoll(draginfo, rActor, sSave, bSecretRoll, sSaveDC, rSource, bRemoveOnMiss)
	local bOverride = false;
	local rRoll = {};
	
	-- Build the basic roll
	rRoll.aDice = { "d20" };
	rRoll.nValue = 0;
	
	-- Look up actor specific information
	local sAttribute = nil;
	if rActor then
		if rActor.sType == "pc" then
			rRoll.nValue = NodeManager.get(rActor.nodeCreature, "saves." .. sSave .. ".total", 0);
			sAttribute = NodeManager.get(rActor.nodeCreature, "saves." .. sSave .. ".base.attribute", "");
		elseif rActor.sType == "ct" or rActor.sType == "npc" then
			if rActor.nodeCT then
				rRoll.nValue = NodeManager.get(rActor.nodeCT, sSave .. "save", 0);
			else
				rRoll.nValue = NodeManager.get(rActor.nodeCreature, sSave .. "save", 0);
			end
		end
	end
	
	-- Build the description
	rRoll.sDesc = "[SAVE] " .. string.upper(string.sub(sSave, 1, 1)) .. string.sub(sSave, 2);
	if bSecretRoll then
		rRoll.sDesc = "[GM] " .. rRoll.sDesc;
	end
	if sAttribute and sAttribute ~= "" then
		if (sSave == "fortitude" and sAttribute ~= "constitution") or
				(sSave == "reflex" and sAttribute ~= "dexterity") or
				(sSave == "will" and sAttribute ~= "wisdom") then
			local sAttributeEffect = DataCommon.attribute_ltos[sAttribute];
			if sAttributeEffect then
				rRoll.sDesc = rRoll.sDesc .. " [MOD:" .. sAttributeEffect .. "]";
			end
		end
	end
	if sSaveDC then
		rRoll.sDesc = rRoll.sDesc .. " [VS DC " .. sSaveDC .. "]";
		bOverride = true;
	end
	if bRemoveOnMiss then
		rRoll.sDesc = rRoll.sDesc .. " [RM]";
	end

	-- Make the roll
	ActionsManager.performSingleRollAction(draginfo, rActor, "save", rRoll, bOverride, rSource);
end

function modCastAttack(rSource, rTarget, rRoll)
	ActionAttack.modAttack(rSource, rTarget, rRoll);
end

function modCLC(rSource, rTarget, rRoll)
	if rTarget and rTarget.nOrder then
		return;
	end
	
	local aAddDesc = {};
	local aAddDice = {};
	local nAddMod = 0;
	
	if rSource then
		local bEffects = false;

		-- HANDLE NEGATIVE LEVELS
		local nNegLevelMod, nNegLevelCount = EffectsManager.getEffectsBonus(rSource.nodeCT, {"NLVL"}, true);
		if nNegLevelCount > 0 then
			bEffects = true;
			nAddMod = nAddMod - nNegLevelMod;
		end

		-- IF EFFECTS HAPPENED, THEN ADD NOTE
		if bEffects then
			local sEffects = "";
			local sMod = StringManager.convertDiceToString(aAddDice, nAddMod, true);
			if sMod ~= "" then
				sEffects = "[EFFECTS " .. sMod .. "]";
			else
				sEffects = "[EFFECTS]";
			end
			table.insert(aAddDesc, sEffects);
		end
	end
	
	if #aAddDesc > 0 then
		rRoll.sDesc = rRoll.sDesc .. " " .. table.concat(aAddDesc, " ");
	end
	for k, v in ipairs(aAddDice) do
		table.insert(rRoll.aDice, v);
	end
	rRoll.nValue = rRoll.nValue + nAddMod;
end

function modSave(rSource, rTarget, rRoll)
	if rTarget and rTarget.nOrder then
		return;
	end

	local aAddDesc = {};
	local aAddDice = {};
	local nAddMod = 0;
	
	if rSource then
		local bEffects = false;

		-- DETERMINE STAT IF ANY
		local sSave = nil;
		local sSaveMatch = string.match(rRoll.sDesc, "%[SAVE%] ([^[]+)");
		if sSaveMatch then
			sSave = string.lower(StringManager.trimString(sSaveMatch));
		end
		local sActionStat = nil;
		local sModStat = string.match(rRoll.sDesc, "%[MOD:(%w+)%]");
		if sModStat then
			sActionStat = DataCommon.attribute_stol[sModStat];
		end
		if not sActionStat then
			if sSave == "fortitude" then
				sActionStat = "constitution";
			elseif sSave == "reflex" then
				sActionStat = "dexterity";
			elseif sSave == "will" then
				sActionStat = "wisdom";
			end
		end
		
		local aSaveFilter = {};
		if sSave then
			table.insert(aSaveFilter, sSave);
		end
		
		-- DETERMINE EFFECTS
		local nEffectCount;
		aAddDice, nAddMod, nEffectCount = EffectsManager.getEffectsBonus(rSource.nodeCT, {"SAVE"}, false, aSaveFilter, rTarget);
		if (nEffectCount > 0) then
			bEffects = true;
		end

		-- GET CONDITION MODIFIERS
		if EffectsManager.hasEffectCondition(rSource, "Frightened") or 
				EffectsManager.hasEffectCondition(rSource, "Panicked") or
				EffectsManager.hasEffectCondition(rSource, "Shaken") then
			nAddMod = nAddMod - 2;
			bEffects = true;
		end
		if EffectsManager.hasEffectCondition(rSource, "Sickened") then
			nAddMod = nAddMod - 2;
			bEffects = true;
		end
		if sSave == "reflex" and EffectsManager.hasEffectCondition(rSource, "Slowed") then
			nAddMod = nAddMod - 1;
			bEffects = true;
		end

		-- GET STAT MODIFIERS
		local nBonusStat, nBonusEffects = ActorManager.getattributeEffectsBonus(rSource, sActionStat);
		if nBonusEffects > 0 then
			bEffects = true;
			nAddMod = nAddMod + nBonusStat;
		end
		
		-- HANDLE NEGATIVE LEVELS
		local nNegLevelMod, nNegLevelCount = EffectsManager.getEffectsBonus(rSource.nodeCT, {"NLVL"}, true);
		if nNegLevelCount > 0 then
			bEffects = true;
			nAddMod = nAddMod - nNegLevelMod;
		end

		-- IF EFFECTS HAPPENED, THEN ADD NOTE
		if bEffects then
			local sEffects = "";
			local sMod = StringManager.convertDiceToString(aAddDice, nAddMod, true);
			if sMod ~= "" then
				sEffects = "[EFFECTS " .. sMod .. "]";
			else
				sEffects = "[EFFECTS]";
			end
			table.insert(aAddDesc, sEffects);
		end
	end
	
	if #aAddDesc > 0 then
		rRoll.sDesc = rRoll.sDesc .. " " .. table.concat(aAddDesc, " ");
	end
	for k, v in ipairs(aAddDice) do
		table.insert(rRoll.aDice, v);
	end
	rRoll.nValue = rRoll.nValue + nAddMod;
end

function onSpellCast(rSource, rTarget, rRoll)
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);

	if rTarget then
		rMessage.text = rMessage.text .. " [at " .. rTarget.sName .. "]";
	end
	
	Comm.deliverChatMessage(rMessage);
end

function onCastAttack(rSource, rTarget, rRoll)
	if (#(rRoll.aDice) == 0) and (rRoll.nValue == 0) then
		return;
	end
	
	ActionAttack.onAttack(rSource, rTarget, rRoll);
end

function onCastCLC(rSource, rTarget, rRoll)
	if rTarget then
		local nSR = ActorManager.getSpellDefense(rTarget);
		if nSR > 0 then
			if not string.match(rRoll.sDesc, "%[SR NOT ALLOWED%]") then
				local rRoll = { sType = "clc", sDesc = rRoll.sDesc, aDice = {"d20"}, nValue = rRoll.nValue };
				ActionsManager.roll(rSource, rTarget, rRoll);
				return true;
			end
		end
	end
	
	return false;
end

function onCastSave(rSource, rTarget, rRoll)
	if rTarget then
		local sSaveShort, sSaveDC = string.match(rRoll.sDesc, "%[(%w+) DC (%d+)%]")
		if sSaveShort then
			local sSave = DataCommon.save_stol[sSaveShort];
			if sSave then
				local msgOOB = {};
				msgOOB.type = OOB_MSGTYPE_APPLYSAVE;
				
				msgOOB.sDesc = rRoll.sDesc;
				if rSource then
					msgOOB.sSourceType = rSource.sType;
					msgOOB.sSourceCreatureNode = rSource.sCreatureNode;
					msgOOB.sSourceCTNode = rSource.sCTNode;
				end
				msgOOB.sTargetType = rTarget.sType;
				msgOOB.sTargetCreatureNode = rTarget.sCreatureNode;
				msgOOB.sTargetCTNode = rTarget.sCTNode;

				if not User.isHost() and rTarget.sType == "pc" and rTarget.nodeCreature and rTarget.nodeCreature.isOwner() then
					CombatCommon.handleApplySave(msgOOB);
				else
					Comm.deliverOOBMessage(msgOOB, "");
				end

				return true;
			end
		end
	end

	return false;
end

function onSpellCLC(rSource, rTarget, rRoll)
	if onCastCLC(rSource, rTarget, rRoll) then
		return;
	end
	
	local rRoll = { sType = "clc", sDesc = rRoll.sDesc, aDice = {"d20"}, nValue = rRoll.nValue };
	ActionsManager.roll(rSource, rTarget, rRoll);
end

function onCLC(rSource, rTarget, rRoll)
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);

	local nTotal = ActionsManager.total(rRoll);
	local bSRAllowed = not string.match(rRoll.sDesc, "%[SR NOT ALLOWED%]");
	
	if rTarget then
		rMessage.text = rMessage.text .. " [at " .. rTarget.sName .. "]";
		
		if bSRAllowed then
			local nSR = ActorManager.getSpellDefense(rTarget);
			if nSR > 0 then
				if nTotal >= nSR then
					rMessage.text = rMessage.text .. " [SUCCESS]";
				else
					rMessage.text = rMessage.text .. " [FAILURE]";
					if rSource then
						local bRemoveTarget = false;
						if OptionsManager.isOption("RMMT", "on") then
							bRemoveTarget = true;
						elseif OptionsManager.isOption("RMMT", "multi") then
							if (string.match(rRoll.sDesc, "%[MULTI%]")) then
								bRemoveTarget = true;
							end
						end
						
						if bRemoveTarget then
							TargetingManager.removeTargetEx(rSource.nodeCT, rTarget.sCTNode);
						end
					end
				end
			else
				rMessage.text = rMessage.text .. " [TARGET HAS NO SR]";
			end
		end
	end
	
	Comm.deliverChatMessage(rMessage);
end

function onSpellSave(rSource, rTarget, rRoll)
	if onCastSave(rSource, rTarget, rRoll) then
		return;
	end

	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	Comm.deliverChatMessage(rMessage);
end

function onSave(rSource, rTarget, rRoll)
	local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
	
	local sSaveDC = string.match(rRoll.sDesc, "%[VS DC (%d+)%]");
	local nSaveDC = tonumber(sSaveDC) or 0;
	if nSaveDC > 0 then
		local nTotal = ActionsManager.total(rRoll);
		if nTotal >= nSaveDC then
			rMessage.text = rMessage.text .. " [SUCCESS]";

			if rSource and rTarget then
				local bRemoveTarget = false;
				if string.match(rRoll.sDesc, "%[RM%]") then
					bRemoveTarget = true;
				end
				
				if bRemoveTarget then
					TargetingManager.removeTargetEx(rTarget.nodeCT, rSource.sCTNode);
				end
			end
		else
			rMessage.text = rMessage.text .. " [FAILURE]";
		end
	end
	
	Comm.deliverChatMessage(rMessage);
end
