-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

local sets = {};
local options = {};
local callbacks = {};

function isMouseWheelEditEnabled()
	return isOption("MWHL", "on") or Input.isControlPressed();
end

function onInit()
	registerOption("DCLK", true, "Client", "Mouse: Double click action", "option_entry_radio", 
			{ labels = "Roll|Mod|Off", values = "on|mod|off", optionwidth = 70, default = "on" });
	registerOption("DRGR", true, "Client", "Mouse: Drag rolling", "option_entry_radio", 
			{ labels = "On|Off", values = "on|off", optionwidth = 70, default = "on" });
	registerOption("MWHL", true, "Client", "Mouse: Wheel editing", "option_entry_radio", 
			{ labels = "Always|Ctrl", values = "on|ctrl", optionwidth = 70, default = "ctrl" });
	
	registerOption("CTAV", false, "Game (GM)", "Chat: Set GM voice to active CT", "option_entry_radio", 
			{ labels = "On|Off", values = "on|off", optionwidth = 70, default = "off" });
	registerOption("SHPW", false, "Game (GM)", "Chat: Show all whispers to GM", "option_entry_radio", 
			{ labels = "On|Off", values = "on|off", optionwidth = 70, default = "off" });
	registerOption("REVL", false, "Game (GM)", "Chat: Show GM/NPC actions", "option_entry_radio", 
			{ labels = "On|Rolls|Off", values = "on|rolls|off", optionwidth = 70, default = "off" });
	registerOption("SHRL", false, "Game (GM)", "Chat: Show roll actor", "option_entry_radio", 
			{ labels = "All|PC|Off", values = "all|pc|off", optionwidth = 70, default = "off" });
	registerOption("PCHT", false, "Game (GM)", "Chat: Show portraits", "option_entry_radio", 
			{ labels = "On|Off", values = "on|off", optionwidth = 70, default = "off" });
	registerOption("TOTL", false, "Game (GM)", "Chat: Show roll totals", "option_entry_radio", 
			{ labels = "On|Off", values = "on|off", optionwidth = 70, default = "off" });
	registerOption("TBOX", false, "Game (GM)", "Table: Dice tower", "option_entry_radio", 
			{ labels = "On|Off", values = "on|off", optionwidth = 70, default = "off" });
	
	registerOption("INIT", false, "Combat (GM)", "Add: Auto NPC initiatives", "option_entry_radio", 
			{ labels = "On|Group|Off", values = "on|group|off", optionwidth = 70, default = "off" });
	registerOption("NNPC", false, "Combat (GM)", "Add: Auto NPC numbering", "option_entry_radio", 
			{ labels = "Append|Random|Off", values = "append|random|off", optionwidth = 70, default = "append" });
	registerOption("ANPC", false, "Combat (GM)", "Chat: Anonymize NPC attacks", "option_entry_radio", 
			{ labels = "On|Off", values = "on|off", optionwidth = 70, default = "off" });
	registerOption("RING", false, "Combat (GM)", "Turn: Ring bell on PC turn", "option_entry_radio", 
			{ labels = "On|Off", values = "on|off", optionwidth = 70, default = "off" });
	registerOption("RSHE", false, "Combat (GM)", "Turn: Show effects", "option_entry_radio", 
			{ labels = "On|Off", values = "on|off", optionwidth = 70, default = "off" });
	registerOption("RNDS", false, "Combat (GM)", "Turn: Stop at round start", "option_entry_radio", 
			{ labels = "On|Off", values = "on|off", optionwidth = 70, default = "off" });
	registerOption("SHHD", false, "Combat (GM)", "View: Show health to clients", "option_entry_radio", 
			{ labels = "All|PC|Off", values = "all|pc|off", optionwidth = 70, default = "pc" });

	registerOption("SHGL", false, "Shadowrun (GM)", "Glitch: Houserule (Successes+1)", "option_entry_radio", 
			{ labels = "On|Off", values = "on|off", optionwidth = 70, default = "off" });
	registerOption("SHIN", false, "Shadowrun (GM)", "New Initiative Rolls every RD", "option_entry_radio", 
			{ labels = "On|Off", values = "on|off", optionwidth = 70, default = "on" });
	registerOption("SHIR", true, "Shadowrun (Client)", "New Initiative automatic Roll", "option_entry_radio", 
			{ labels = "On|Off", values = "on|off", optionwidth = 70, default = "off" });

		registerOption("LOCL", false, "Localization", "Language", "option_entry_radio", 
			{ labels = "Deutsch|English", values = "deutsch|english", optionwidth = 70, default = "english" });
end

function populate(win)
	for keySet, rSet in pairs(sets) do
		local winSet = win.grouplist.createWindow();
		if winSet then
			winSet.label.setValue(keySet);
			
			for keyOption, rOption in pairs(rSet) do
				local winOption = winSet.options_list.createWindowWithClass(rOption.sType);
				if winOption then
					winOption.setLabel(rOption.sLabel);
					winOption.initialize(rOption.sKey, rOption.aCustom);
					winOption.setLock(not (rOption.bLocal or User.isHost()));
				end
			end
		end
	end
end

function registerOption(sKey, bLocal, sGroup, sLabel, sOptionType, aCustom)
	local rOption = {};
	rOption.sKey = sKey;
	rOption.bLocal = bLocal;
	rOption.sLabel = sLabel;
	rOption.aCustom = aCustom;
	rOption.sType = sOptionType;
	
	if not sets[sGroup] then
		sets[sGroup] = {};
	end
	table.insert(sets[sGroup], rOption);
	
	options[sKey] = rOption;
	options[sKey].value = (options[sKey].aCustom[default]) or "";
	
	linkNode(sKey);
end

function linkNode(sKey)
	if options[sKey] and not options[sKey].bLinked and not options[sKey].bLocal then
		local nodeOptions = DB.createNode("options");
		if nodeOptions then
			local nodeOption = NodeManager.createChild(nodeOptions, sKey, "string");
			if nodeOption then
				nodeOption.onUpdate = onOptionChanged;
				options[sKey].bLinked = true;
			end
		end
	end
end

function onOptionChanged(nodeOption)
	local sKey = nodeOption.getName();
	makeCallback(sKey);
end

function registerCallback(sKey, fCallback)
	if not callbacks[sKey] then
		callbacks[sKey] = {};
	end
	
	table.insert(callbacks[sKey], fCallback);

	linkNode(sKey);
end

function unregisterCallback(sKey, fCallback)
	if callbacks[sKey] then
		for k, v in pairs(callbacks[sKey]) do
			if v == fCallback then
				callbacks[sKey][k] = nil;
			end
		end
	end
end

function makeCallback(sKey)
	if callbacks[sKey] then
		for k, v in pairs(callbacks[sKey]) do
			v(sKey);
		end
	end
end

function setOption(sKey, sValue)
	if options[sKey] then
		if options[sKey].bLocal then
			CampaignRegistry["Opt" .. sKey] = sValue;
			makeCallback(sKey);
		else
			if not User.isHost() then
				return;
			end
			local nodeOptions = DB.createNode("options");
			if not nodeOptions then
				return;
			end
			local nodeOption = NodeManager.createChild(nodeOptions, sKey, "string");
			if not nodeOption then
				return;
			end

			nodeOption.setValue(sValue);
		end
	end
end

function isOption(sKey, sTargetValue)
	return (getOption(sKey) == sTargetValue);
end

function getOption(sKey)
	if options[sKey] then
		if options[sKey].bLocal then
			if CampaignRegistry["Opt" .. sKey] then
				return CampaignRegistry["Opt" .. sKey];
			end
		else
			local nodeOptions = DB.findNode("options");
			if nodeOptions then
				local nodeOption = nodeOptions.getChild(sKey);
				if nodeOption then
					local sValue = nodeOption.getValue();
					if sValue ~= "" then
						return sValue;
					end
				end
			end
		end

		return (options[sKey].aCustom.default) or "";
	end

	return "";
end
