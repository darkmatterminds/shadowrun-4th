-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

--  ACTION FLOW
--
--	1. INITIATE ACTION (DRAG OR DOUBLE-CLICK)
--	2. DETERMINE TARGETS (DROP OR TARGETING SUBSYSTEM)
--	3. APPLY MODIFIERS
--	4. PERFORM ROLLS (IF ANY)
--	5. RESOLVE ACTION

-- ROLL
--		.sType
--		.sDesc
--		.aDice
--		.nValue
--                    bEdge
--
	
	
function onInit()
	Interface.onHotkeyActivated = onHotkey;
end

function onHotkey(draginfo)
	local sDragType = draginfo.getType();
	if StringManager.contains(DataCommon.actions, sDragType) then
		local rSource, rRolls = decodeActionFromDrag(draginfo);
		handleActionNonDrag(rSource, sDragType, rRolls);
		return true;
	end
end

local aMultiHandlers = {};
function registerMultiHandler(sActionType, callback)
	aMultiHandlers[sActionType] = callback;
end
function unregisterMultiHandler(sRollType, callback)
	if aMultiHandlers then
		aMultiHandlers[sActionType] = nil;
	end
end

function getRollType(sDragType, nSlot)
	for k, v in pairs(aMultiHandlers) do
		if k == sDragType then
			return v(nSlot);
		end
	end

	return sDragType;
end

local aTargetingHandlers = {};
function registerTargetingHandler(sActionType, callback)
	aTargetingHandlers[sActionType] = callback;
end
function unregisterTargetingHandler(sRollType, callback)
	if aTargetingHandlers then
		aTargetingHandlers[sActionType] = nil;
	end
end

local aModHandlers = {};
function registerModHandler(sActionType, callback)
	aModHandlers[sActionType] = callback;
end
function unregisterModHandler(sRollType, callback)
	if aModHandlers then
		aModHandlers[sActionType] = nil;
	end
end

local aResultHandlers = {};
function registerResultHandler(sActionType, callback)
	aResultHandlers[sActionType] = callback;
end
function unregisterResultHandler(sRollType, callback)
	if aResultHandlers then
		aResultHandlers[sActionType] = nil;
	end
end


--
--  INITIATE ACTION
--
function performReroll(rSource, rRoll)
	rRoll.aDice = {};
	rRoll.sDesc = rRoll.sRerollDesc;
	for i = 1, rRoll.nRerollCount do
		table.insert (rRoll.aDice, "d6");
	end

	performSingleRollAction(nil,rSource,"reroll", rRoll)
end

function performOppossedRoll(rSource, rRoll)

	rRoll.aDice = {};
	rRoll.sDesc = ""
	rRoll.sDesc = "[OPPOSED] Check"

	rRoll = ActionSkill.getSpiritOposedRoll(rSource, rRoll);	
		
	rRoll.nValue = 0;
	
	performSingleRollAction(nil,rSource,"spiritisopposed", rRoll)
end


function performSingleRollAction(draginfo, rActor, sType, rRoll, bOverride, rTarget)
	if not rRoll then
		return;
	end
	
	local rRolls = {};
	table.insert(rRolls, rRoll);

	performMultiRollAction(draginfo, rActor, sType, rRolls, bOverride, rTarget);
end

function performMultiRollAction(draginfo, rActor, sType, rRolls, bOverride, rTarget)
	if not rRolls or #rRolls < 1 then
		return false;
	end
	
	if not bOverride then
		if draginfo then
			local bOptionDRGR = OptionsManager.getOption("DRGR");
			if bOptionDRGR ~= "on" then
				draginfo.setType("number");
				draginfo.setSlot(1);
				draginfo.setDescription(rRolls[1].sDesc);
				draginfo.setNumberData(rRolls[1].nValue);
				return true;
			end
		else
			local bOptionDCLK = OptionsManager.getOption("DCLK");
			if bOptionDCLK ~= "on" then
				if bOptionDCLK == "mod" then
					ModifierStack.addSlot(rRolls[1].sDesc, rRolls[1].nValue);
				end
				return true;
			end
		end
	end
		
	if draginfo then
		encodeActionForDrag(draginfo, rActor, sType, rRolls);
	else
		handleActionNonDrag(rActor, sType, rRolls, rTarget);
	end
	return true;
end

function encodeActionForDrag(draginfo, rSource, sType, rRolls)
	draginfo.setType(sType);

	draginfo.setSlot(1);
	if rSource then
		if rSource.sCTNode ~= "" then
			draginfo.setShortcutData("combattracker_entry", rSource.sCTNode);
		elseif rSource.sCreatureNode ~= "" then
			if rSource.sType == "pc" then
				draginfo.setShortcutData("charsheet", rSource.sCreatureNode);
			elseif rSource.sType == "npc" then
				draginfo.setShortcutData("npc", rSource.sCreatureNode);
			end
		end
	end
	
	for k,v in ipairs(rRolls) do
		draginfo.setSlot(k);
		draginfo.setStringData(v.sDesc);
		draginfo.setDieList(v.aDice);
		draginfo.setNumberData(v.nValue);
	end
end

function decodeActorFromDrag(draginfo)
	local rSource = nil;

	draginfo.setSlot(1);
	local sRefClass, sRefNode = draginfo.getShortcutData();
	if sRefClass and sRefNode then
		if sRefClass == "combattracker_entry" then
			rSource = ActorManager.getActor("ct", sRefNode);
		elseif sRefClass == "charsheet" then
			rSource = ActorManager.getActor("pc", sRefNode);
		elseif sRefClass == "npc" then
			rSource = ActorManager.getActor("npc", sRefNode);
		end
	end
	
	return rSource;
end

function decodeActionFromDrag(draginfo)
	local rSource = decodeActorFromDrag(draginfo);
	
	local rRolls = {};
	local nSlots = draginfo.getSlotCount();
	for i = 1, nSlots do
		draginfo.setSlot(i);
		
		local sDesc = draginfo.getStringData();
		local nValue = draginfo.getNumberData();
		
		local aDice = {};
		local aDragDice = draginfo.getDieList();
		if aDragDice then
			for k, v in pairs(aDragDice) do
				if type(v) == "string" then
					table.insert(aDice, v);
				elseif type(v) == "table" then
					table.insert(aDice, v["type"]);
				end
			end
		end
		
		table.insert(rRolls, { sDesc = sDesc, aDice = aDice, nValue = nValue });
	end
	
	return rSource, rRolls;
end

--
--  DETERMINE TARGETS
--

function handleActionDrop(draginfo, rTarget)
	if rTarget then
		local sDragType = draginfo.getType();
		local rSource, rRolls = decodeActionFromDrag(draginfo);

		lockModifiers();
		for k,v in ipairs(rRolls) do
			local sType = getRollType(sDragType, k);
			applyModifiersAndRoll(rSource, rTarget, sType, v.sDesc, v.aDice, v.nValue, false);
		end
		unlockAndResetModifiers();
	else
		handleDragDrop(draginfo, true);
	end
end

function handleActionNonDrag(rActor, sDragType, rRolls, rTarget)
	local bSingleRoll = false;
	local aTargets = {};
	if rTarget then
		table.insert(aTargets, rTarget);
	else
		for k, v in pairs(aTargetingHandlers) do
			if k == sDragType then
				aTargets, bSingleRoll = v(rActor, rRolls);
				break;
			end
		end
	end

	lockModifiers();
	local nRerollSuccesses = 0;
	local nTotalDice = 0;
	if #aTargets == 0 or bSingleRoll then
		for k, v in ipairs(rRolls) do
			local sType = getRollType(sDragType, k);
			nRerollSuccesses = v.nRerollSuccesses;
			nTotalDice = v.nTotalDice;
			applyModifiersAndRoll(rActor, aTargets, sType, v.sDesc, v.aDice, v.nValue, true, nRerollSuccesses, nTotalDice);
		end
	else
		for i = 1, #aTargets do
			for k, v in ipairs(rRolls) do
				local sType = getRollType(sDragType, k);
				applyModifiersAndRoll(rActor, aTargets[i], sType, v.sDesc, v.aDice, v.nValue, false, nRerollSuccesses, nTotalDice);
			end
		end
	end
	
	unlockAndResetModifiers();
end


--
--  APPLY MODIFIERS
--

function applyModifiersAndRoll(rSource, vTarget, sType, sDesc, aDice, nValue, bMultiTarget, nRerollSuccesses, nTotalDice)
	local rRoll = {};
	rRoll.sType = sType;
	rRoll.sDesc = sDesc;
	rRoll.aDice = aDice;
	rRoll.nValue = nValue;
	rRoll.bEdge = bEdge;
	rRoll.nRerollSuccesses = nRerollSuccesses;
	rRoll.nTotalDice = nTotalDice
	
	-- Only apply non-target specific modifiers before roll
	if bMultiTarget then
		if vTarget and #vTarget > 1 then
			applyModifiers(rSource, nil, rRoll);
		elseif vTarget and #vTarget == 1 then
			applyModifiers(rSource, vTarget[1], rRoll);
		else
			applyModifiers(rSource, nil, rRoll);
		end
	else
		applyModifiers(rSource, vTarget, rRoll);
	end
	roll(rSource, vTarget, rRoll, bMultiTarget);
end

function handleDragDrop(draginfo, bResolveIfNoDice)
	lockModifiers();
	
	local rSource = decodeActorFromDrag(draginfo);
	local sDragType = draginfo.getType();
	
	local nSlots = draginfo.getSlotCount();
	for i = 1, nSlots do
		draginfo.setSlot(i);
		
		local sType = getRollType(sDragType, i);
		applyModifiersToDragSlot(draginfo, rSource, sType, bResolveIfNoDice);
	end
	
	unlockAndResetModifiers();
end

function applyModifiersToDragSlot(draginfo, rSource, sType, bResolveIfNoDice)
	local rRoll = {};
	rRoll.sType = sType;
	rRoll.sDesc = draginfo.getStringData();
	rRoll.aDice = draginfo.getDieList() or {};
	rRoll.nValue = draginfo.getNumberData();
	local nDice = #(rRoll.aDice);
	applyModifiers(rSource, nil, rRoll);
	local nNewDice = #(rRoll.aDice);
	
	if bResolveIfNoDice and nNewDice == 0 then
		resolveAction(rSource, nil, rRoll);
		return;
	end
	draginfo.setStringData(rRoll.sDesc);
	for i = nDice + 1, nNewDice do
		draginfo.addDie(rRoll.aDice[i].type);
	end
	draginfo.setNumberData(rRoll.nValue);
end

function lockModifiers()
	ModifierStack.lock();
	EffectsManager.lock();
end

function unlockAndResetModifiers()
	ModifierStack.unlock();
	EffectsManager.unlock();
end

function applyModifiers(rSource, rTarget, rRoll)	
	local bHasDice = (#(rRoll.aDice) > 0);

	for k, v in pairs(aModHandlers) do
		if k == rRoll.sType then
			v(rSource, rTarget, rRoll);
			break;
		end
	end

	if bHasDice then
		local sStackDesc, nStackMod = ModifierStack.getStack();
		
		if sStackDesc ~= "" then
			if rRoll.sDesc ~= "" then
				rRoll.sDesc = rRoll.sDesc .. " (" .. sStackDesc .. ")";
			else
				rRoll.sDesc = rRoll.sDesc .. " " .. sStackDesc;
			end
		end
		rRoll.nValue = rRoll.nValue + nStackMod;
		
	
	end
end


--
--	RESOLVE DICE
--

function roll(rSource, vTargets, rRoll, bMultiTarget)
	if #(rRoll.aDice) > 0 then
		local aCustom = ActorManager.encodeActors(rSource, vTargets, bMultiTarget, rRoll.nRerollSuccesses, rRoll.nTotalDice);
		Comm.throwDice(rRoll.sType, rRoll.aDice, rRoll.nValue, rRoll.sDesc, aCustom);
	else
		if bMultiTarget then
			if vTargets and #vTargets > 1 then
				for kTarget, rTarget in ipairs(vTargets) do
					rTarget.nOrder = kTarget;
					resolveAction(rSource, rTarget, rRoll); 
				end
			elseif vTargets and #vTargets == 1 then
				resolveAction(rSource, vTargets[i], rRoll);
			else
				resolveAction(rSource, nil, rRoll);
			end
		else
			resolveAction(rSource, vTargets, rRoll);
		end
	end
end 


function onDiceLanded(draginfo)
	local sDragType = draginfo.getType();
	if StringManager.contains(DataCommon.actions, sDragType) then

		local rSource, aTargets, nRerollSuccesses, nTotalDice = ActorManager.decodeActors(draginfo);
		local sDesc = draginfo.getDescription();
		local nSlots = draginfo.getSlotCount();
		for i = 1, nSlots do
			draginfo.setSlot(i);

			local rRoll = {};
			rRoll.nRerollSuccesses = nRerollSuccesses;
			rRoll.nTotalDice = nTotalDice;
			rRoll.sType = getRollType(sDragType, i);
			rRoll.sDesc = draginfo.getStringData();
			if rRoll.sDesc == "" then
				rRoll.sDesc = sDesc;
			end
			rRoll.aDice = processPercentiles(draginfo) or {};
			rRoll.nValue = draginfo.getNumberData();
			rRoll.aCustom = draginfo.getCustomData();
			if #aTargets == 0 then
				resolveAction(rSource, nil, rRoll);
			elseif #aTargets == 1 then
				resolveAction(rSource, aTargets[1], rRoll);
			else
				for kTarget, rTarget in ipairs(aTargets) do
					rTarget.nOrder = kTarget;
					resolveAction(rSource, rTarget, rRoll);
				end
			end
		end
		return true;
	end
end

function processPercentiles(draginfo)
	local aDragDieList = draginfo.getDieList();
	if not aDragDieList then
		return nil;
	end

	local aD100Indexes = {};
	local aD10Indexes = {};
	for k, v in pairs(aDragDieList) do
		if v["type"] == "d100" then
			table.insert(aD100Indexes, k);
		elseif v["type"] == "d10" then
			table.insert(aD10Indexes, k);
		end
	end

	local nMaxMatch = #aD100Indexes;
	if #aD10Indexes < nMaxMatch then
		nMaxMatch = #aD10Indexes;
	end
	if nMaxMatch <= 0 then
		return aDragDieList;
	end
	
	local nMatch = 1;
	local aNewDieList = {};
	for k, v in pairs(aDragDieList) do
		if v["type"] == "d100" then
			if nMatch > nMaxMatch then
				table.insert(aNewDieList, v);
			else
				v["result"] = aDragDieList[aD100Indexes[nMatch]]["result"] + aDragDieList[aD10Indexes[nMatch]]["result"];
				table.insert(aNewDieList, v);
				nMatch = nMatch + 1;
			end
		elseif v["type"] == "d10" then
			local bInsert = true;
			for i = 1, nMaxMatch do
				if aD10Indexes[i] == k then
					bInsert = false;
				end
			end
			if bInsert then
				table.insert(aNewDieList, v);
			end
		else
			table.insert(aNewDieList, v);
		end
	end

	return aNewDieList;
end

-- 
--  RESOLVE ACTION
--  (I.E. DISPLAY CHAT MESSAGE, COMPARISONS, ETC.)
--

function resolveAction(rSource, rTarget, rRoll)
	-- Apply target specific modifiers before roll
	if rTarget and rTarget.nOrder then
		applyModifiers(rSource, rTarget, rRoll);
	end

	for k, v in pairs(aResultHandlers) do
		if k == rRoll.sType then
			v(rSource, rTarget, rRoll);
			return;
		end
	end
	local rMessage, rSource, rRoll = createRollTable(rSource, rRoll);
	rMessage, rSource, rRoll = createActionMessage(rSource, rRoll, rMessage);
	if rRoll.sType == "spiritisopposed" then
		local isInvoced = string.match(rRoll.sDesc, "%invocation")
		if isInvoced then
			nDrain = rRoll.nSuccessesCalling * 3;
		else 
			nDrain = rRoll.nSuccessesCalling * 2;
		end
		rMessage.text = rMessage.text .. " draining your Mental Power for " .. nDrain .. " Stun Damage."
	end
	Comm.deliverChatMessage(rMessage); 
	if rRoll.sType == "reroll" and rRoll.nRerollCount then
	performReroll(rSource, rRoll);
	end
	if rRoll.sType == "spiritopposed" then
	performOppossedRoll(rSource, rRoll)
	end
end


function createRollTable(rSource, rRoll)
	local nSuccesses = 0;
	local nFailures = 0;
	local nGlitchDice = 0;
	local bNoPrint = false;
	local bEdge = false;
	local nRerollCount = 0
	local nTotalDice = 0
	local rMessage = ChatManager.createBaseMessage(rSource);
	local bNonD6 = false
	local nSuccessesCalling = 0
	local isEdge = string.match(rRoll.sDesc, "%[EDGE]");
		if isEdge then
			bEdge = true;
		end
		if rSource and rSource.sType == "pc" then
		local b = DB.findNode(rSource.sCreatureNode).getChild("edgecheckbox").getValue();
			if b == 1 then
				bEdge = true;
			end
		end

	if bEdge == true and rSource then
		local nEdgeUsed = 0
		nEdgeUsed = DB.findNode(rSource.sCreatureNode).getChild("base.attribute.edge.mod").getValue();
		if DB.findNode(rSource.sCreatureNode).getChild("edgecheckbox").getValue() == 1 then
		nEdgeUsed = (nEdgeUsed + 1)
		DB.findNode(rSource.sCreatureNode).getChild("base.attribute.edge.mod").setValue(nEdgeUsed);
		DB.findNode(rSource.sCreatureNode).getChild("edgecheckbox").setValue(0);
		end
	end

	-- Individual Dice Handling, Trooping the Color
		if rRoll.nTotalDice then
			nTotalDice = rRoll.nTotalDice
		end
		if rRoll.nRerollSuccesses and rRoll.nRerollSuccesses > 0 then
			nSuccesses = rRoll.nRerollSuccesses
		end
	

		for i , v in ipairs (rRoll.aDice) do
			if rRoll.aDice[i].type ~= "d6" or rRoll.aDice[i].type ~= "d6" or rRoll.aDice[i].type ~= "d6" or rRoll.aDice[i].type ~= "d6" then
				bNonD6 = true
			elseif rRoll.aDice[i].type == "d6" then
				nTotalDice = nTotalDice + 1
				if rRoll.aDice[i].result == 6 then 
					nSuccesses = nSuccesses + 1;
					nSuccessesCalling = nSuccessesCalling + 1;
					rRoll.aDice[i].type = "ddg6";
					if bEdge == true or rRoll.sType == "reroll" then
						bNoPrint = true;
						nRerollCount = nRerollCount + 1;
					end
				elseif rRoll.aDice[i].result == 5 then 
					nSuccesses = nSuccesses + 1;
					nSuccessesCalling = nSuccessesCalling + 1;
					rRoll.aDice[i].type = "dhg6";
				elseif rRoll.aDice[i].result == 1 then 
					nFailures = nFailures + 1;
					rRoll.aDice[i].type = "dr6";
				end
			end
		end
	if nRerollCount == 0 then
		bNoPrint = false	
	end
	if bNoPrint == true then 

		if rRoll.nRerollsuccesses then
			rRoll.nRerollSuccesses = rRoll.nRerollSuccesses + nSuccesses;
		else
			rRoll.nRerollSuccesses = nSuccesses
		end
		if rRoll.nTotalDice then
			rRoll.nTotalDice = nTotalDice
		end
		local isDiceTower = string.match(rRoll.sDesc, "^%[TOWER%]");
		local isGMOnly = string.match(rRoll.sDesc, "^%[GM%]");
		if isDiceTower then
			rMessage.dicesecret = true;
			rMessage.sender = "";
			rMessage.icon = "dicetower_icon";
		elseif isGMOnly then
			rMessage.dicesecret = true;
			rMessage.text = "[GM] " .. rMessage.text;
		elseif User.isHost() and OptionsManager.isOption("REVL", "off") then
			rMessage.dicesecret = true;
			rMessage.text = "[GM] " .. rMessage.text;
		end
		rRoll.sRerollDesc = rRoll.sDesc;
		rRoll. sDesc = "Reroll comenced";
		rRoll.nRerollCount = nRerollCount
		rMessage.text = rRoll.sDesc;
		rMessage.dice = rRoll.aDice;
		rRoll.sType = "reroll"
		return rMessage, rSource, rRoll;
	else
		rRoll.sDesc = rRoll.sDesc .. " ".. "[TOTAL-Dice] (" .. nTotalDice .. ") " .. "(Successes: ".. nSuccesses .. ") "
		rRoll.nSuccessesCalling = nSuccessesCalling
		
		local nHalfDice = math.ceil(nTotalDice / 2)
		if bEdge == false then
			if OptionsManager.isOption("SHGL", "on") then
				if (nSuccesses < nFailures or nHalfDice < nFailures) and nSuccesses >= 1 then
					rRoll.sDesc = rRoll.sDesc .. " ".. "[GLITCH]"
				elseif (nSuccesses < nFailures or nHalfDice < nFailures) and nSuccesses <= 0 then
					rRoll.sDesc = rRoll.sDesc .. " ".. "[CRITICAL GLITCH]"
				end
			else
				if nHalfDice <= nFailures and nSuccesses <= 0 then
					rRoll.sDesc = rRoll.sDesc .. " ".. "[CRITICAL GLITCH]"
				elseif nHalfDice <= nFailures and nSuccesses >= 1 then
					rRoll.sDesc = rRoll.sDesc .. " ".. "[GLITCH]"
				end
			end
		end	
	end

		local isSpiritOpposedCheck = string.match(rRoll.sDesc, "%[SPIRIT%]")
		if isSpiritOpposedCheck then
		local isDiceTower = string.match(rRoll.sDesc, "^%[TOWER%]");
		local isGMOnly = string.match(rRoll.sDesc, "^%[GM%]");
		if isDiceTower then
			rMessage.dicesecret = true;
			rMessage.sender = "";
			rMessage.icon = "dicetower_icon";
		elseif isGMOnly then
			rMessage.dicesecret = true;
			rMessage.text = "[GM] " .. rMessage.text;
		elseif User.isHost() and OptionsManager.isOption("REVL", "off") then
			rMessage.dicesecret = true;
			rMessage.text = "[GM] " .. rMessage.text;
		end
			rRoll.sRerollDesc = rRoll.sDesc;
			rMessage.text = rRoll.sDesc;
			rMessage.dice = rRoll.aDice;
			rRoll.sType = "spiritopposed"
			rRoll.nRerollCount = nRerollCount
			rRoll.nSuccessesPC = nSuccesses
		return rMessage, rSource, rRoll;
		end
	
	return rMessage, rSource, rRoll;
end

function createActionMessage(rSource, rRoll, rMessage)
	local isDiceTower = string.match(rRoll.sDesc, "^%[TOWER%]");
	local isGMOnly = string.match(rRoll.sDesc, "^%[GM%]");
	local isInit = string.match (rRoll.sDesc, "%[INIT]");
	
	if isInit then
		local nInit = 0
		if rSource.nodeCT and rSource.nodeCT ~= "" then
			local sCTRolled = NodeManager.get(rSource.nodeCT, "ctinitrolled", "2")
			if sCTRolled == 0 then
				nInit = rRoll.nSuccessesCalling
				if rRoll.nRerollSuccesses > 0 then
					nInit = nInit + rRoll.nRerollSuccesses
				end
			NodeManager.set(rSource.nodeCT, "initroll", "number", nInit)
			NodeManager.set(rSource.nodeCT, "ctinitrolled", "number", 1)
			CombatCommon.postInit(rSource)
			else
			rRoll.sDesc = "Sorry, Initiative already rolled for this Round";
			rRoll.aDice = {};
			
			end
		end
	end
	-- Handle GM flag and TOWER flag

	if isDiceTower then
		rSource = nil;
	elseif isGMOnly then
		rRoll.sDesc = string.sub(rRoll.sDesc, 6);
	end

	-- Build the basic message to deliver

	rMessage.text = rRoll.sDesc;
	rMessage.dice = rRoll.aDice;
	rMessage.diemodifier = rRoll.nValue;
	
	-- Check to see if this roll should be secret (GM or dice tower tag)
	if isDiceTower then
		rMessage.dicesecret = true;
		rMessage.sender = "";
		rMessage.icon = "dicetower_icon";
	elseif isGMOnly then
		rMessage.dicesecret = true;
		rMessage.text = "[GM] " .. rMessage.text;
	elseif User.isHost() and OptionsManager.isOption("REVL", "off") then
		rMessage.dicesecret = true;
		rMessage.text = "[GM] " .. rMessage.text;
	end
	
	-- Show total if option enabled
	if OptionsManager.isOption("TOTL", "on") and #(rRoll.aDice) > 0 then
		rMessage.dicedisplay = 1;
	end
	return rMessage, rSource, rRoll;
end

function total(rRoll)
	local nTotal = 0;

	for k, v in ipairs(rRoll.aDice) do
		nTotal = nTotal + v.result;
	end
	nTotal = nTotal + rRoll.nValue;
	
	return nTotal;
end

