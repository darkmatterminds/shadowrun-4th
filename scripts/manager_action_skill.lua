-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	ActionsManager.registerModHandler("skill", modRoll);
end

function getWeaponData(rActor, rRoll, nValue)
		local nWeaponBonus = 0;
			if rActor.nodeCreature.getChild("node.activeweapon") and rActor.nodeCreature.getChild("node.activeweapon").getValue() ~="none" then
				local nodeActiveWeapon = rActor.nodeCreature.getChild("node.activeweapon").getValue();
				local sWeaponName = DB.findNode(nodeActiveWeapon).getChild("weaponname").getValue();
				local nWeaponBonus = DB.findNode(nodeActiveWeapon).getChild("weapon.skillbonus").getValue();
				rRoll.sDesc = rRoll.sDesc.." using " .. sWeaponName .. " (Bonus: ".. nWeaponBonus .. ") ";
				nValue = nValue + nWeaponBonus;
				if DB.findNode(nodeActiveWeapon).getChild("firemode").getValue() == 1 then
					rRoll.sDesc = rRoll.sDesc .. "firing " .. DB.findNode(nodeActiveWeapon).getChild("weapon.singleshot").getValue() .. " round (single) ";
				elseif DB.findNode(nodeActiveWeapon).getChild("firemode").getValue() == 2 then
					rRoll.sDesc = rRoll.sDesc .. "firing " .. DB.findNode(nodeActiveWeapon).getChild("weapon.shortburst").getValue() .. " rounds (short burst)";
				elseif DB.findNode(nodeActiveWeapon).getChild("firemode").getValue() == 3 then
					rRoll.sDesc = rRoll.sDesc .. "firing " .. DB.findNode(nodeActiveWeapon).getChild("weapon.longburst").getValue() .. " rounds (long burst)";
				elseif DB.findNode(nodeActiveWeapon).getChild("firemode").getValue() == 4 then
					rRoll.sDesc = rRoll.sDesc .. "firing " .. DB.findNode(nodeActiveWeapon).getChild("weapon.fullauto").getValue() .. " rounds (full auto) ";
				end
			end
		return rRoll.sDesc, nValue;
end

function getFinalWeaponText(rActor, rRoll, nValue)
	local sDamageType = " ";
	local nDV = 0;
	local nAP = 0;

	if rActor.nodeCreature.getChild("node.activeweapon").getValue() ~="none" then
		nodeActiveWeapon = rActor.nodeCreature.getChild("node.activeweapon").getValue();
		if DB.findNode(nodeActiveWeapon).getChild("weapondamagetype") then
		sDamageType = DB.findNode(nodeActiveWeapon).getChild("weapondamagetype").getValue();
			if DB.findNode(nodeActiveWeapon).getChild("activeammo") and DB.findNode(nodeActiveWeapon).getChild("activeammo").getValue() ~= "" then
				local sActiveAmmo = DB.findNode(nodeActiveWeapon).getChild("activeammo").getValue();
				local nFireMode = DB.findNode(nodeActiveWeapon).getChild("firemode").getValue();
				local nAmmoUsed = 0
				local nAmmoCount = DB.findNode(sActiveAmmo).getChild("ammunition.count").getValue();
					if nFireMode == 1 then
						nAmmoUsed = DB.findNode(nodeActiveWeapon).getChild("weapon.singleshot").getValue();
					elseif nFireMode == 2 then
						nAmmoUsed = DB.findNode(nodeActiveWeapon).getChild("weapon.shortburst").getValue();
					elseif nFireMode == 3 then
						nAmmoUsed = DB.findNode(nodeActiveWeapon).getChild("weapon.longburst").getValue();
					elseif nFireMode == 4 then
						nAmmoUsed = DB.findNode(nodeActiveWeapon).getChild("weapon.fullauto").getValue();
					end
					local nAmmoLeft = nAmmoCount - nAmmoUsed
					if nAmmoLeft == 0 then
						rRoll.sDesc = rRoll.sDesc .. "[WARNING! No more Ammo]";
					elseif nAmmoLeft < 0 and nAmmoUsed > (-nAmmoLeft) then 
						rRoll.sDesc = rRoll.sDesc .. "[WARNING! No more Ammo] (short " .. nAmmoLeft .. " Rounds)";
						nAmmoLeft = 0
					elseif nAmmoLeft < 0 and nAmmoUsed <= (-nAmmoLeft) then
						rRoll.sDesc = "[Click.... uhm Click? I thought this weapon comes automatically with ammo]"
						nAmmoLeft = 0
						return rRoll.sDesc
					end
					DB.findNode(sActiveAmmo).getChild("ammunition.count").setValue(nAmmoLeft);
				if DB.findNode(sActiveAmmo).getChild("ammodamagetype").getValue() and DB.findNode(sActiveAmmo).getChild("ammodamagetype").getValue() ~= "" then
					sDamageType = DB.findNode(sActiveAmmo).getChild("ammodamagetype").getValue();
				end
			end
		elseif DB.findNode(nodeActiveWeapon).getChild("activeammo") then
			local sActiveAmmo = DB.findNode(nodeActiveWeapon).getChild("activeammo").getValue();
				if DB.findNode(sActiveAmmo).getChild("ammodamagetype").getValue() and DB.findNode(sActiveAmmo).getChild("ammodamagetype").getValue() ~= " " then
					sDamageType = DB.findNode(sActiveAmmo).getChild("ammodamagetype").getValue();
				end
		end

		if sDamageType ~= "" then
			rRoll.sDesc = rRoll.sDesc .. " harming the targets sorry behind with ".. sDamageType.." Damage ";
		else
			rRoll.sDesc = rRoll.sDesc .. " harming the targets sorry behind"
		end
		if DB.findNode(nodeActiveWeapon).getChild("weapon.damage") then
			nDV = DB.findNode(nodeActiveWeapon).getChild("weapon.damage").getValue();
			if DB.findNode(nodeActiveWeapon).getChild("activeammo") then
				local sActiveAmmo = DB.findNode(nodeActiveWeapon).getChild("activeammo").getValue();
				nDV = nDV + DB.findNode(sActiveAmmo).getChild("ammunition.dv").getValue();
			end
		end
		if nDV and nDV > 0 then
			rRoll.sDesc = rRoll.sDesc .. " (Base DV: ".. nDV .. ") "
		end
		if DB.findNode(nodeActiveWeapon).getChild("weapon.armorpenetration") then
			nAP = DB.findNode(nodeActiveWeapon).getChild("weapon.armorpenetration").getValue();
			if DB.findNode(nodeActiveWeapon).getChild("activeammo") then
				local sActiveAmmo = DB.findNode(nodeActiveWeapon).getChild("activeammo").getValue();
				nAP = nAP + DB.findNode(sActiveAmmo).getChild("ammunition.ap").getValue();
			end
		end
		if nAP and nAP ~= 0 then
			rRoll.sDesc = rRoll.sDesc .. " (Base AP: ".. nAP .. ") "
		end
	end
	return rRoll.sDesc;
end

function getProgramData(rActor, rRoll, nValue)
local nComlinkResponse = 0;
local nComlinkSystem = 0;
local nodeComlink = "none";
local bProgramActive = false

			
			if rActor.nodeCreature.getChild("node.activeprogram") and rActor.nodeCreature.getChild("node.activeprogram").getValue() ~="none" then
				if rActor.nodeCreature.getChild("node.activecomlink") then
					if rActor.nodeCreature.getChild("node.activecomlink").getValue() == "none" then
						rRoll.sDesc = rRoll.sDesc .. " [NO Active Comlink] "
					else
					nodeComlink = rActor.nodeCreature.getChild("node.activecomlink").getValue();
					nComlinkResponse = DB.findNode(nodeComlink).getChild("comlinkresponse").getValue();
					nComlinkSystem = DB.findNode(nodeComlink).getChild("comlinksystem").getValue();
						if nComlinkResponse < nComlinkSystem then
							nComlinkSystem = nComlinkResponse
							rRoll.sDesc = rRoll.sDesc .. " [ATTENTION - Systemrating smaller then base Response]";
						end
					end
				end

				local nodeActiveProgram = rActor.nodeCreature.getChild("node.activeprogram").getValue();
				local sProgramName = DB.findNode(nodeActiveProgram).getChild("programname").getValue();
				local nProgramRating = DB.findNode(nodeActiveProgram).getChild("programrating").getValue();
				if nProgramRating > nComlinkSystem then
					nProgramRating = nComlinkSystem
					rRoll.sDesc = rRoll.sDesc .. "[ATTENTION - ProgramRating higher then Systemrating]";
				end
				if nodeComlink ~= "none" then
					local a = {}
					a = DB.findNode(nodeComlink).getChild("comlinkmodifier").getChildren();
					for k, v in pairs(a) do
						if nodeActiveProgram == v.getChild("comlinkmodifiernode").getValue() and v.getChild("comlinkmodifierinusebox").getValue() == 1 then
							bProgramActive = true
						end
					end
					if bProgramActive == true then
					rRoll.sDesc = rRoll.sDesc .. " using " .. sProgramName .. " with Rating of (" .. nProgramRating .. ")";
					nValue = nValue + nProgramRating
					else 
					rRoll.sDesc = rRoll.sDesc .. " using " .. sProgramName .. " [ATTENTION - Program not active on Comlink] with Rating of (" .. nProgramRating .. ")";
					nValue = nValue + nProgramRating
					end
				end
			end
		return rRoll.sDesc, nValue;
end

function getSpellData(rActor, rRoll, nValue)
			if rActor.nodeCreature.getChild("node.activespell") and rActor.nodeCreature.getChild("node.activespell").getValue() ~="none" then
				local nodeActiveSpell = rActor.nodeCreature.getChild("node.activespell").getValue();
				local sSpellName = DB.findNode(nodeActiveSpell).getChild("spellname").getValue();
				local nSpellForce = DB.findNode(nodeActiveSpell).getChild("spellforce").getValue();
				local nSpellStandardDrain = DB.findNode(nodeActiveSpell).getChild("spelldrain").getValue();
				local nSpellDrain = math.floor((nSpellForce /2) + nSpellStandardDrain);
				if nSpellDrain < 1 then
					nSpellDrain = 1
				end
				local nStandardSpellForce = DB.findNode(nodeActiveSpell).getChild("spellstandardforce").getValue();
				local nPhysicalDrain = nSpellForce - nStandardSpellForce;
				if nPhysicalDrain <=0 then
					rRoll.sDesc = rRoll.sDesc.." casting " .. sSpellName .. " (Force: ".. nSpellForce .. ") draining your Mental Power for [DRAIN: " .. nSpellDrain .. "]";
				elseif nPhysicalDrain > 0 and nStandardSpellForce >=  nPhysicalDrain then
					rRoll.sDesc = rRoll.sDesc.." casting " .. sSpellName .. " (Force (exeeds Magic Attribute: ".. nSpellForce .. ") draining your PHYSICAL Power for [DRAIN: " .. nSpellDrain .. "]";
				elseif nPhysicalDrain > 0 and nStandardSpellForce <  nPhysicalDrain then
					rRoll.sDesc = "OVERCAST, YOU are DEAD.... Dead? Yes, it is Dead, Jim   "
				end
				
				nValue = nValue;
			end
		return rRoll.sDesc, nValue;
end

function getSpiritOposedRoll(rActor, rRoll)
		local sActiveSpirit = rActor.nodeCreature.getChild("node.activespirit").getValue();
		local nSpiritForce = DB.findNode(sActiveSpirit).getChild("spirit.force").getValue();
		rRoll.aDice = {}
		if DB.findNode(sActiveSpirit).getChild("spiritsummoning").getValue() == 1 then
			nSpiritForce = nSpiritForce
		rRoll.sDesc = DB.findNode(sActiveSpirit).getChild("spiritname").getValue() .. " oposes summoning with [FORCE: (" .. nSpiritForce ..")"
		elseif DB.findNode(sActiveSpirit).getChild("spiritbinding").getValue() == 1 and DB.findNode(sActiveSpirit).getChild("spiritinvocation").getValue() == 0 then
			nSpiritForce = nSpiritForce * 2
		rRoll.sDesc = DB.findNode(sActiveSpirit).getChild("spiritname").getValue() .. " oposes binding with [FORCE: (" .. nSpiritForce ..")"
		elseif DB.findNode(sActiveSpirit).getChild("spiritbinding").getValue() == 1 and DB.findNode(sActiveSpirit).getChild("spiritinvocation").getValue() == 1 then
			nSpiritForce = nSpiritForce * 2
		rRoll.sDesc = DB.findNode(sActiveSpirit).getChild("spiritname").getValue() .. " oposes binding combined with invocation with [FORCE: (" .. nSpiritForce ..")"
		end
		for i=1, nSpiritForce do
			table.insert (rRoll.aDice, "d6");
		end

		rRoll.sType = "spiritisopposed"
		rRoll.bEdge = false
		rRoll.nEdgeValue = 0
		
	return rRoll
end

function getSpiritData(rActor, rRoll, nValue)
	if rActor.nodeCreature.getChild("node.activespirit") and rActor.nodeCreature.getChild("node.activespirit").getValue() ~="none" then
		local sActiveSpirit = rActor.nodeCreature.getChild("node.activespirit").getValue();
		local nSpiritForce = DB.findNode(sActiveSpirit).getChild("spirit.force").getValue();
		if DB.findNode(sActiveSpirit).getChild("spiritsummoning").getValue() == 1 then
			rRoll.sDesc = rRoll.sDesc .. " " .. DB.findNode(sActiveSpirit).getChild("spiritname").getValue().. " with " ;
		end
	end			
		nValue = nValue;
		return rRoll.sDesc, nValue;
end

function performRoll(draginfo, rActor, sStatName)
	local rRoll = {};
	local nValue = 0;
	local aDice =  {};
	rRoll.sDesc = rActor.nodeCreature.getChild("name").getValue() .. " -> "
	rRoll.sDesc = rRoll.sDesc .. "[SKILL-Check]"

	--pre Skill Roll Descriptions
	if rActor.sType == "pc" then
		if rActor.sWindowClass == "charsheet_3_1_weapons" then
			rRoll.sDesc = "[COMBAT] Check"
		elseif rActor.sWindowClass == "charsheet_4_1_spells" then
			rRoll.sDesc = "[MAGIC] Check"
		elseif rActor.sWindowClass == "charsheet_4_2_spirits" then
			if rActor.nodeCreature.getChild("node.activespirit") and rActor.nodeCreature.getChild("node.activespirit").getValue() ~="none" then
			rRoll.sDesc = "[SPIRIT] Check"
			else
			rRoll.sDesc = "[MAGIC] Check"
			end
		elseif rActor.sWindowClass == "charsheet_5_1_programs" then
			rRoll.sDesc = "[MATRIX] Check"
			rRoll.sType = "matrix"
		elseif rActor.sWindowClass == "charsheet_6_1_pilot" then
			rRoll.sDesc = "[PILOT] Check"
		end
	end
	
	if rRoll.sType == "matrix" and rActor.nodeCreature.getChild("node.activeprogram").getValue() ~="none" then
	rRoll.sDesc , nValue = ActorManager.getPureSkillScore(rActor, sStatName, rRoll.sDesc);
	else
	rRoll.sDesc , nValue = ActorManager.getSkillScore(rActor, sStatName, rRoll.sDesc);
	end
	-- get Additional Bonuses and Texts
	if rActor.sType == "pc" then
		if rActor.sWindowClass == "charsheet_3_1_weapons" then
			rRoll.sDesc, nValue = getWeaponData(rActor, rRoll,nValue);
		elseif rActor.sWindowClass == "charsheet_4_1_spells" then
			rRoll.sDesc, nValue = getSpellData(rActor, rRoll,nValue);
		elseif rActor.sWindowClass == "charsheet_4_2_spirits" then
			rRoll.sDesc, nValue = getSpiritData(rActor, rRoll,nValue);
		elseif rActor.sWindowClass == "charsheet_5_1_programs" then
			rRoll.sDesc, nValue = getProgramData(rActor, rRoll,nValue);
		elseif rActor.sWindowClass == "charsheet_6_1_pilot" then
		end
	end


	
	rRoll.sDesc , nValue = ActorManager.getPenalty(rActor, nValue, rRoll.sDesc, sStatName);
	rRoll.sDesc , nValue = ActorManager.getSustainingPenalty(rActor, nValue, rRoll.sDesc, sStatName);
	rRoll.sDesc , nValue = ActorManager.getArmorPenalty(rActor, nValue, rRoll.sDesc, sStatName);	
	rRoll.sDesc, nValue = ActorManager.getModifiers(rRoll.sDesc, nValue)
	rRoll.sDesc, rRoll.bEdge , rRoll.nEdgeVal = ActorManager.getEdge(rActor, rRoll.sDesc);
	nValue = nValue + rRoll.nEdgeVal
	
	-- finalize Roll (additional Texting)
	if rActor.sType == "pc" then
		if rActor.sWindowClass == "charsheet_3_1_weapons" then
			rRoll.sDesc = getFinalWeaponText(rActor, rRoll, nValue);
		elseif rActor.sWindowClass == "charsheet_4_1_spells" then
		elseif rActor.sWindowClass == "charsheet_3_1_weapons" then
		elseif rActor.sWindowClass == "charsheet_3_1_weapons" then
		end
	end
	
	
	-- SETUP
	for i = 1, nValue do
		table.insert (aDice, "d6");
	end
	rRoll.aDice = aDice;
	rRoll.nValue = 0;
	
	ActionsManager.performSingleRollAction(draginfo, rActor, "skill", rRoll);
end