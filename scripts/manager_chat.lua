-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

-- Initialization

function onInit()
	registerSlashHandler("/die", processDie);
	registerSlashHandler("/mod", processMod);
	registerSlashHandler("/flushdb", processFlushDB);

	registerSlashHandler("/ooc", processChatOOC);
	registerSlashHandler("/o", processChatOOC);
	registerSlashHandler("/emote", processChatEmote);
	registerSlashHandler("/e", processChatEmote);
	registerSlashHandler("/action", processChatAction);
	registerSlashHandler("/a", processChatAction);
	registerSlashHandler("/mood", processChatMood);
	registerSlashHandler("/m", processChatMood);
	
	
	if User.isHost() then
		registerSlashHandler("/story", processChatStory);
		registerSlashHandler("/s", processChatStory);
		registerSlashHandler("/clear", processChatClear);
	end

	if User.isHost() then
		registerSlashHandler("/day", processLightingDay);
		registerSlashHandler("/night", processLightingNight);
		registerSlashHandler("/lighting", processLighting);
		
		registerSlashHandler("/importchar", processImport);
		registerSlashHandler("/exportchar", processExport);
	end
end

-- Chat entry control registration for auto-complete
function registerEntryControl(ctrl)
	entrycontrol = ctrl;
	ctrl.onSlashCommand = onSlashCommand;
end

--
-- LAUNCH MESSAGES
--

launchmsg = {};

function registerLaunchMessage(msg)
	table.insert(launchmsg, msg);
end

function retrieveLaunchMessages()
	return launchmsg;
end


--
--
-- SLASH COMMAND HANDLER
--
--

slashhandlers = {};

function registerSlashHandler(command, callback)
	slashhandlers[command] = callback;
end

function unregisterSlashHandler(command, callback)
	slashhandlers[command] = nil;
end

function onSlashCommand(command, parameters)
	if string.len(command) > 1 then
		-- Check for exact match
		for c, h in pairs(slashhandlers) do
			if string.lower(c) == string.lower(command) then
				h(parameters);
				return;
			end
		end

		-- Check for unique partial match
		local fSlashCommand = nil;
		for c, h in pairs(slashhandlers) do
			if string.find(string.lower(c), string.lower(command), 1, true) == 1 then
				if fSlashCommand then
					fSlashCommand = nil;
					break;
				end
				fSlashCommand = h;
			end
		end
		if fSlashCommand then
			fSlashCommand(parameters);
			return;
		end
	end
	
	onSlashHelp();
end

function onSlashHelp()
	if User.isHost() then
		SystemMessage("SLASH COMMANDS [required] <optional>");
		SystemMessage("----------------");
		SystemMessage("BUILT-IN COMMANDS");
		SystemMessage("/console");
		SystemMessage("/reload");
		SystemMessage("/save");
		SystemMessage("----------------");
		SystemMessage("RULESET COMMANDS");
		SystemMessage("/clear");
		SystemMessage("/day");
		SystemMessage("/die [NdN+N] <message>");
		SystemMessage("/emote [message]");
		SystemMessage("/export");
		SystemMessage("/exportchar");
		SystemMessage("/exportchar [name]");
		SystemMessage("/flushdb");
		SystemMessage("/gmid [name]");
		SystemMessage("/identity [name]");
		SystemMessage("/importchar");
		SystemMessage("/lighting [RGB hex color]");
		SystemMessage("/mod [N] <message>");
		SystemMessage("/mood [mood] <message>");
		SystemMessage("/mood ([multiword mood]) <message>");
		SystemMessage("/ooc [message]");
		SystemMessage("/night");
		SystemMessage("/reply [message]");
		SystemMessage("/story [message]");
		SystemMessage("/vote <message>");
		SystemMessage("/whisper [character] [message]");
		SystemMessage("/w [character] [message]");
	else
		SystemMessage("SLASH COMMANDS [required] <optional>");
		SystemMessage("----------------");
		SystemMessage("BUILT-IN COMMANDS");
		SystemMessage("/console");
		SystemMessage("/save");
		SystemMessage("----------------");
		SystemMessage("RULESET COMMANDS");
		SystemMessage("/action [message]");
		SystemMessage("/die [NdN+N] <message>");
		SystemMessage("/emote [message]");
		SystemMessage("/mod [N] <message>");
		SystemMessage("/mood [mood] <message>");
		SystemMessage("/mood ([multiword mood]) <message>");
		SystemMessage("/ooc [message]");
		SystemMessage("/reply [message]");
		SystemMessage("/vote <message>");
		SystemMessage("/whisper GM [message]");
		SystemMessage("/whisper [character] [message]");
		SystemMessage("/w gm [message]");
		SystemMessage("/w [character] [message]");

	end
end


--
--
-- AUTO-COMPLETE
--
--

function searchForIdentity(sSearch)
	for _, sIdentity in ipairs(User.getAllActiveIdentities()) do
		local sLabel = User.getIdentityLabel(sIdentity);
		if string.find(string.lower(sLabel), string.lower(sSearch), 1, true) == 1 then
			if User.getIdentityOwner(sIdentity) then
				return sIdentity;
			end
		end
	end

	return nil;
end

function doAutocomplete()
	local buffer = entrycontrol.getValue();
	if buffer == "" then 
		return ;
	end

	-- Parse the string, adding one chunk at a time, looking for the maximum possible match
	local sReplacement = nil;
	local nStart = 2;
	while not sReplacement do
		local nSpace = string.find(string.reverse(buffer), " ", nStart, true);

		if nSpace then
			local sSearch = string.sub(buffer, #buffer - nSpace + 2);

			if not string.match(sSearch, "^%s$") then
				local sIdentity = searchForIdentity(sSearch);
				if sIdentity then
					local sRemainder = string.sub(buffer, 1, #buffer - nSpace + 1);
					sReplacement = sRemainder .. User.getIdentityLabel(sIdentity) .. " ";
					break;
				end
			end
		else
			local sIdentity = searchForIdentity(buffer);
			if sIdentity then
				sReplacement = User.getIdentityLabel(sIdentity) .. " ";
				break;
			end
			
			return;
		end

		nStart = nSpace + 1;
	end

	if sReplacement then
		entrycontrol.setValue(sReplacement);
		entrycontrol.setCursorPosition(#sReplacement + 1);
		entrycontrol.setSelectionPosition(#sReplacement + 1);
	end
end


--
--
-- DICE AND MOD SLASH HANDLERS
--
--
function DieControlThrow(type, bonus, name, custom, dice, charnode)
	if control then
		if type ~= "reroll" then
		custom = charnode
		end
		control.throwDice(type, dice, bonus, name, custom);
	end
end

function processDie(params)
	if User.isHost() then
		if params == "reveal" then
			OptionsManager.setOption("REVL", "on");
			SystemMessage("Revealing all die rolls");
			return;
		end
		if params == "hide" then
			OptionsManager.setOption("REVL", "off");
			SystemMessage("Hiding all die rolls");
			return;
		end
	end

	local sDice, sDesc = string.match(params, "%s*(%S+)%s*(.*)");
	
	if not StringManager.isDiceString(sDice) then
		SystemMessage("Usage: /die [dice] [description]");
		return;
	end
	
	local aDice, nValue = StringManager.convertStringToDice(sDice);
	
	local rRolls = {};
	local rRoll = { sType = "dice", sDesc = sDesc, aDice = aDice, nValue = nValue };
	table.insert(rRolls, rRoll);
	ActionsManager.handleActionNonDrag(nil, "dice", rRolls);
end

function processMod(params)
	local sMod, sDesc = string.match(params, "%s*(%S+)%s*(.*)");
	
	local nValue = tonumber(sMod);
	if not nValue then
		SystemMessage("Usage: /mod [number] [description]");
		return;
	end
	
	ModifierStack.addSlot(sDesc, nValue);
end

function processFlushDB(params)
	local rootnode = DB.findNode(".");
	if rootnode then
		rootnode.removeAllHolders(true);
	end
	SystemMessage("All client view-only access to database has been reset.");
end

function onChatNormal(msg)
	local bOptPCHT = OptionsManager.isOption("PCHT", "on");
	
	if User.isHost() then
		if bOptPCHT then
			msg.icon = "portrait_gm_token";
		end

		gmid, isgm = GmIdentityManager.getCurrent();
		msg.sender = gmid;
		if isgm then
			msg.font = "chatgmfont";
		else
			msg.font = "chatnpcfont";
		end
	else
		if bOptPCHT then
			local sCurrentId = User.getCurrentIdentity();
			if sCurrentId then
				msg.icon = "portrait_" .. sCurrentId .. "_chat";
			end
		end
	end
end

function processChatOOC(params)
	local msg = { mode = "ooc", text = params };

	onChatOOC(msg);
	
	Comm.deliverChatMessage(msg);
end

function onChatOOC(msg)
	msg.font = "oocfont";
	
	msg.sender = User.getUsername();
	if not User.isHost() then
		local sIdentity = User.getIdentityLabel();
		if sIdentity then
			msg.sender = msg.sender .. " (" .. sIdentity .. ")";
		end
	end
end

function processChatEmote(params)
	local msg = { mode = "emote", text = params };

	onChatEmote(msg);
	
	Comm.deliverChatMessage(msg);
end

function onChatEmote(msg)
	msg.font = "emotefont";

	if User.isHost() then
		local gmid, isgm = GmIdentityManager.getCurrent();
		if isgm then
			msg.sender = gmid;
		else
			msg.sender = "";
			msg.text = gmid .. " " .. msg.text;
		end
	else
		local sIdentity = User.getIdentityLabel();
		if sIdentity then
			msg.sender = "";
			msg.text = sIdentity .. " " .. msg.text;
		else
			msg.sender = User.getUsername();
		end
	end
end

function processChatAction(params)
	local msg = { mode = "act", text = params };

	onChatAction(msg);
	
	Comm.deliverChatMessage(msg);
end

function onChatAction(msg)
	if User.isHost() then
		msg.font = "narratorfont";
		msg.mode = "story";
	else
		msg.font = "emotefont";
		local sIdentity = User.getIdentityLabel();
		if sIdentity then
			msg.sender = sIdentity;
		else
			msg.sender = User.getUsername();
		end
	end
end

function processChatStory(params)
	if not User.isHost() then
		return;
	end
	
	local msg = { mode = "story", text = params };
	
	onChatStory(msg);
	
	Comm.deliverChatMessage(msg);
end

function onChatStory(msg)
	msg.sender = "";
	msg.font = "narratorfont";
end

function processChatMood(params)
	local sMood = string.match(params, "^%(.+%)");
	if not sMood then
		sMood = string.match(params, "^[^ ]+");
	end
	if not sMood then
		sMood = "";
	end
	
	local sText = string.sub(params, #sMood + 1);
	sText = StringManager.trimString(sText);

	if sText == "" then
		Comm.setChatMood(sMood);
	else
		local msg = { mode = "chat", mood = sMood, text = sText };
		
		onChatNormal(msg);
		
		Comm.deliverChatMessage(msg);
	end
end

function processChatClear(params)
	Comm.clearChat();
end

function processLightingDay(params)
	Interface.setLighting("FFFFFFFF", "FFFFFFFF", "FFFFFFFF", "FFFFFFFF");
end

function processLightingNight(params)
	Interface.setLighting("FFFFFFFF", "FFFFB8A4", "FFFFFFFF", "FFFFFFFF");
end

function processLighting(params)
	local sLighting = string.match(params, "^[0-9a-fA-F]+$");
	if not sLighting then
		SystemMessage("Invalid lighting value.\rUsage:\r/lighting [RGB hex value]");
		return;
	end
	if #sLighting ~= 6 then
		SystemMessage("Invalid lighting value.\rUsage:\r/lighting [RGB hex value]");
		return;
	end
	
	Interface.setLighting("FFFFFFFF", "FF" .. sLighting, "FFFFFFFF", "FFFFFFFF");
end

function processImport(params)
	local sFile = Interface.dialogFileOpen();
	if sFile then
		DB.import(sFile, "charsheet", "character");
	end
end

function processExport(params)
	local nodeChar = nil;
	
	local sFind = StringManager.trimString(params);
	if string.len(sFind) > 0 then
		local nodeCharacters = DB.findNode("charsheet");
		if nodeCharacters then
			for kChar, vChar in pairs(nodeCharacters.getChildren()) do
				local sChar = NodeManager.get(vChar, "name", "");
				if string.len(sChar) > 0 then
					if string.lower(sFind) == string.lower(string.sub(sChar, 1, string.len(sFind))) then
						nodeChar = vChar;
						break;
					end
				end
			end
		end
		if not nodeChar then
			SystemMessage("Unable to find character requested for export. (" .. params .. ")");
			return;
		end
	end
	
	local sFile = Interface.dialogFileSave();
	if sFile then
		if nodeChar then
			DB.export(sFile, nodeChar, "character");
			SystemMessage("Exported character: " .. NodeManager.get(nodeChar, "name", ""));
		else
			DB.export(sFile, "charsheet", "character", true);
			SystemMessage("Exported all characters");
		end
	end
end

--
--
-- MESSAGES
--
--

function createBaseMessage(rSource)
	-- Set up the basic message components
	local msg = {font = "systemfont", text = "", dicesecret = false};

	-- PORTRAIT CHAT?
	local bShowPortrait = false;
	if OptionsManager.isOption("PCHT", "on") then
		msg.icon = getPortraitChatIcon(rSource);
		bShowPortrait = true;
	end

	-- GET SOURCE ACTOR NAME
	local bShowActorName = false;
	if rSource then
		local sOptionShowRoll = OptionsManager.getOption("SHRL");
		if (sOptionShowRoll == "all") or ((sOptionShowRoll == "pc") and (rSource.sType == "pc")) then
			msg.text = rSource.sName .. " -> " .. msg.text;
			bShowActorName = true;
		end
	end
	
	-- DETERMINE WHETHER TO SHOW USER ID
	if User.isHost() then
		if not bShowActorName then
			local sGMID, bDefaultID = GmIdentityManager.getCurrent();
			
			if not bShowPortrait or not bDefaultID then
				msg.sender = sGMID;
			end
		end
	else
		if not bShowPortrait and not bShowActorName then
			msg.sender = User.getIdentityLabel();
		end
	end
	
	-- RESULTS
	return msg;
end

-- Get portrait icon
function getPortraitChatIcon(rSource)
	if User.isHost() then
		return "portrait_gm_token";
	else
		if rSource and rSource.sType == "pc" and rSource.nodeCreature then
			return "portrait_" .. rSource.nodeCreature.getName() .. "_chat";
		else
			local sIdentity = User.getCurrentIdentity();
			if sIdentity then
				return "portrait_" .. User.getCurrentIdentity() .. "_chat";
			end
		end
	end
	
	return nil;
end

-- Message: prints a message in the Chatwindow
function Message(msgtxt, broadcast, rActor)
	local msg = createBaseMessage(rActor);
	msg.text = msg.text .. msgtxt;

	if broadcast then
		Comm.deliverChatMessage(msg);
	else
		Comm.addChatMessage(msg);
	end
end

-- SystemMessage: prints a message in the Chatwindow
function SystemMessage(msgtxt)
	local msg = {font = "systemfont"};
	msg.text = msgtxt;
	Comm.addChatMessage(msg);
end
