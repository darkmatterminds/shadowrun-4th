-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

--
--  DATA STRUCTURES
--
-- rActor
--		sType
--		sName
--		nodeCreature
--		sCreatureNode
--		nodeCT
-- 		sCTNode
--

function getActor(sActorType, varActor)
	-- GET ACTOR NODE
	local nodeActor = nil;
	if type(varActor) == "string" then
		if varActor ~= "" then
			nodeActor = DB.findNode(varActor);
		end
	elseif type(varActor) == "databasenode" then
		nodeActor = varActor;
	end
	if not nodeActor then
		return nil;
	end

	-- BASED ON ORIGINAL ACTOR NODE, FILL IN THE OTHER INFORMATION
	local rActor = nil;
	if sActorType == "ct" then
		rActor = {};
		rActor.sType = NodeManager.get(nodeActor, "type", "npc");
		rActor.sName = NodeManager.get(nodeActor, "name", "");
		rActor.nodeCT = nodeActor;
		
		local nodeLink = nodeActor.getChild("link");
		if nodeLink then
			local sRefClass, sRefNode = nodeLink.getValue();
			if rActor.sType == "pc" and sRefClass == "charsheet" then
				rActor.nodeCreature = DB.findNode(sRefNode);
			elseif rActor.sType == "npc" and sRefClass == "npc" then
				rActor.nodeCreature = DB.findNode(sRefNode);
			end
		end

	elseif sActorType == "pc" then
		rActor = {};
		rActor.sType = "pc";
		rActor.nodeCreature = nodeActor;
		rActor.nodeCT = CombatCommon.getCTFromNode(nodeActor);
		rActor.sName = NodeManager.get(rActor.nodeCT or rActor.nodeCreature, "name", "");

	elseif sActorType == "npc" then
		rActor = {};
		rActor.sType = "npc";
		rActor.nodeCreature = nodeActor;
		
		-- IF ACTIVE CT IS THIS NPC TYPE, THEN ASSOCIATE
		local nodeActiveCT = CombatCommon.getActiveCT();
		if nodeActiveCT then
			local nodeLink = nodeActiveCT.getChild("link");
			if nodeLink then
				local sRefClass, sRefNode = nodeLink.getValue();
				if sRefNode == nodeActor.getNodeName() then
					rActor.nodeCT = nodeActiveCT;
				end
			end
		end
		-- OTHERWISE, ASSOCIATE WITH UNIQUE CT, IF POSSIBLE
		if not rActor.nodeCT then
			local nodeTracker = DB.findNode("combattracker");
			if nodeTracker then
				local bMatch = false;
				for keyEntry, nodeEntry in pairs(nodeTracker.getChildren()) do
					local nodeLink = nodeEntry.getChild("link");
					if nodeLink then
						local sRefClass, sRefNode = nodeLink.getValue();
						if sRefNode == nodeActor.getNodeName() then
							if bMatch then
								rActor.nodeCT = nil;
								break;
							end
							
							rActor.nodeCT = nodeEntry;
							bMatch = true;
						end
					end
				end
			end
		end
		
		rActor.sName = NodeManager.get(rActor.nodeCT or rActor.nodeCreature, "name", "");
	end
	
	-- TRACK THE NODE NAMES AS WELL
	if rActor.nodeCT then
		rActor.sCTNode = rActor.nodeCT.getNodeName();
	else
		rActor.sCTNode = "";
	end
	if rActor.nodeCreature then
		rActor.sCreatureNode = rActor.nodeCreature.getNodeName();
	else
		rActor.sCreatureNode = "";
	end
	
	-- RETURN ACTOR INFORMATION
	return rActor;
end

function getActorFromToken(token)
	local nodeCT = CombatCommon.getCTFromToken(token);
	if nodeCT then
		return getActor("ct", nodeCT.getNodeName());
	end
	
	return nil;
end

function getDropActors(sNodeType, nodename, draginfo)
	local rSource = decodeActors(draginfo);
	local rTarget = getActor(sNodeType, nodename);
	return rSource, rTarget;
end

function encodeActors(rSource, vTargets, bMultiTarget, nRerollSuccesses, nTotalDice)
	-- SETUP
	local aCustom = {};

	if nRerollSuccesses then
		aCustom.nRerollSuccesses = nRerollSuccesses
	end
	if nTotalDice then
		aCustom.nTotalDice = nTotalDice
	end

	-- ENCODE SOURCE ACTOR (CT, PC, NPC)
	if rSource then
		local sSourceType = "ct";
		local nodeSource = rSource.nodeCT;
		if not nodeSource then
			if rSource.sType == "pc" then
				sSourceType = "pc";
			elseif rSource.sType == "npc" then
				sSourceType = "npc";
			end
			nodeSource = rSource.nodeCreature;
		end
		if nodeSource then
			aCustom[sSourceType] = nodeSource;
		end
	end
	
	-- ENCODE TARGET ACTOR (CT, PC) (NO NPC TARGETING)
	if vTargets then
		local aTargets;
		if bMultiTarget then
			aTargets = vTargets;
		else
			aTargets = {};
			table.insert(aTargets, vTargets);
		end
		
		local sSourceType = "targetct";
		local aTargetNodes = {};
		for kTarget, rTarget in ipairs(aTargets) do
			if rTarget.nodeCT then
				table.insert(aTargetNodes, rTarget.nodeCT.getNodeName());
			elseif kTarget == 1 and rTarget.sType == "pc" and rTarget.nodeCreature then
				table.insert(aTargetNodes, rTarget.nodeCreature.getNodeName());
				sSourceType = "targetpc";
				break;
			end
		end		

		aCustom[sSourceType] = table.concat(aTargetNodes, "|");
	end
	

	-- RESULTS
	return aCustom;
end

function decodeActors(draginfo)
	-- SETUP
	local rSource = nil;
	local aTargets = {};
	local nRerollSuccesses = 0;
	local nTotalDice = 0;
	-- CHECK FOR DRAG INFORMATION
	if draginfo then
		local varCustom = draginfo.getCustomData();
		-- CHECK FOR CUSTOM DATA
		if varCustom then
			-- GET CUSTOM SOURCE ACTOR
			if varCustom["ct"] then
				rSource = getActor("ct", varCustom["ct"]);
			elseif varCustom["pc"] then
				rSource = getActor("pc", varCustom["pc"]);
			elseif varCustom["npc"] then
				rSource = getActor("npc", varCustom["npc"]);
			end
			
			-- GET CUSTOM TARGET ACTOR
			if varCustom["targetct"] then
				local aTargetNodeNames = StringManager.split(varCustom["targetct"], "|");
				for kName, vName in ipairs(aTargetNodeNames) do
					local rTarget = getActor("ct", vName);
					if rTarget then
						table.insert(aTargets, rTarget);
					end
				end
			elseif varCustom["targetpc"] then
				table.insert(aTargets, getActor("pc", varCustom["targetpc"]));
			end
			if varCustom["nRerollSuccesses"] then
				nRerollSuccesses = varCustom.nRerollSuccesses
			end
			if varCustom ["nTotalDice"] then
				nTotalDice = varCustom.nTotalDice
			end
		end
		
		-- IF NO CUSTOM DATA FOR SOURCE, THEN TRY THE SHORTCUT DATA
		if not rSource then
			-- GET SHORTCUT SOURCE ACTOR
			local sRefClass, sRefNode = draginfo.getShortcutData();
			if sRefClass and sRefNode then
				if sRefClass == "combattracker_entry" then
					rSource = getActor("ct", sRefNode);

				elseif sRefClass == "charsheet" then
					rSource = getActor("pc", sRefNode);

				elseif sRefClass == "npc" then
					rSource = getActor("npc", sRefNode);
				end
			end
		end
	end
	
	-- RESULTS
	return rSource, aTargets, nRerollSuccesses, nTotalDice;
end

function getPercentWounded(sNodeType, node)
	local nHP = 0;
	local nTemp = 0;
	local nWounds = 0;
	local nNonlethal = 0;
	
	if sNodeType == "ct" then
		nHP = NodeManager.get(node, "hp", 0);
		nTemp = NodeManager.get(node, "hptemp", 0);
		nWounds = NodeManager.get(node, "wounds", 0);
		nNonlethal = NodeManager.get(node, "nonlethal", 0);
	elseif sNodeType == "pc" then
		nHP = NodeManager.get(node, "hp.total", 0);
		nTemp = NodeManager.get(node, "hp.temporary", 0);
		nWounds = NodeManager.get(node, "hp.wounds", 0)
		nNonlethal = NodeManager.get(node, "hp.nonlethal", 0);
	end
	
	local nPercentWounded = 0;
	local nPercentNonlethal = 0;
	if nHP > 0 then
		nPercentWounded = nWounds / nHP;

		if nTemp > 0 then
			nPercentNonlethal = (nWounds + nNonlethal) / (nHP + nTemp);
		else
			nPercentNonlethal = (nWounds + nNonlethal) / nHP;
		end
	end
	
	return nPercentWounded, nPercentNonlethal;
end

function getattributeEffectsBonus(rActor, sAttribute)
	if not rActor or not sAttribute then
		return 0, 0;
	end
	
	local sAttributeEffect = DataCommon.attribute_ltos[sAttribute];
	if not sAttributeEffect then
		return 0, 0;
	end
	
	local nattributeMod, nattributeEffects = EffectsManager.getEffectsBonus(rActor.nodeCT, sAttributeEffect, true);
	
	if EffectsManager.hasEffectCondition(rActor, "Entangled") and sAttribute == "dexterity" then
		nattributeMod = nattributeMod - 4;
		nattributeEffects = nattributeEffects + 1;
	end
	if sAttribute == "dexterity" or sAttribute == "strength" then
		if EffectsManager.hasEffectCondition(rActor, "Exhausted") then
			nattributeMod = nattributeMod - 6;
			nattributeEffects = nattributeEffects + 1;
		elseif EffectsManager.hasEffectCondition(rActor, "Fatigued") then
			nattributeMod = nattributeMod - 2;
			nattributeEffects = nattributeEffects + 1;
		end
	end
	
	local nattributeScore = getAttributeScore(rActor, sAttribute);
	if nattributeScore > 0 then
		local nAffectedScore = math.max(nattributeScore + nattributeMod, 0);
		
		local nCurrentBonus = math.floor((nattributeScore - 10) / 2);
		local nAffectedBonus = math.floor((nAffectedScore - 10) / 2);
		
		nattributeMod = nAffectedBonus - nCurrentBonus;
	else
		if nattributeMod > 0 then
			nattributeMod = math.floor(nattributeMod / 2);
		else
			nattributeMod = math.ceil(nattributeMod / 2);
		end
	end

	return nattributeMod, nattributeEffects;
end

function getSkillScore(rActor, sStatName, sDesc)
	if not rActor or not rActor.nodeCreature or not sStatName then
		return 0;
	end
	local sStat = sStatName
	local nStatScore = 0;
	
	if rActor.sType == "npc" then
	
	elseif rActor.sType == "pc" then
			nStatScore = NodeManager.get(rActor.nodeSkill, "skillcheck", 0);
	elseif rActor.sType == "ct" then
	
	end
	
	sDesc = sDesc .." " .. sStat .. " (" .. nStatScore .. ") "

	return sDesc, nStatScore;
end

function getPureSkillScore(rActor, sStatName, sDesc)
	if not rActor or not rActor.nodeCreature or not sStatName then
		return 0;
	end
	local sStat = sStatName
	local nStatScore = 0;
	
	if rActor.sType == "npc" then
	
	elseif rActor.sType == "pc" then
			nStatScore = NodeManager.get(rActor.nodeSkill, "skillbase", 0);
			nStatScore = nStatScore + NodeManager.get(rActor.nodeSkill, "skillbonus", 0);
	elseif rActor.sType == "ct" then
	
	end
	
	sDesc = sDesc .." " .. sStat .. " (" .. nStatScore .. ") "

	return sDesc, nStatScore;
end


function getResistScore(rActor, sStatName, sDesc)
	if not rActor or not rActor.nodeCreature or not sStatName then
		return 0;
	end
	local sStat = sStatName
	local nStatScore = 0;
	local nModifier = 0;
	if rActor.sType == "npc" then
	
	elseif rActor.sType == "pc" then
		nModifier = NodeManager.get(rActor.nodeCreature, "armor.hiddenbalisticvalue", 0);
		if sStatName == "Balistic Armor" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.special.balisticvalue", 0);
			if NodeManager.get(rActor.nodeCreature, "armorcheckmode", 0) == 1 then
				nModifier = math.ceil (NodeManager.get(rActor.nodeCreature, "armor.hiddenbalisticvalue", 0) / 2);
				nStatScore = nStatScore - nModifier
				sDesc = "(half Armor -"..nModifier..")"
			end
		elseif sStatName == "Impact Armor" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.special.impactvalue", 0);
			if NodeManager.get(rActor.nodeCreature, "armorcheckmode", 0) == 1 then
				nModifier = math.ceil (NodeManager.get(rActor.nodeCreature, "armor.hiddenimpactvalue", 0) / 2);
				nStatScore = nStatScore - nModifier
				sDesc = "(half Armor -"..nModifier..")"
			end
		elseif sStatName == "resist Drain" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.special.drainresistvalue", 0);
		end
	elseif rActor.sType == "ct" then
	
	end
		sDesc = "[Resist-Check] " .. sStat .. " (" .. nModifier .. ") Resist Value (".. nStatScore .. ") ".. sDesc

	return sDesc, nStatScore;
end


-- Checking if Edge is active
function getEdge(rActor, sDesc)
	local nEdgeVal = 0
	
	if not rActor or not rActor.nodeCreature then
		return false;
	end
	bEdge = false;
	local check = 0;
	
	if rActor.sType == "npc" then
	elseif rActor.sType == "pc" then
		check = NodeManager.get(rActor.nodeCreature, "edgecheckbox",0);
	elseif rActor.nodeCT then
	end
	
	if check == 1 then
		bEdge = true;
	else
		bEdge = false;
	end
	
	if bEdge == true then
		if NodeManager.get(rActor.nodeCreature, "base.attribute.edge.check",0) <= 0 then
		nEdgeVal = 0;
		sDesc = "[Warnung! - kein Edge mehr" .. " " .. sDesc;
		DB.findNode(rActor.sCreatureNode).getChild("edgecheckbox").setValue(0);
		else
		sDesc = "[EDGE]".." "..sDesc;
		nEdgeVal = NodeManager.get(rActor.nodeCreature, "base.attribute.edge.score",0);
		end
	end 
	
	return sDesc, bEdge, nEdgeVal;
end



function getSpecialAttributeScore(rActor, sSpecialAttribute, sDesc)
	if not rActor or not rActor.nodeCreature or not sSpecialAttribute then
		return 0;
	end
	local sStat = sSpecialAttribute
	local nStatScore = 0;
	if rActor.sType == "npc" then
	
	elseif rActor.sType == "pc" then
		if sSpecialAttribute == "Composure" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.composure.check", 0);
		elseif sSpecialAttribute == "Memory" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.memory.check", 0);
		elseif sSpecialAttribute == "Judge" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.judge.check", 0);
		elseif sSpecialAttribute == "Lift" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.lift.check", 0);
		end
	elseif rActor.sType == "ct" then
	
	end
		sDesc = sDesc .. "[Spec.-ATTRIBUTE-Check] " .. sStat .. " (" .. nStatScore .. ") "
	return sDesc, nStatScore;
end


function getAttributeScore(rActor, sAttribute, sDesc)
	if not rActor or not rActor.nodeCreature or not sAttribute then
		return 0;
	end
	
	local nStatScore = 0;
	if rActor.sType == "npc" then
		if sAttribute == "Body" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.body.check", 0);
		elseif sAttribute == "Agility" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.agility.check", 0);
		elseif sAttribute == "Reaction" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.reaction.check", 0);
		elseif sAttribute == "Strength" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.strength.check", 0);
		elseif sAttribute == "Willpower" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.willpower.check", 0);
		elseif sAttribute == "Logic" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.logic.check", 0);
		elseif sAttribute == "Intuition" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.intuition.check", 0);
		elseif sAttribute == "Charisma" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.charisma.check", 0);
		end
	elseif rActor.sType == "pc" then
		if sAttribute == "Body" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.body.check", 0);
		elseif sAttribute == "Agility" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.agility.check", 0);
		elseif sAttribute == "Reaction" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.reaction.check", 0);
		elseif sAttribute == "Strength" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.strength.check", 0);
		elseif sAttribute == "Willpower" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.willpower.check", 0);
		elseif sAttribute == "Logic" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.logic.check", 0);
		elseif sAttribute == "Intuition" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.intuition.check", 0);
		elseif sAttribute == "Charisma" then
			nStatScore = NodeManager.get(rActor.nodeCreature, "base.attribute.charisma.check", 0);
		end
	elseif rActor.sType == "ct" then
	
	end
	return nStatScore;
end

function getAttributeBonus(rActor, sAttribute, sDesc)

	-- VALIDATE
	if not rActor or not rActor.nodeCreature or not sAttribute then
		return 0;
	end

	-- SETUP
	local sStat = sAttribute;
	local nStatVal = 0;
	
	-- GET base.attribute VALUE
	local nStatScore = getAttributeScore(rActor, sStat);
	if nStatScore < 0 then
		return 0;
	end
	-- Get Roll Normal, Default, Double	
	local sNDD = "1"  -- Normal Double or Default Checkmode
	sNDD = NodeManager.get(rActor.nodeCreature, "checkmode", 1);
	if sNDD ~="1" and sNDD ~="2" and sNDD ~="3" then 
		sNDD = "1"
	end
	if sNDD == "1" then
	nStatVal = nStatScore;
	sDesc = sDesc .. "[ATTRIBUTE-Check] " .. sStat .. " (" .. nStatVal .. ") "
	elseif sNDD == "2" then
	nStatVal = nStatScore - 1;
	sDesc = sDesc .."[DEFAULT-Check]" .. sStat.. "(" .. nStatVal .. ") "
	elseif sNDD == "3" then
	nStatVal = nStatScore * 2;
	sDesc = sDesc .."[DOUBLE-Check]" .. sStat.. "(" .. nStatVal .. ") "
	end	
	 if rActor.sType == "npc" then
		sDesc = rActor.sName .. ": " .. sDesc
	 end
	
	return sDesc, nStatVal
end

function getSustainingPenalty(rActor, nStatVal, sDesc, sStatName)
	if not StringManager.contains(DataCommon.sustainingpenalty, sStatName) then
		local sustainingpenalty = 0;
		sustainingpenalty = NodeManager.get(rActor.nodeCreature, "base.special.spellsustainedpenalty", 0);
		nStatVal = nStatVal + sustainingpenalty
			if sustainingpenalty < 0 then
			sDesc = sDesc .. " (SUST-Penalty: ".. sustainingpenalty .. ")"
			end
	end
	return sDesc, nStatVal;	
end


function getPenalty(rActor, nStatVal, sDesc, sStatName)
	if not StringManager.contains(DataCommon.damagepenalty, sStatName) then
		local damagepenalty = 0;
		damagepenalty = NodeManager.get(rActor.nodeCreature, "damage.all.modifier", 0);
		nStatVal = nStatVal + damagepenalty
		if damagepenalty < 0 then
		sDesc = sDesc .. " (DMG-Penalty: ".. damagepenalty .. ")"
		end
	end
	return sDesc, nStatVal;
end

function getArmorPenalty(rActor, nStatVal, sDesc, sStat)
	local armorpenalty = 0;
	if sStat == "Agility" or sStat == "Reaction" then
	armorpenalty = NodeManager.get(rActor.nodeCreature, "armor.penalty", 0);
	nStatVal = nStatVal + armorpenalty
		if armorpenalty < 0 then
			sDesc = sDesc .. " (ENC-Penalty ".. armorpenalty .. ")"
		end
	end

	return sDesc, nStatVal;
end

function getModifiers(sDesc, nStatVal)
	local sStackDesc, nStackMod = ModifierStack.getStack();
		
		if sStackDesc ~= "" then
			if sDesc ~= "" then
				sDesc = sDesc .. " other Modifiers(" .. sStackDesc .. ")";
			else
				sDesc = sDesc .. " " .. sStackDesc;
			end
		end
		nStatVal = nStatVal + nStackMod
	-- RESULTS
	return sDesc , nStatVal;
end

