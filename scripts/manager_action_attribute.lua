-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	ActionsManager.registerModHandler("attribute", modRoll);
end

function performRoll(draginfo, rActor, sAttributeStat)
	local rRoll = {};
	local nValue = 0;
	local aDice =  {};
	rRoll.sDesc = rActor.nodeCreature.getChild("name").getValue() .. " -> "
	rRoll.sDesc , nValue = ActorManager.getAttributeBonus(rActor, sAttributeStat, rRoll.sDesc);
	rRoll.sDesc , nValue = ActorManager.getPenalty(rActor, nValue, rRoll.sDesc);
	rRoll.sDesc , nValue = ActorManager.getSustainingPenalty(rActor, nValue, rRoll.sDesc);
	rRoll.sDesc , nValue = ActorManager.getArmorPenalty(rActor, nValue, rRoll.sDesc, sAttributeStat);	
	rRoll.sDesc, nValue = ActorManager.getModifiers(rRoll.sDesc, nValue)
	rRoll.sDesc, rRoll.bEdge , rRoll.nEdgeVal = ActorManager.getEdge(rActor, rRoll.sDesc);
	nValue = nValue + rRoll.nEdgeVal
	-- SETUP
	for i = 1, nValue do
		table.insert (aDice, "d6");
	end
	rRoll.aDice = aDice;
	rRoll.nValue = 0;
	
	ActionsManager.performSingleRollAction(draginfo, rActor, "attribute", rRoll);
end