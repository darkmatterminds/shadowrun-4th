-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

OOB_MSGTYPE_APPLYEFF = "applyeff";
OOB_MSGTYPE_EXPIREEFF = "expireeff";

local bLocked = false;
local aUsedActionEffects = {};

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_APPLYEFF, handleApplyEffect);
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_EXPIREEFF, handleExpireEffect);
end

function handleApplyEffect(msgOOB)
	-- Get the combat tracker node
	local nodeCTEntry = DB.findNode(msgOOB.sTargetNode);
	if not nodeCTEntry then
		ChatManager.SystemMessage("[ERROR] Unable to resolve CT effect application on node = " .. msgOOB.sTargetNode);
		return;
	end
	
	-- Reconstitute the effect details
	local rEffect = {};
	rEffect.sName = msgOOB.sName;
	rEffect.nDuration = tonumber(msgOOB.nDuration) or 1;
	rEffect.sUnits = msgOOB.sUnits;
	rEffect.nInit = tonumber(msgOOB.nInit) or 0;
	rEffect.sSource = msgOOB.sSource;
	rEffect.nGMOnly = tonumber(msgOOB.nGMOnly) or 0;
	rEffect.sApply = msgOOB.sApply;
	
	-- Apply the damage
	addEffect(msgOOB.user, msgOOB.identity, nodeCTEntry, rEffect, true);
end

function handleExpireEffect(msgOOB)
	-- Get the effect and combat tracker node
	local nodeEffect = DB.findNode(msgOOB.sEffectNode);
	if not nodeEffect then
		ChatManager.SystemMessage("[ERROR] Unable to find effect to remove = " .. msgOOB.sEffectNode);
		return;
	end
	local nodeActor = nodeEffect.getChild("...");
	if not nodeActor then
		ChatManager.SystemMessage("[ERROR] Unable to find actor to remove effect from = " .. msgOOB.sEffectNode);
		return;
	end
	
	-- Get the parameters
	local nExpireType = tonumber(msgOOB.nExpireType) or 0;
	
	-- Apply the damage
	expireEffect(nodeActor, nodeEffect, nExpireType);
end

function notifyExpire(varEffect, nMatch)
	if type(varEffect) == "databasenode" then
		varEffect = varEffect.getNodeName();
	elseif type(varEffect) ~= "string" then
		return;
	end
	
	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_EXPIREEFF;
	msgOOB.sEffectNode = varEffect;
	msgOOB.nExpireType = nMatch;
	
	Comm.deliverOOBMessage(msgOOB, "");
end

--
-- EFFECTS
--

function message(sMsg, nodeCTEntry, gmflag, target)
	-- ADD NAME OF CT ENTRY TO NOTIFICATION
	if nodeCTEntry then
		sMsg = sMsg .. " [on " .. NodeManager.get(nodeCTEntry, "name", "") .. "]";
	end

	-- BUILD MESSAGE OBJECT
	local msg = {font = "msgfont", icon = "indicator_effect", text = sMsg};
	
	-- DELIVER MESSAGE BASED ON TARGET AND GMFLAG
	if target then
		if msguser == "" then
			Comm.addChatMessage(msg);
		else
			Comm.deliverChatMessage(msg, msguser);
		end
	elseif gmflag then
		msg.text = "[GM] " .. msg.text;
		if User.isHost() then
			Comm.addChatMessage(msg);
		else
			Comm.deliverChatMessage(msg, User.getUsername());
		end
	else
		Comm.deliverChatMessage(msg);
	end
end

function parseEffect(s)
	local aEffects = {};
	
	local sEffectClause;
	for sEffectClause in string.gmatch(s, "([^;]*);?") do
		local aWords = StringManager.parseWords(sEffectClause, "%[%]:");
		if #aWords > 0 then
			local sType = string.match(aWords[1], "^([^:]+):");
			local aDice = {};
			local nValue = 0;
			local aRemainder = {};
			local nRemainderIndex = 1;
			if sType then
				nRemainderIndex = 2;

				local sValueCheck = "";
				local sTypeRemainder = string.sub(aWords[1], #sType + 2);
				if sTypeRemainder == "" then
					sValueCheck = aWords[2] or "";
					nRemainderIndex = nRemainderIndex + 1;
				else
					sValueCheck = sTypeRemainder;
				end
				
				if StringManager.isDiceString(sValueCheck) then
					aDice, nValue = StringManager.convertStringToDice(sValueCheck);
				elseif sValueCheck ~= "" then
					table.insert(aRemainder, sValueCheck);
				end
			end
			
			for i = nRemainderIndex, #aWords do
				table.insert(aRemainder, aWords[i]);
			end

			table.insert(aEffects, {type = sType or "", mod = nValue, dice = aDice, remainder = aRemainder});
		end
	end
	
	return aEffects;
end

function rebuildParsedEffect(aEffectComps)
	local aEffect = {};
	
	for kComp, rComp in ipairs(aEffectComps) do
		local aComp = {};

		if rComp.type ~= "" then
			table.insert(aComp, rComp.type .. ":");
		end

		local sDiceString = StringManager.convertDiceToString(rComp.dice, rComp.mod);
		if sDiceString ~= "" then
			table.insert(aComp, sDiceString);
		end
		
		for kRemainder, sRemainder in ipairs(rComp.remainder) do
			table.insert(aComp, sRemainder);
		end
		
		table.insert(aEffect, table.concat(aComp, " "));
	end
	
	return table.concat(aEffect, "; ");
end

function getEffectsString(nodeCTEntry, bPublicOnly)
	-- Make sure we can get to the effects list
	local nodeEffects = NodeManager.createChild(nodeCTEntry, "effects");
	if not nodeEffects then
		return "";
	end

	-- Start with an empty effects list string
	local aOutputEffects = {};
	
	-- Iterate through each effect
	for k,v in pairs(nodeEffects.getChildren()) do
		if NodeManager.get(v, "isactive", 0) ~= 0 then
			local sLabel = NodeManager.get(v, "label", "");

			local bAddEffect = true;
			local bGMOnly = false;
			if sLabel == "" then
				bAddEffect = false;
			elseif NodeManager.get(v, "isgmonly", 0) == 1 then
				if User.isHost() and not bPublicOnly then
					bGMOnly = true;
				else
					bAddEffect = false;
				end
			end

			if bAddEffect then
				local aAddCompList = {};
				local bTargeted = false;
				local aEffectComps = EffectsManager.parseEffect(sLabel);
				for kComp, vComp in ipairs(aEffectComps) do
					if vComp.remainder[1] == "TRGT" then
						bTargeted = true;
					else
						table.insert(aAddCompList, vComp);
					end
				end
				
				if isTargetedEffect(v) then
					local sTargets = table.concat(getEffectTargets(v, true), ",");
					table.insert(aAddCompList, 1, {type = "", mod = 0, dice = {}, remainder = {"[TRGT: " .. sTargets .. "]"}});
				elseif bTargeted then
					table.insert(aAddCompList, 1, {type = "", mod = 0, dice = {}, remainder = {"[TRGT]"}});
				end

				local sApply = NodeManager.get(v, "apply", "");
				if sApply == "action" then
					table.insert(aAddCompList, 1, {type = "", mod = 0, dice = {}, remainder = {"[ACTN]"}});
				elseif sApply == "roll" then
					table.insert(aAddCompList, 1, {type = "", mod = 0, dice = {}, remainder = {"[ROLL]"}});
				elseif sApply == "single" then
					table.insert(aAddCompList, 1, {type = "", mod = 0, dice = {}, remainder = {"[SNG]"}});
				end
				
				local nDuration = NodeManager.get(v, "duration", 0);
				if nDuration > 0 then
					table.insert(aAddCompList,  {type = "", mod = 0, dice = {}, remainder = {"[D: " .. nDuration .. "]"}});
				end
				
				local sOutputLabel = rebuildParsedEffect(aAddCompList);
				if bGMOnly then
					sOutputLabel = "(" .. sOutputLabel .. ")";
				end

				table.insert(aOutputEffects, sOutputLabel);
			end
		end
	end
	
	-- Return the final effect list string
	return table.concat(aOutputEffects, " | ");
end

function isGMEffect(nodeActor, nodeEffect)
	local bGMOnly = false;
	if nodeEffect then
		bGMOnly = (NodeManager.get(nodeEffect, "isgmonly", 0) == 1);
	end
	if nodeActor then
		if (NodeManager.get(nodeActor, "type", "") ~= "pc") and 
				(NodeManager.get(nodeActor, "show_npc", 0) == 0) then
			bGMOnly = true;
		end
	end
	return bGMOnly;
end

function isTargetedEffect(nodeEffect)
	local bTargeted = false;

	if nodeEffect then
		local nodeTargetList = nodeEffect.getChild("targets");
		if nodeTargetList then
			if nodeTargetList.getChildCount() > 0 then
				bTargeted = true;
			end
		end
	end

	return bTargeted;	
end

function getEffectTargets(nodeEffect, bUseName)
	local aTargets = {};
	
	if nodeEffect then
		local nodeTargetList = nodeEffect.getChild("targets");
		if nodeTargetList then
			for keyTarget, nodeTarget in pairs(nodeTargetList.getChildren()) do
				local sNode = NodeManager.get(nodeTarget, "noderef", "");
				if bUseName then
					local nodeTargetCT = DB.findNode(sNode);
					table.insert(aTargets, NodeManager.get(nodeTargetCT, "name", ""));
				else
					table.insert(aTargets, sNode);
				end
			end
		end
	end

	return aTargets;
end

function removeEffect(nodeCTEntry, sEffPatternToRemove)
	-- VALIDATE
	if not nodeCTEntry or not sEffPatternToRemove then
		return;
	end

	-- GET EFFECTS LIST
	local nodeEffectsList = NodeManager.createChild(nodeCTEntry, "effects");
	if not nodeEffectsList then
		return;
	end

	-- COMPARE EFFECT NAMES TO EFFECT TO REMOVE
	for keyEffect, nodeEffect in pairs(nodeEffectsList.getChildren()) do
		local s = NodeManager.get(nodeEffect, "label", "");
		if string.match(s, sEffPatternToRemove) then
			nodeEffect.delete();
			return;
		end
	end
end

function addEffect(sUser, sIdentity, nodeCT, rNewEffect, bShowMsg)
	-- VALIDATE
	if not nodeCT or not rNewEffect or not rNewEffect.sName then
		return;
	end
	rNewEffect.nDuration = rNewEffect.nDuration or 1;
	if rNewEffect.sUnits == "minute" then
		rNewEffect.nDuration = rNewEffect.nDuration * 10;
	elseif rNewEffect.sUnits == "hour" or rNewEffect.sUnits == "day" then
		rNewEffect.nDuration = 0;
	end
	rNewEffect.nInit = rNewEffect.nInit or 0;
	
	-- GET EFFECTS LIST
	local sCTName = NodeManager.get(nodeCT, "name", "");
	local nodeEffectsList = NodeManager.createChild(nodeCT, "effects");
	if not nodeEffectsList then
		return;
	end
	
	-- PARSE NEW EFFECT
	local aNewEffectParse = parseEffect(rNewEffect.sName);
	
	-- CHECKS TO IGNORE NEW EFFECT (DUPLICATE, SHORTER, WEAKER)
	local nodeTargetEffect = nil;
	for k, v in pairs(nodeEffectsList.getChildren()) do
		-- CHECK FOR DUPLICATE EFFECT (ONLY IF LABEL, INIT AND DURATION ARE THE SAME)
		if (NodeManager.get(v, "label", "") == rNewEffect.sName) and 
				(NodeManager.get(v, "init", 0) == rNewEffect.nInit) and
				(NodeManager.get(v, "duration", 0) == rNewEffect.nDuration) then
			local sMsg = "Effect ['" .. rNewEffect.sName .. "'] ";
			if rNewEffect.nDuration > 0 then
				sMsg = sMsg .. " [D:" .. rNewEffect.nDuration .. "] ";
			end
			sMsg = sMsg .. "-> [ALREADY EXISTS]";
			message(sMsg, nodeCT, false, sUser);
			return;
		end
	end
	
	-- FILL EMPTY EFFECT, OR CREATE NEW ONE
	if not nodeTargetEffect then
		for k, v in pairs(nodeEffectsList.getChildren()) do
			if NodeManager.get(v, "label", "") == "" then
				nodeTargetEffect = v;
				break;
			end
		end
	end
	if not nodeTargetEffect then
		nodeTargetEffect = NodeManager.createChild(nodeEffectsList);
		if not nodeTargetEffect then
			return;
		end
	end
	
	-- ADD EFFECT DETAILS
	NodeManager.set(nodeTargetEffect, "label", "string", rNewEffect.sName);
	NodeManager.set(nodeTargetEffect, "duration", "number", rNewEffect.nDuration);
	NodeManager.set(nodeTargetEffect, "init", "number", rNewEffect.nInit);
	NodeManager.set(nodeTargetEffect, "isgmonly", "number", rNewEffect.nGMOnly);
	if rNewEffect.sApply then
		NodeManager.set(nodeTargetEffect, "apply", "string", rNewEffect.sApply);
	end

	-- BUILD MESSAGE
	if bShowMsg then
		local msg = {font = "msgfont", icon = "indicator_effect"};
		msg.text = "Effect ['" .. rNewEffect.sName .. "'] ";
		if rNewEffect.nDuration > 0 then
			msg.text = msg.text .. " [D:" .. rNewEffect.nDuration .. "] ";
		end
		msg.text = msg.text .. "-> [to " .. sCTName .. "]";
		
		-- HANDLE APPLIED BY SETTING
		if rNewEffect.sSource and rNewEffect.sSource ~= "" then
			NodeManager.set(nodeTargetEffect, "source_name", "string", rNewEffect.sSource);
			msg.text = msg.text .. " [by " .. NodeManager.get(DB.findNode(rNewEffect.sSource), "name", "") .. "]";
		end
		
		-- SEND MESSAGE
		if isGMEffect(nodeCT, nodeTargetEffect) then
			if sUser == "" then
				msg.text = "[GM] " .. msg.text;
				Comm.addChatMessage(msg);
			elseif sUser ~= "" then
				Comm.addChatMessage(msg);
				Comm.deliverChatMessage(msg, sUser);
			end
		else
			Comm.deliverChatMessage(msg);
		end
	end
end

-- MAKE SURE AT LEAST ONE EFFECT REMAINS AFTER DELETING
function deleteEffect(nodeEffect)
	if not nodeEffect then
		return;
	end
	
	local nodeEffectList = nodeEffect.getParent();
	nodeEffect.delete();
	if nodeEffectList then
		if nodeEffectList.getChildCount() == 0 then
			NodeManager.createChild(nodeEffectList);
		end
	end
end

function expireEffect(nodeActor, nodeEffect, nExpireComponent, bOverride)
	-- VALIDATE
	if not nodeEffect then
		return false;
	end

	-- PARSE THE EFFECT
	local sEffect = NodeManager.get(nodeEffect, "label", "");

	-- DETERMINE MESSAGE VISIBILITY
	local bGMOnly = isGMEffect(nodeActor, nodeEffect);

	-- CHECK FOR PARTIAL EXPIRATION
	if nExpireComponent > 0 then
		local listEffectComp = parseEffect(sEffect);
		if #listEffectComp > 1 then
			table.remove(listEffectComp, nExpireComponent);

			local sNewEffect = rebuildParsedEffect(listEffectComp);
			NodeManager.set(nodeEffect, "label", "string", sNewEffect);

			message("Effect ['" .. sEffect .. "'] -> [SINGLE MOD USED]", nodeActor, bGMOnly);
			return true;
		end
	end
	
	-- DELETE THE EFFECT
	sMsg = "Effect ['" .. sEffect .. "'] -> [EXPIRED]";
	deleteEffect(nodeEffect);

	-- SEND NOTIFICATION TO THE HOST
	message(sMsg, nodeActor, bGMOnly);
	return true;
end

function applyOngoingDamageAdjustment(nodeActor, nodeEffect, rEffectComp)
	-- EXIT IF EMPTY FHEAL OR DMGO
	if #(rEffectComp.dice) == 0 and rEffectComp.mod == 0 then
		return;
	end
	
	-- BUILD MESSAGE
	local aResults = {};
	if isGMEffect(nodeActor, nodeEffect) then
		table.insert(aResults, "[GM]");
	end
	if rEffectComp.type == "FHEAL" then
		-- MAKE SURE AFFECTED ACTOR IS WOUNDED
		if NodeManager.get(nodeActor, "wounds", 0) == 0 and NodeManager.get(nodeActor, "nonlethal", 0) == 0 then
			return;
		end
		
		table.insert(aResults, "[FHEAL] Fast Heal");

	elseif rEffectComp.type == "REGEN" then
		-- MAKE SURE AFFECTED ACTOR HAS NONLETHAL WOUNDS
		if NodeManager.get(nodeActor, "nonlethal", 0) == 0 then
			return;
		end
		
		table.insert(aResults, "[REGEN] Regeneration");

	else
		table.insert(aResults, "[DAMAGE] Ongoing Damage");

		if #(rEffectComp.remainder) > 0 then
			table.insert(aResults, "[TYPE: " .. string.lower(table.concat(rEffectComp.remainder, ",")) .. "]");
		end
	end

	-- MAKE ROLL AND APPLY RESULTS
	local rTarget = ActorManager.getActor("ct", nodeActor);
	local rRoll = { sType = "damage", sDesc = table.concat(aResults, " "), aDice = rEffectComp.dice, nValue = rEffectComp.mod };
	ActionsManager.roll(nil, rTarget, rRoll);
end

function processEffects(nodeActorList, nodeCurrentActor, nodeNewActor)
	if not nodeActorList then
		return;
	end
	
	-- SETUP CURRENT AND NEW INITIATIVE VALUES
	local nCurrentInit = 10000;
	if nodeCurrentActor then
		nCurrentInit = NodeManager.get(nodeCurrentActor, "initresult", 0); 
	end
	local nNewInit = -10000;
	if nodeNewActor then
		nNewInit = NodeManager.get(nodeNewActor, "initresult", 0);
	end
	
	-- ITERATE THROUGH EACH ACTOR
	for keyActor, nodeActor in pairs(nodeActorList.getChildren()) do
		-- ITERATE THROUGH EACH EFFECT
		local nodeEffectList = nodeActor.getChild("effects");
		if nodeEffectList then
			for keyEffect, nodeEffect in pairs(nodeEffectList.getChildren()) do
				-- MAKE SURE THE EFFECT IS ACTIVE
				local bActive = NodeManager.get(nodeEffect, "isactive", 0);
				if (bActive == 1) then
				
					-- DECREMENT EFFECT AT START OF MATCHING INIT, AND CHECK FOR EXPIRATION
					local nDuration = NodeManager.get(nodeEffect, "duration", 0);
					if (nDuration > 0) then
						local nEffInit = NodeManager.get(nodeEffect, "init", 0);
						if nEffInit < nCurrentInit and nEffInit >= nNewInit then
							nDuration = nDuration - 1;
							
							if (nDuration <= 0) then
								expireEffect(nodeActor, nodeEffect, 0);
							else
								NodeManager.set(nodeEffect, "duration", "number", nDuration);
							end
						end
					end
					
					-- HANDLE START OF TURN EFFECTS
					if nodeActor == nodeNewActor then
						local sEffName = NodeManager.get(nodeEffect, "label", "");
						local listEffectComp = parseEffect(sEffName);
						for keyEffectComp, rEffectComp in ipairs(listEffectComp) do
							-- ONGOING DAMAGE ADJUSTMENT (INCLUDING REGENERATION)
							if rEffectComp.type == "DMGO" or rEffectComp.type == "FHEAL" or rEffectComp.type == "REGEN" then
								applyOngoingDamageAdjustment(nodeActor, nodeEffect, rEffectComp);
							end
						end
					end
				end -- END ACTIVE EFFECT CHECK
			end -- END EFFECT LOOP
		end
	end -- END ACTOR LOOP
end

function evalattributeHelper(rActor, sEffectattribute)
	-- PARSE EFFECT base.attribute TAG
	local sSign, sModifier, sShortattribute = string.match(sEffectattribute, "^%[([%+%-]?)([H2]?)([A-Z][A-Z][A-Z])%]$");
	
	-- FIGURE OUT WHICH base.attribute
	local nattribute = nil;
	if sShortattribute == "BOD" then
		nattribute = ActorManager.getAttributeBonus(rActor, "body");
	elseif sShortattribute == "AGI" then
		nattribute = ActorManager.getAttributeBonus(rActor, "agility");
	elseif sShortattribute == "REA" then
		nattribute = ActorManager.getAttributeBonus(rActor, "reaction");
	elseif sShortattribute == "STR" then
		nattribute = ActorManager.getAttributeBonus(rActor, "strength");
	elseif sShortattribute == "CHA" then
		nattribute = ActorManager.getAttributeBonus(rActor, "charisma");
	elseif sShortattribute == "LOG" then
		nattribute = ActorManager.getAttributeBonus(rActor, "logic");
	elseif sShortattribute == "INT" then
		nattribute = ActorManager.getAttributeBonus(rActor, "intuition");
	elseif sShortattribute == "WIL" then
		nattribute = ActorManager.getAttributeBonus(rActor, "willpower");
	end
	
	-- IF VALID SHORT base.attribute, THEN APPLY SIGN AND MODIFIERS
	if nattribute then
		if sSign == "-" then
			nattribute = 0 - nattribute;
		end
		if sModifier == "H" then
			if nattribute > 0 then
				nattribute = math.floor(nattribute / 2);
			else
				nattribute = math.ceil(nattribute / 2);
			end
		elseif sModifier == "2" then
			nattribute = nattribute * 2;
		end
	end
	
	-- RESULTS
	return nattribute;
end

function evalEffect(rActor, s)
	-- VALIDATE
	if not s then
		return "";
	end
	if not rActor then
		return s;
	end
	
	-- SETUP
	local aNewEffect = {};
	
	-- PARSE EFFECT STRING
	local aEffectComp = StringManager.split(s, ";", true);
	for kComp, sComp in pairs(aEffectComp) do
		local aWords = StringManager.parseWords(sComp, "%[%]:");
		
		if #aWords > 0 then
			if string.match(aWords[1], ":$") then
				local aTempWords = { aWords[1] };
				local nTotalMod = 0;
				
				local i = 2;
				local battributeFound = false;
				while aWords[i] do
					local nattribute = evalattributeHelper(rActor, aWords[i]);
					if nattribute then
						battributeFound = true;
						nTotalMod = nTotalMod + nattribute;
					else
						table.insert(aTempWords, aWords[i]);
					end

					i = i + 1;
				end
				
				if StringManager.isDiceString(aTempWords[2]) then
					if nTotalMod ~= 0 then
						local aTempDice, nTempMod = StringManager.convertStringToDice(aTempWords[2]);
						nTempMod = nTempMod + nTotalMod;
						aTempWords[2] = StringManager.convertDiceToString(aTempDice, nTempMod);
					end
				elseif battributeFound then
					table.insert(aTempWords, 2, "" .. nTotalMod);
				end

				table.insert(aNewEffect, table.concat(aTempWords, " "));
			else
				table.insert(aNewEffect, sComp);
			end
		end
	end
	
	return table.concat(aNewEffect, "; ");
end

function getEffectsByType(nodeCTEntry, sEffectType, aFilter, rFilterActor, bTargetedOnly)
	-- GET EFFECTS LIST
	local aEffects = NodeManager.createChild(nodeCTEntry, "effects");
	if not aEffects then
		return {};
	end

	-- SETUP
	local results = {};
	
	-- SEPARATE FILTERS
	local aRangeFilter = {};
	local aOtherFilter = {};
	if aFilter then
		for k, v in pairs(aFilter) do
			if type(v) ~= "string" then
				table.insert(aOtherFilter, v);
			elseif StringManager.contains(DataCommon.rangetypes, v) then
				table.insert(aRangeFilter, v);
			else
				table.insert(aOtherFilter, v);
			end
		end
	end
	
	-- DETERMINE WHETHER EFFECT COMPONENT WE ARE LOOKING FOR SUPPORTS TARGETING
	local bTargetSupport = StringManager.isWord(sEffectType, DataCommon.targetableeffectcomps);
	
	-- ITERATE THROUGH EFFECTS
	for k,v in pairs(aEffects.getChildren()) do
		-- MAKE SURE EFFECT IS ACTIVE
		local bActive = NodeManager.get(v, "isactive", 0);
		if (bActive == 1) then
			-- PARSE EFFECT
			local sLabel = NodeManager.get(v, "label", "");
			local sApply = NodeManager.get(v, "apply", "");
			local effect_list = EffectsManager.parseEffect(sLabel);

			-- IF COMPONENT WE ARE LOOKING FOR SUPPORTS TARGETS, THEN GET ANY EFFECT TARGETS
			local aEffectTargets = {};
			if bTargetSupport then
				local nodeTargetList = v.getChild("targets");
				if nodeTargetList then
					for keyTarget, nodeTarget in pairs(nodeTargetList.getChildren()) do
						table.insert(aEffectTargets, NodeManager.get(nodeTarget, "noderef", ""));
					end
				end
			end

			-- LOOK THROUGH EFFECT CLAUSES FOR A TYPE (or TYPE/SUBTYPE) MATCH
			local nMatch = 0;
			for i = 1, #effect_list do
				-- STRIP OUT ENERGY OR BONUS TYPES FOR SUBTYPE COMPARISON
				local aEffectRangeFilter = {};
				local aEffectOtherFilter = {};
				local j = 0;
				while effect_list[i].remainder[j] do
					if effect_list[i].remainder[j] == "cold" and effect_list[i].remainder[j + 1] and effect_list[i].remainder[j + 1] == "iron" then
						j = j + 1;
					elseif StringManager.contains(DataCommon.dmgtypes, effect_list[i].remainder[j]) or 
							StringManager.contains(DataCommon.bonustypes, effect_list[i].remainder[j]) or
							StringManager.contains(DataCommon.connectors, effect_list[i].remainder[j]) then
						-- SKIP
					elseif StringManager.contains(DataCommon.rangetypes, effect_list[i].remainder[j]) then
						table.insert(aEffectRangeFilter, effect_list[i].remainder[j]);
					else
						table.insert(aEffectOtherFilter, effect_list[i].remainder[j]);
					end
					
					j = j + 1;
				end
			
				-- CHECK TO MAKE SURE THIS COMPONENT MATCHES THE ONE WE'RE SEARCHING FOR
				local comp_match = false;
				if effect_list[i].type == sEffectType then
					-- IF EFFECT TARGETED AND THIS COMPONENT SUPPORTS TARGETING,
					-- THEN ONLY APPLY IF TARGET ACTOR MATCHES
					if (#aEffectTargets > 0) then
						comp_match = false;
						if rFilterActor and rFilterActor.nodeCT then
							for keyTarget, sTargetNode in pairs(aEffectTargets) do
								if sTargetNode == rFilterActor.sCTNode then
									comp_match = true;
								end
							end
						end
					elseif bTargetedOnly then
						comp_match = false;
					else
						comp_match = true;
					end
				
					-- CHECK THE FILTERS
					if #aEffectRangeFilter > 0 then
						local bRangeMatch = false;
						for k2, v2 in pairs(aRangeFilter) do
							if StringManager.contains(aEffectRangeFilter, v2) then
								bRangeMatch = true;
								break;
							end
						end
						if not bRangeMatch then
							comp_match = false;
						end
					end
					if #aEffectOtherFilter > 0 then
						local bOtherMatch = false;
						for k2, v2 in pairs(aOtherFilter) do
							if type(v2) == "table" then
								local bOtherTableMatch = true;
								for k3, v3 in pairs(v2) do
									if not StringManager.contains(aEffectOtherFilter, v3) then
										bOtherTableMatch = false;
										break;
									end
								end
								if bOtherTableMatch then
									bOtherMatch = true;
									break;
								end
							elseif StringManager.contains(aEffectOtherFilter, v2) then
								bOtherMatch = true;
								break;
							end
						end
						if not bOtherMatch then
							comp_match = false;
						end
					end
				end

				-- WE FOUND A MATCH
				if comp_match then
					nMatch = i;
					table.insert(results, effect_list[i]);
				end
			end -- END EFFECT COMPONENT LOOP

			-- REMOVE ONE-SHOT EFFECTS
			if nMatch > 0 then
				if sApply == "action" then
					if bLocked then
						table.insert(aUsedActionEffects, v.getNodeName());
					else
						notifyExpire(v, 0);
					end
				elseif sApply == "roll" then
					notifyExpire(v, 0);
				elseif sApply == "single" then
					notifyExpire(v, nMatch);
				end
			end
		end  -- END ACTIVE CHECK
	end  -- END EFFECT LOOP
	
	-- RESULTS
	return results;
end

function getEffectsBonusByType(nodeCTEntry, aEffectType, bAddEmptyBonus, aFilter, rFilterActor, bTargetedOnly)
	-- VALIDATE
	if not nodeCTEntry or not aEffectType then
		return {}, 0;
	end
	
	-- MAKE BONUS TYPE INTO TABLE, IF NEEDED
	if type(aEffectType) ~= "table" then
		aEffectType = { aEffectType };
	end
	
	-- PER EFFECT TYPE VARIABLES
	local results = {};
	local bonuses = {};
	local penalties = {};
	local nEffectCount = 0;
	
	for k, v in pairs(aEffectType) do
		-- LOOK FOR EFFECTS THAT MATCH BONUSTYPE
		local aEffectsByType = getEffectsByType(nodeCTEntry, v, aFilter, rFilterActor, bTargetedOnly);

		-- ITERATE THROUGH EFFECTS THAT MATCHED
		for k2,v2 in pairs(aEffectsByType) do
			-- LOOK FOR ENERGY OR BONUS TYPES
			local dmg_type = nil;
			local mod_type = nil;
			for k3, v3 in pairs(v2.remainder) do
				if StringManager.contains(DataCommon.dmgtypes, v3) or StringManager.contains(DataCommon.immunetypes, v3) or v3 == "all" then
					dmg_type = v3;
					break;
				elseif StringManager.contains(DataCommon.bonustypes, v3) then
					mod_type = v3;
					break;
				end
			end
			
			-- IF MODIFIER TYPE IS UNTYPED, THEN APPEND MODIFIERS
			-- (SUPPORTS DICE)
			if dmg_type or not mod_type then
				-- ADD EFFECT RESULTS 
				local new_key = dmg_type or "";
				local new_results = results[new_key] or {dice = {}, mod = 0, remainder = {}};

				-- BUILD THE NEW RESULT
				for k3,v3 in pairs(v2.dice) do
					table.insert(new_results.dice, v3); 
				end
				if bAddEmptyBonus then
					new_results.mod = new_results.mod + v2.mod;
				else
					new_results.mod = math.max(new_results.mod, v2.mod);
				end
				for k3,v3 in pairs(v2.remainder) do
					table.insert(new_results.remainder, v3);
				end

				-- SET THE NEW DICE RESULTS BASED ON ENERGY TYPE
				results[new_key] = new_results;

			-- OTHERWISE, TRACK BONUSES AND PENALTIES BY MODIFIER TYPE 
			-- (IGNORE DICE, ONLY TAKE BIGGEST BONUS AND/OR PENALTY FOR EACH MODIFIER TYPE)
			else
				if v2.mod >= 0 then
					bonuses[mod_type] = math.max(v2.mod, bonuses[mod_type] or 0);
				elseif v2.mod < 0 then
					penalties[mod_type] = math.min(v2.mod, penalties[mod_type] or 0);
				end

			end
			
			-- INCREMENT EFFECT COUNT
			nEffectCount = nEffectCount + 1;
		end
	end

	-- COMBINE BONUSES AND PENALTIES FOR NON-ENERGY TYPED MODIFIERS
	for k2,v2 in pairs(bonuses) do
		if results[k2] then
			results[k2].mod = results[k2].mod + v2;
		else
			results[k2] = {dice = {}, mod = v2};
		end
	end
	for k2,v2 in pairs(penalties) do
		if results[k2] then
			results[k2].mod = results[k2].mod + v2;
		else
			results[k2] = {dice = {}, mod = v2};
		end
	end

	-- RESULTS
	return results, nEffectCount;
end

function getEffectsBonus(nodeCTEntry, aEffectType, bModOnly, aFilter, rFilterActor, bTargetedOnly)
	-- VALIDATE
	if not nodeCTEntry or not aEffectType then
		if bModOnly then
			return 0, 0;
		end
		return {}, 0, 0;
	end
	
	-- MAKE BONUS TYPE INTO TABLE, IF NEEDED
	if type(aEffectType) ~= "table" then
		aEffectType = { aEffectType };
	end
	
	-- START WITH AN EMPTY MODIFIER TOTAL
	local aTotalDice = {};
	local nTotalMod = 0;
	local nEffectCount = 0;
	
	-- ITERATE THROUGH EACH BONUS TYPE
	local masterbonuses = {};
	local masterpenalties = {};
	for k, v in pairs(aEffectType) do
		-- GET THE MODIFIERS FOR THIS MODIFIER TYPE
		local effbonusbytype, nEffectSubCount = getEffectsBonusByType(nodeCTEntry, v, true, aFilter, rFilterActor, bTargetedOnly);
		
		-- ITERATE THROUGH THE MODIFIERS
		for k2, v2 in pairs(effbonusbytype) do
			-- IF MODIFIER TYPE IS UNTYPED, THEN APPEND TO TOTAL MODIFIER
			-- (SUPPORTS DICE)
			if k2 == "" or StringManager.contains(DataCommon.dmgtypes, k2) then
				for k3, v3 in pairs(v2.dice) do
					table.insert(aTotalDice, v3);
				end
				nTotalMod = nTotalMod + v2.mod;
			
			-- OTHERWISE, WE HAVE A NON-ENERGY MODIFIER TYPE, WHICH MEANS WE NEED TO INTEGRATE
			-- (IGNORE DICE, ONLY TAKE BIGGEST BONUS AND/OR PENALTY FOR EACH MODIFIER TYPE)
			else
				if v2.mod >= 0 then
					masterbonuses[k2] = math.max(v2.mod, masterbonuses[k2] or 0);
				elseif v2.mod < 0 then
					masterpenalties[k2] = math.min(v2.mod, masterpenalties[k2] or 0);
				end
			end
		end

		-- ADD TO EFFECT COUNT
		nEffectCount = nEffectCount + nEffectSubCount;
	end

	-- ADD INTEGRATED BONUSES AND PENALTIES FOR NON-ENERGY TYPED MODIFIERS
	for k,v in pairs(masterbonuses) do
		nTotalMod = nTotalMod + v;
	end
	for k,v in pairs(masterpenalties) do
		nTotalMod = nTotalMod + v;
	end
	
	-- RESULTS
	if bModOnly then
		return nTotalMod, nEffectCount;
	end
	return aTotalDice, nTotalMod, nEffectCount;
end

function isEffectTarget(nodeEffect, nodeTarget)
	local bMatch = false;
	
	if nodeEffect and nodeTarget then
		local nodeTargetList = nodeEffect.getChild("targets");
		if nodeTargetList then
			for k, v in pairs(nodeTargetList.getChildren()) do
				if NodeManager.get(v, "noderef", "") == nodeTarget.getNodeName() then
					bMatch = true;
					break;
				end
			end
		end
	end

	return bMatch;
end

function hasEffectCondition(rActor, sEffect)
	return hasEffect(rActor, sEffect, nil, false, true);
end

function hasEffect(rActor, sEffect, rTarget, bTargetedOnly, bIgnoreEffectTargets)
	-- Parameter validation
	if not sEffect or not rActor then
		return false;
	end
	
	-- Make sure we can get to the effects list
	local node_list_effects = NodeManager.createChild(rActor.nodeCT, "effects");
	if not node_list_effects then
		return false;
	end

	-- Iterate through each effect
	local aMatch = {};
	for k,v in pairs(node_list_effects.getChildren()) do
		if NodeManager.get(v, "isactive", 0) ~= 0 then
			-- Parse each effect label
			local sLabel = NodeManager.get(v, "label", "");
			local effect_list = StringManager.split(sLabel, ";", true);
			local bTargeted = EffectsManager.isTargetedEffect(v);

			-- Iterate through each effect component looking for a type match
			local nMatch = 0;
			for i = 1, #effect_list do
				if string.lower(effect_list[i]) == string.lower(sEffect) then
					if bTargeted and not bIgnoreEffectTargets then
						if rTarget and rTarget.nodeCT then
							if isEffectTarget(v, rTarget.nodeCT) then
								table.insert(aMatch, v);
								nMatch = i;
							end
						end
					elseif not bTargetedOnly then
						table.insert(aMatch, v);
						nMatch = i;
					end
				end
				
			end
			
			-- If matched, then remove one-off effects
			if nMatch > 0 then
				local sApply = NodeManager.get(v, "apply", "");
				if sApply == "action" then
					if bLocked then
						table.insert(aUsedActionEffects, v.getNodeName());
					else
						notifyExpire(v, 0);
					end
				elseif sApply == "roll" then
					notifyExpire(v, 0);
				elseif sApply == "single" then
					notifyExpire(v, nMatch);
				end
			end
		end
	end
	
	-- Return results
	if #aMatch > 0 then
		return true;
	end
	return false;
end

--
--  HANDLE EFFECT LOCKING
--

function lock()
	bLocked = true;
end

function unlock()
	bLocked = false;

	local aExpired = {};
	
	for k, v in ipairs(aUsedActionEffects) do
		if not aExpired[v] then
			notifyExpire(v, 0);
			aExpired[v] = true;
		end
	end
	
	aUsedActionEffects = {};
end
