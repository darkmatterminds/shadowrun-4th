-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function performRoll(draginfo, rActor, sAttributeStat)
	local rRoll = {};
	local nValue = 0;
	local aDice =  {};
	local sInitType = " "
	
	nValue = NodeManager.get (rActor.nodeCreature, sAttributeStat, "0");
	if sAttributeStat == "base.attribute.ini.check" then
		sInitType = NodeManager.set(rActor.nodeCreature, "initype", "string" , "0");
		rRoll.sDesc = "[INIT] Check Mundane "
	elseif sAttributeStat == "base.attribute.astini.check" then
		sInitType = NodeManager.set(rActor.nodeCreature, "initype", "string" , "1");
		rRoll.sDesc = "[INIT] Check Astral "
	elseif sAttributeStat == "base.attribute.matini.check" then
		sInitType = NodeManager.set(rActor.nodeCreature, "initype", "string" , "2");
		rRoll.sDesc = "[INIT] Check Matrix "
	elseif sAttributeStat == "base.attribute.hotmatini.check" then
		sInitType = NodeManager.set(rActor.nodeCreature, "initype", "string" ,"3");
		rRoll.sDesc = "[INIT] Check Matrix (HOTSIM)"
	end

	
	rRoll.sDesc , nValue = ActorManager.getPenalty(rActor, nValue, rRoll.sDesc);
	rRoll.sDesc , nValue = ActorManager.getSustainingPenalty(rActor, nValue, rRoll.sDesc);
	rRoll.sDesc , nValue = ActorManager.getArmorPenalty(rActor, nValue, rRoll.sDesc, sAttributeStat);	
	rRoll.sDesc, nValue = ActorManager.getModifiers(rRoll.sDesc, nValue)
	rRoll.sDesc, rRoll.bEdge , rRoll.nEdgeVal = ActorManager.getEdge(rActor, rRoll.sDesc);
	nValue = nValue + rRoll.nEdgeVal
	-- SETUP
	for i = 1, nValue do
		table.insert (aDice, "d6");
	end
	rRoll.aDice = aDice;
	rRoll.nValue = 0;
	
	ActionsManager.performSingleRollAction(draginfo, rActor, "init", rRoll);
end

