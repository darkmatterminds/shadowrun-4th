-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

local sSort = "label";
local sFocus = "label";

function onInit()
	if User.isHost() then
		registerMenuItem("Create Item", "insert", 5);
	end
end

function addEntry(bFocus)
	local win = NodeManager.createWindow(self);
	if bFocus and win and win[sFocus] then
		win[sFocus].setFocus();
	end
	return win;
end

function onMenuSelection(selection)
	if selection == 5 then
		addEntry(true);
	end
end

function onClickDown(button, x, y)
	return true;
end

function onClickRelease(button, x, y)
	if User.isHost() then
		if not getNextWindow(nil) then
			addEntry(true);
		end
	end
	return true;
end

function onSortCompare(w1, w2)
	if w1[sSort] and w2[sSort] then
		local sValue1 = string.lower(w1[sSort].getValue());
		local sValue2 = string.lower(w2[sSort].getValue());
		if sValue1 ~= sValue2 then
			return sValue1 > sValue2;
		end
	end

	return w1.getDatabaseNode().getName() > w2.getDatabaseNode().getName();
end

function onFilter(w)
	if not User.isHost() and w.isgmonly.getState() then
		return false;
	end

	if window.filter then
		local f = string.lower(window.filter.getValue());

		if f == "" then
			return true;
		end
		if string.find(string.lower(w.label.getValue()), f, 0, true) then
			return true;
		end
		
		return false;
	end
	
	return true;
end
