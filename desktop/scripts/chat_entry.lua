-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	ChatManager.registerEntryControl(self);
end

function onDeliverMessage(msg, mode)
	if mode == "chat" then
		ChatManager.onChatNormal(msg);
	elseif mode == "ooc" then
		ChatManager.onChatOOC(msg);
	elseif mode == "story" then
		ChatManager.onChatStory(msg);
	elseif mode == "act" then
		ChatManager.onChatAction(msg);
	elseif mode == "emote" then
		ChatManager.onChatEmote(msg);
	end
	
	return msg;
end

function onTab()
	ChatManager.doAutocomplete();
end
