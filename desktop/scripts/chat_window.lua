-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	if User.isHost() then
		Module.onActivationRequested = moduleActivationRequested;
	end

	Module.onUnloadedReference = moduleUnloadedReference;

	deliverLaunchMessage()
end

function deliverLaunchMessage()
    local msg = {sender = "", font = "emotefont", icon="portrait_ruleset_token"};
    msg.text = "Welcome to the Shadowrun(TM) Ruleset (Version 4.2.8.87). This Ruleset is created by Fenloh, Verokh, exothermal and Thasmiel from the Drachenzwinge Germany Community. Shadowrun � 2005-2011 The Topps Company, Inc. All rights reserved. Shadowrun is a registered trademark of The Topps Company, Inc"
    Comm.addChatMessage(msg);
    
    local launchmsg = ChatManager.retrieveLaunchMessages();
    for keyMessage, rMessage in ipairs(launchmsg) do
    	Comm.addChatMessage(rMessage);
    end
end

function onDiceLanded(draginfo)
 	return ActionsManager.onDiceLanded(draginfo);
end

function onDrop(x, y, draginfo)
	local sDragType = draginfo.getType();
	if StringManager.contains(DataCommon.actions, sDragType) then
		ActionsManager.handleActionDrop(draginfo, nil);
	end
end

function moduleActivationRequested(sModule)
	local msg = {};
	msg.text = "Players have requested permission to load '" .. sModule .. "'";
	msg.font = "systemfont";
	msg.icon = "indicator_moduleloaded";
	Comm.addChatMessage(msg);
end

function moduleUnloadedReference(sModule)
	local msg = {};
	msg.text = "Could not open sheet with data from unloaded module '" .. sModule .. "'";
	msg.font = "systemfont";
	Comm.addChatMessage(msg);
end
