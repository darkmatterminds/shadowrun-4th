-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

targetingon = false;
activeon = false;
defensiveon = false;
spacingon = false;
effectson = false;

function onInit()
	onTypeChanged();
	
	-- Set the displays to what should be shown
	setTargetingVisible(false);
	setActiveVisible(false);
	setDefensiveVisible(false);
	setSpacingVisible(false);
	setEffectsVisible(false);

	-- Acquire token reference, if any
	linkToken();
	
	-- Set up the PC links
	if type.getValue() == "pc" then
		linkPCFields();
	end
	
	-- Update the displays
	updateDisplay();
	onWoundsChanged();
	
	-- Register the deletion menu item for the host
	registerMenuItem("Delete Item", "delete", 6);
	registerMenuItem("Confirm Delete", "delete", 6, 7);

	-- Track the effects list
	local nodeEffects = effects.getDatabaseNode();
	if nodeEffects then
		nodeEffects.onChildUpdate = onEffectsChanged;
		nodeEffects.onChildAdded = onEffectsChanged;
		onEffectsChanged();
	end
	
	-- Track the targets list
	local nodeTargets = targets.getDatabaseNode();
	if nodeTargets then
		nodeTargets.onChildUpdate = onTargetsChanged;
		nodeTargets.onChildAdded = onTargetsChanged;
		onTargetsChanged();
	end
end

function updateDisplay()
	local sFaction = friendfoe.getStringValue();

	if type.getValue() ~= "pc" then
		name.setFrame("textlinesmall", 0, 0, 0, 0);
	end

	if isActive() then
		name.setFont("ct_active");
		
		active_spacer_top.setVisible(true);
		active_spacer_bottom.setVisible(true);
		
		if sFaction == "friend" then
			setFrame("ctentrybox_friend_active");
		elseif sFaction == "neutral" then
			setFrame("ctentrybox_neutral_active");
		elseif sFaction == "foe" then
			setFrame("ctentrybox_foe_active");
		else
			setFrame("ctentrybox_active");
		end
	else
		name.setFont("ct_name");
		
		active_spacer_top.setVisible(false);
		active_spacer_bottom.setVisible(false);
		
		if sFaction == "friend" then
			setFrame("ctentrybox_friend");
		elseif sFaction == "neutral" then
			setFrame("ctentrybox_neutral");
		elseif sFaction == "foe" then
			setFrame("ctentrybox_foe");
		else
			setFrame("ctentrybox");
		end
	end
end

function linkToken()
	local imageinstance = token.populateFromImageNode(tokenrefnode.getValue(), tokenrefid.getValue());
	if imageinstance then
		token.link(imageinstance);
	end
end

function onMenuSelection(selection, subselection)
	if selection == 6 and subselection == 7 then
		delete();
	end
end

function delete()
	local node = getDatabaseNode();
	if not node then
		close();
		return;
	end
	
	-- Remember node name
	local sNode = node.getNodeName();
	
	-- Clear any effects first, so that saves aren't triggered by nextActor
	effects.reset(false);
	
	-- Move to the next actor, if this CT entry is active
	if isActive() then
		windowlist.nextActor();
	end

	-- If this is an NPC with a token on the map, then remove the token also
	if type.getValue() ~= "pc" then
		token.deleteReference();
	end

	-- Delete the database node and close the window
	node.delete();

	-- Update list information (global subsection toggles, targeting)
	windowlist.onVisibilityToggle();
	windowlist.onEntrySectionToggle();
	windowlist.deleteTarget(sNode);
	TargetingManager.rebuildClientTargeting();
end

function onTypeChanged()
	-- If a PC, then set up the links to the char sheet
	local sType = type.getValue();
	if sType == "pc" then
		linkPCFields();
	end

	-- If a NPC, then show the NPC display button; otherwise, hide it
	if sType == "pc" then
		show_npc.setVisible(false);
	else
		show_npc.setVisible(true);
	end
end

function onWoundsChanged()
	-- Calculate the percent wounded for this unit
--	local nPercentWounded, nPercentNonlethal = ActorManager.getPercentWounded("ct", getDatabaseNode());
	
	-- Based on the percent wounded, change the font color for the Wounds field
--	if nPercentWounded > 1 then
--		wounds.setFont("ct_dead_number");
--		nonlethal.setFont("ct_dead_number");
--	elseif nPercentNonlethal > 1 then
--		wounds.setFont("ct_unc_number");
--		nonlethal.setFont("ct_unc_number");
--	elseif nPercentNonlethal > 0.66 then
--		wounds.setFont("ct_hvywound_number");
--		nonlethal.setFont("ct_hvywound_number");
--	elseif nPercentNonlethal > 0.33 then
--		wounds.setFont("ct_modwound_number");
--		nonlethal.setFont("ct_modwound_number");
--	elseif nPercentNonlethal > 0 then
--		wounds.setFont("ct_ltwound_number");
--		nonlethal.setFont("ct_ltwound_number");
--	else
--		wounds.setFont("ct_healthy_number");
--		nonlethal.setFont("ct_healthy_number");
--	end
	
	-- Based on the percent wounded, set the Status text field
--	if nPercentWounded > 1 then
--		if (wounds.getValue() - hp.getValue()) < 10 then
--			status.setValue("Dying");
--		else
--			status.setValue("Dead");
--		end
--	elseif nPercentNonlethal > 1 then
--		status.setValue("Unconscious");
--	elseif nPercentWounded == 1 then
--		status.setValue("Disabled");
--	elseif nPercentNonlethal == 1 then
--		status.setValue("Staggered");
--	elseif nPercentNonlethal > .66 then
--		status.setValue("Heavy");
--	elseif nPercentNonlethal > .33 then
--		status.setValue("Moderate");
--	elseif nPercentNonlethal > 0 then
--		status.setValue("Light");
--	else
--		status.setValue("Healthy");
--	end

	-- Update the token underlay to reflect wound status
--	updateTokenUnderlay();
end

function onFactionChanged()
	-- Update the entry frame
	updateDisplay();

	-- Update the token underlay to friend-or-foe status
	TokenManager.updateFaction(getDatabaseNode());
	updateTokenUnderlay();
end

function updateTokenUnderlay()
	TokenManager.updateUnderlay(getDatabaseNode());
end

function onVisibilityChanged()
	TokenManager.updateVisibility(getDatabaseNode());
	windowlist.onVisibilityToggle();
end

function onEffectsChanged()
	-- SET THE EFFECTS CONTROL STRING
	local affectedby = EffectsManager.getEffectsString(getDatabaseNode());
	effects_str.setValue(affectedby);
	
	-- UPDATE VISIBILITY
	if affectedby == "" or effectson then
		effects_label.setVisible(false);
		effects_str.setVisible(false);
	else
		effects_label.setVisible(true);
		effects_str.setVisible(true);
	end
	setSpacerState();
end

function onTargetsChanged()
	-- VALIDATE (SINCE THIS FUNCTION CAN BE CALLED BEFORE FULLY INSTANTIATED)
	if not targets_str then
		return;
	end
	
	-- GET TARGET NAMES
	local aTargetNames = {};
	for keyTarget, winTarget in pairs(targets.getWindows()) do
		local sTargetName = NodeManager.get(DB.findNode(winTarget.noderef.getValue()), "name", "");
		if sTargetName == "" then
			sTargetName = "<Target>";
		end
		table.insert(aTargetNames, sTargetName);
	end

	-- SET THE TARGETS CONTROL STRING
	targets_str.setValue(table.concat(aTargetNames, ", "));
	
	-- UPDATE VISIBILITY
	if #aTargetNames == 0 or targetingon then
		targets_label.setVisible(false);
		targets_str.setVisible(false);
	else
		targets_label.setVisible(true);
		targets_str.setVisible(true);
	end
	setSpacerState();
end

function setSpacerState()
	if effects_label.isVisible() then
		if targets_label.isVisible() then
			spacer2.setAnchoredHeight(2);
		else
			spacer2.setAnchoredHeight(6);
		end
	else
		spacer2.setAnchoredHeight(0);
	end
end

function linkPCFields(src)
	local src = link.getTargetDatabaseNode();
	if src then
		name.setLink(NodeManager.createChild(src, "name", "string"), true);
		ctphysicaldamage.setLink(NodeManager.createChild(src, "damage.physical.current", "number"), true);
		ctstundamage.setLink(NodeManager.createChild(src, "damage.stun.current", "number"), true);
		ctphysicaldamagemax.setLink(NodeManager.createChild(src, "damage.physical.max", "number"), true);
		ctstundamagemax.setLink(NodeManager.createChild(src, "damage.stun.max", "number"), true);
		ctinitypelink.setLink(NodeManager.createChild(src, "initype", "string"), true);		
		
		edgescore.setLink(NodeManager.createChild(src, "base.attribute.edge.score", "number"), true);
		edgemod.setLink(NodeManager.createChild(src, "base.attribute.edge.mod", "number"), true);
		edgecheck.setLink(NodeManager.createChild(src, "base.attribute.edge.check", "number"), true);
 		damagepenalty.setLink(NodeManager.createChild(src, "damage.all.modifier", "number"), true);
		sustainedpenalty.setLink(NodeManager.createChild(src, "base.special.spellsustainedpenalty", "number"), true);
		armorpenalty.setLink(NodeManager.createChild(src, "base.special.penalty", "number"), true);
		
		ctbaseini.setLink(NodeManager.createChild(src, "base.attribute.ini.check", "number"), true);
		ctbaseastini.setLink(NodeManager.createChild(src, "base.attribute.astini.check", "number"), true);
		ctbasematini.setLink(NodeManager.createChild(src, "base.attribute.matini.check", "number"), true);
		ctbasehotmatini.setLink(NodeManager.createChild(src, "base.attribute.hotmatini.check", "number"), true);
		ctbaseinimod.setLink(NodeManager.createChild(src, "base.attribute.ini.mod", "number"), true);
		ctbaseastinimod.setLink(NodeManager.createChild(src, "base.attribute.astini.mod", "number"), true);
		ctbasematinimod.setLink(NodeManager.createChild(src, "base.attribute.matini.mod", "number"), true);
		ctbasehotmatinimod.setLink(NodeManager.createChild(src, "base.attribute.hotmatini.mod", "number"), true);
		ctbaseinipass.setLink(NodeManager.createChild(src, "base.attribute.inipass.score", "number"), true);
		ctbaseastinipass.setLink(NodeManager.createChild(src, "base.attribute.astinipass.score", "number"), true);
		ctbasematinipass.setLink(NodeManager.createChild(src, "base.attribute.matinipass.score", "number"), true);
		ctbasehotmatinipass.setLink(NodeManager.createChild(src, "base.attribute.hotmatinipass.score", "number"), true);
	
	end
end

--
-- SECTION VISIBILITY FUNCTIONS
--

function setTargetingVisible(v)
	if activatetargeting.getValue() then
		v = true;
	end
	if type.getValue() ~= "pc" and active.getState() then
		v = true;
	end
	
	targetingon = v;
	targetingicon.setVisible(v);
	
	targeting_add_button.setVisible(v);
	targeting_clear_button.setVisible(v);
	targets.setVisible(v);
	
	frame_targeting.setVisible(v);
	
	onTargetsChanged();
end

function setActiveVisible(v)
	if activateactive.getValue() then
		v = true;
	end
	
	activeon = v;
	activeicon.setVisible(v);

	initbase.setVisible(v);
	initlabel.setVisible(v);
	label_initbase.setVisible(v);
	initroll.setVisible(v);
	label_initroll.setVisible(v);
	initmod.setVisible(v);
	label_initmod.setVisible(v);
	passbase.setVisible(v);
	passlabel.setVisible(v);
	label_passbase.setVisible(v);
	passbonus.setVisible(v);
	label_passbonus.setVisible(v);
	ctinitrolled.setVisible(v);
	label_ctinitrolled.setVisible(v);
	ctaction1.setVisible(v);
	label_ctaction1.setVisible(v);
	ctaction2.setVisible(v);
	label_ctaction2.setVisible(v);
	ctactionfree.setVisible(v);
	label_ctactionfree.setVisible(v);
	frame_active.setVisible(v);
end

function setDefensiveVisible(v)
	if activatedefensive.getValue() then
		v = true;
	end
	
	defensiveon = v;
	defensiveicon.setVisible(v);

	edgescore.setVisible(v);
	edgescorelabel.setVisible(v);
	label_edgescore.setVisible(v);
	edgemod.setVisible(v);
	label_edgemod.setVisible(v);
	edgecheck.setVisible(v);
	label_edgecheck.setVisible(v);
	damagepenalty.setVisible(v);
	damagepenaltylabel.setVisible(v);
	label_damagepenalty.setVisible(v);
	sustainedpenalty.setVisible(v);
	label_sustainedpenalty.setVisible(v);
	armorpenalty.setVisible(v);
	label_armorpenalty.setVisible(v);
	
	frame_defensive.setVisible(v);
end
	
function setSpacingVisible(v)
	if activatespacing.getValue() then
		v = true;
	end

	spacingon = v;
	spacingicon.setVisible(v);
	
	space.setVisible(v);
	spacelabel.setVisible(v);
	reach.setVisible(v);
	reachlabel.setVisible(v);
	
	frame_spacing.setVisible(v);
end

function setEffectsVisible(v)
	if activateeffects.getValue() then
		v = true;
	end
	
	effectson = v;
	effecticon.setVisible(v);
	
	effects.setVisible(v);
	if v then
		effects.checkForEmpty();
	end
	
	frame_effects.setVisible(v);

	onEffectsChanged();
end

-- Activity state

function isActive()
	return active.getState();
end

function setActive(state)
	-- Set the active indicator
	active.setState(state);
	
	-- Visible changes
	updateDisplay();
	
	-- Notifications 
	if state then
		-- Turn notification
		local msg = {font = "narratorfont", icon = "indicator_flag"};
		msg.text = "[TURN] " .. name.getValue();

		if OptionsManager.isOption("RSHE", "on") then
			local sEffects = EffectsManager.getEffectsString(getDatabaseNode(), true);
			if sEffects ~= "" then
				msg.text = msg.text .. " - " .. "[" .. sEffects .. "]";
			end
		end
		
		if type.getValue() == "pc" then
			-- Player Turn notification
			Comm.deliverChatMessage(msg);
			
			-- Ring bell also, if option enabled
			if OptionsManager.isOption("RING", "on") then
				local usernode = link.getTargetDatabaseNode();
				if usernode then
					local ownerid = User.getIdentityOwner(usernode.getName());
					if ownerid then
						User.ringBell(ownerid);
					end
				end
			end
		else
			-- DM Turn notification
			if show_npc.getState() then
				Comm.deliverChatMessage(msg);
			else
				msg.text = "[GM] " .. msg.text;
				Comm.addChatMessage(msg);
			end
		end
	end
end

-- Client Visibility

function isClientVisible()
	if type.getValue() == "pc" then
		return true;
	end
	if show_npc.getState() then
		return true;
	end
	return false;
end
