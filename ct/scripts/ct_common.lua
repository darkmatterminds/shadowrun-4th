-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

OOB_MSGTYPE_ENDTURN = "endturn";

function onInit()
	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_ENDTURN, handleEndTurn);
end

function handleEndTurn(msgOOB)
	CombatCommon.endTurn(msgOOB.user);
end

--
--	GENERAL
--

function getActiveInit()
	local nActiveInit = nil;
	
	local nodeActive = getActiveCT();
	if nodeActive then
		nActiveInit = NodeManager.get(nodeActive, "initresult", 0);
	end
	
	return nActiveInit;
end

--
--  NODE TRANSLATION
--

function getActiveCT()
	-- FIND TRACKER NODE
	local nodeTracker = DB.findNode("combattracker");
	if not nodeTracker then
		return nil;
	end

	-- LOOK FOR ACTIVE NODE
	for keyEntry, nodeEntry in pairs(nodeTracker.getChildren()) do
		if NodeManager.get(nodeEntry, "active", 0) == 1 then
			return nodeEntry;
		end
	end

	-- IF NO ACTIVE NODES, THEN RETURN NIL
	return nil;
end

function getCTFromNode(varNode)
	-- SETUP
	local sNode = "";
	if type(varNode) == "string" then
		sNode = varNode;
	elseif type(varNode) == "databasenode" then
		sNode = varNode.getNodeName();
	else
		return nil;
	end
	
	-- FIND TRACKER NODE
	local nodeTracker = DB.findNode("combattracker");
	if not nodeTracker then
		return nil;
	end

	-- Check for exact CT match
	for keyEntry, nodeEntry in pairs(nodeTracker.getChildren()) do
		if nodeEntry.getNodeName() == sNode then
			return nodeEntry;
		end
	end

	-- Otherwise, check for link match
	for keyEntry, nodeEntry in pairs(nodeTracker.getChildren()) do
		local nodeLink = nodeEntry.getChild("link");
		if nodeLink then
			local sRefClass, sRefNode = nodeLink.getValue();
			if sRefNode == sNode then
				return nodeEntry;
			end
		end
	end

	return nil;	
end

function getCTFromTokenRef(nodeContainer, nId)
	if not nodeContainer then
		return nil;
	end
	
	-- FIND TRACKER NODE
	local nodeTracker = DB.findNode("combattracker");
	if not nodeTracker then
		return nil;
	end
	
	local sContainerNode = nodeContainer.getNodeName();

	-- LOOK FOR ACTIVE NODE
	for keyEntry, nodeEntry in pairs(nodeTracker.getChildren()) do
		local sCTContainerName = NodeManager.get(nodeEntry, "tokenrefnode", "");
		local nCTId = tonumber(NodeManager.get(nodeEntry, "tokenrefid", "")) or 0;
		if (sCTContainerName == sContainerNode) and (nCTId == nId) then
			return nodeEntry;
		end
	end

	-- IF NO MATCHES, THEN RETURN NIL
	return nil;
end

function getCTFromToken(token)
	-- GET TOKEN CONTAINER AND ID
	local nodeContainer = token.getContainerNode();
	local nID = token.getId();

	return getCTFromTokenRef(nodeContainer, nID);
end


--
-- DROP HANDLING
--

function onDrop(nodetype, nodename, draginfo)
	local rSource, rTarget = ActorManager.getDropActors(nodetype, nodename, draginfo);
	if rTarget then
		local sDragType = draginfo.getType();

		-- FACTION CHANGES
		if sDragType == "combattrackerff" then
			if User.isHost() then
				NodeManager.set(rTarget.nodeCT, "friendfoe", "string", draginfo.getStringData());
				return true;
			end

		-- TARGETING
		elseif sDragType == "targeting" then
			if User.isHost() then
				onTargetingDrop(rSource, rTarget, draginfo);
				return true;
			end

		-- ACTIONS
		elseif StringManager.contains(DataCommon.targetactions, sDragType) then
			ActionsManager.handleActionDrop(draginfo, rTarget);
			return true;

		-- POTENTIAL ACTIONS
		elseif sDragType == "number" then
			onNumberDrop(rSource, rTarget, draginfo);
			return true;
		end
	end
end

function onTargetingDrop(rSourceActor, rTargetActor, draginfo)
	if rTargetActor.nodeCT then
		-- ADD CREATURE TARGET
		if rSourceActor then
			if rSourceActor.nodeCT then
				TargetingManager.addTarget("host", rSourceActor.sCTNode, rTargetActor.sCTNode);
			end

		-- ADD EFFECT TARGET
		else
			local sRefClass, sRefNode = draginfo.getShortcutData();
			if sRefClass and sRefNode then
				if sRefClass == "combattracker_effect" then
					TargetingManager.addTarget("host", sRefNode, rTargetActor.sCTNode);
				end
			end
		end
	end
end

function onNumberDrop(rSource, rTarget, draginfo)
	-- CHECK FOR ACTION RESULTS
	local sType = nil;
	if string.match(draginfo.getDescription(), "%[ATTACK") then
		sType = "attack";
	elseif string.match(draginfo.getDescription(), "%[GRAPPLE") then
		sType = "grapple";
	elseif string.match(draginfo.getDescription(), "%[DAMAGE") then
		sType = "damage";
	elseif string.match(draginfo.getDescription(), "%[HEAL") then
		sType = "heal";
	elseif string.match(draginfo.getDescription(), "%[EFFECT") then
		sType = "effect";
	elseif string.match(draginfo.getDescription(), "%[CL CHECK") then
		sType = "clc";
	elseif string.match(draginfo.getDescription(), "%[SAVE VS") then
		sType = "spellsave";
	end
	
	-- IF ACTION, THEN RESOLVE IT AGAINST THIS TARGET
	if sType then
		local rRoll = {};
		rRoll.sType = sType;
		rRoll.sDesc = draginfo.getDescription();
		rRoll.aDice = {};
		rRoll.nValue = draginfo.getNumberData();
		
		ActionsManager.resolveAction(rSource, rTarget, rRoll);
	end
end


--
-- END TURN
--

function endTurn(msguser)
	-- Check if the special message user is the same as the owner of the active CT node
	local rActor = ActorManager.getActor("ct", getActiveCT());
	if rActor and rActor.sType == "pc" and rActor.nodeCreature then
		if rActor.nodeCreature.getOwner() == msguser then
			-- Make sure the combat tracker is up on the host
			local wnd = Interface.findWindow("combattracker_window", "combattracker");
			if not wnd then
				local msg = {font = "systemfont"};
				msg.text = "[WARNING] Turns can only be ended when the host combat tracker is open";
				Comm.deliverChatMessage(msg, msguser);
				return;
			end

			-- Everything checks out, so advance the turn
			wnd.list.nextActor();
		end
	end
end

--
-- ROLL HELPERS
--

function parseAttackLine(rActor, sLine)
	-- SETUP
	local rAttackRolls = {};
	local rDamageRolls = {};
	local rAttackCombos = {};

	-- Check the anonymous NPC attacks option
	local sOptANPC = OptionsManager.getOption("ANPC");

	-- PARSE 'OR'/'AND' PHRASES
	local aPhrasesOR, aSkipOR = RulesManager.decodeAndOrClauses(sLine);

	-- PARSE EACH ATTACK
	local nAttackIndex = 1;
	local nLineIndex = 1;
	local aCurrentCombo = {};
	local nStarts, nEnds, sAll, sAttackCount, sAttackLabel, sAttackModifier, sAttackType, nDamageStart, sDamage, nDamageEnd;
	for kOR, vOR in ipairs(aPhrasesOR) do
			
		for kAND, sAND in ipairs(vOR) do

			-- Look for the right patterns
			nStarts, nEnds, sAll, sAttackCount, sAttackLabel, sAttackModifier, sAttackType, nDamageStart, sDamage, nDamageEnd 
					= string.find(sAND, '((%+?%d*) ?([%w%s,%[%]%(%)%+%-]*) ([%+%-%d][%+%-%d/]+)([^%(]*)%(()([^%)]*)()%))');
			
			-- Make sure we got a match
			if nStarts then
				local rAttack = {};
				rAttack.startpos = nLineIndex + nStarts - 1;
				rAttack.endpos = nLineIndex + nEnds;
				
				local rDamage = {};
				rDamage.startpos = nLineIndex + nDamageStart - 1;
				rDamage.endpos = nLineIndex + nDamageEnd - 1;
				
				-- Check for implicit damage types
				local aImplicitDamageType = {};
				local aLabelWords = StringManager.parseWords(sAttackLabel:lower());
				local i = 1;
				while aLabelWords[i] do
					if aLabelWords[i] == "touch" then
						rAttack.touch = true;
					elseif aLabelWords[i] == "sonic" or aLabelWords[i] == "electricity" then
						table.insert(aImplicitDamageType, aLabelWords[i]);
						break;
					elseif aLabelWords[i] == "adamantine" or aLabelWords[i] == "silver" then
						table.insert(aImplicitDamageType, aLabelWords[i]);
					elseif aLabelWords[i] == "cold" and aLabelWords[i+1] and aLabelWords[i+1] == "iron" then
						table.insert(aImplicitDamageType, "cold iron");
						i = i + 1;
					elseif aLabelWords[i] == "holy" then
						table.insert(aImplicitDamageType, "good");
					elseif aLabelWords[i] == "unholy" then
						table.insert(aImplicitDamageType, "evil");
					elseif aLabelWords[i] == "anarchic" then
						table.insert(aImplicitDamageType, "chaotic");
					elseif aLabelWords[i] == "axiomatic" then
						table.insert(aImplicitDamageType, "lawful");
					else
						if aLabelWords[i]:sub(-1) == "s" then
							aLabelWords[i] = aLabelWords[i]:sub(1, -2);
						end
						if DataCommon.naturaldmgtypes[aLabelWords[i]] then
							table.insert(aImplicitDamageType, DataCommon.naturaldmgtypes[aLabelWords[i]]);
						elseif DataCommon.weapondmgtypes[aLabelWords[i]] then
							table.insert(aImplicitDamageType, DataCommon.weapondmgtypes[aLabelWords[i]]);
						end
					end
					
					i = i + 1;
				end
				
				-- Clean up the attack count field (i.e. magical weapon bonuses up front, no attack count)
				local bMagicAttack = false;
				local bEpicAttack = false;
				local nAttackCount = 1;
				if string.sub(sAttackCount, 1, 1) == "+" then
					bMagicAttack = true;
					if sOptANPC ~= "on" then
						sAttackLabel = sAttackCount .. " " .. sAttackLabel;
					end
					local nAttackPlus = tonumber(sAttackCount) or 1;
					if nAttackPlus > 5 then
						bEpicAttack = true;
					end
				elseif #sAttackCount then
					nAttackCount = tonumber(sAttackCount) or 1;
					if nAttackCount < 1 then
						nAttackCount = 1;
					end
				end

				-- Capitalize first letter of label
				sAttackLabel = StringManager.capitalize(sAttackLabel);
				
				-- If the anonymize option is on, then remove any label text within parentheses or brackets
				if sOptANPC == "on" then
					-- Strip out label information enclosed in ()
					sAttackLabel = string.gsub(sAttackLabel, "%s?%b()", "");

					-- Strip out label information enclosed in []
					sAttackLabel = string.gsub(sAttackLabel, "%s?%b[]", "");
				end

				rAttack.label = sAttackLabel;
				rAttack.count = nAttackCount;
				rAttack.modifier = sAttackModifier;
				
				rDamage.label = sAttackLabel;
				
				local bRanged = false;
				local aTypeWords = StringManager.parseWords(string.lower(sAttackType));
				for kWord, vWord in pairs(aTypeWords) do
					if vWord == "ranged" then
						bRanged = true;
					elseif vWord == "touch" then
						rAttack.touch = true;
					end
				end
				
				-- Determine attack type
				if bRanged then
					rAttack.range = "R";
					rDamage.range = "R";
					rAttack.stat = "dexterity";
					rDamage.stat = "strength";
					rDamage.statmult = 0;
				else
					rAttack.range = "M";
					rDamage.range = "M";
					rAttack.stat = "strength";
					rDamage.stat = "strength";
					rDamage.statmult = 1;
				end

				-- Determine critical information
				rAttack.crit = 20;
				nCritStart, nCritEnd, sCritThreshold = string.find(sDamage, "/(%d+)%-20");
				if sCritThreshold then
					rAttack.crit = tonumber(sCritThreshold) or 20;
					if rAttack.crit < 2 or rAttack.crit > 20 then
						rAttack.crit = 20;
					end
				end
				
				-- Determine damage clauses
				rDamage.clauses = {};

				local aClausesDamage = {};
				local nIndexDamage = 1;
				local nStartDamage, nEndDamage;
				while nIndexDamage < #sDamage do
					nStartDamage, nEndDamage = string.find(sDamage, ' plus ', nIndexDamage);
					if nStartDamage then
						table.insert(aClausesDamage, string.sub(sDamage, nIndexDamage, nStartDamage - 1));
						nIndexDamage = nEndDamage;
					else
						table.insert(aClausesDamage, string.sub(sDamage, nIndexDamage));
						nIndexDamage = #sDamage;
					end
				end

				for kClause, sClause in pairs(aClausesDamage) do
					local aDamageAttrib = StringManager.split(sClause, "/", true);
					
					local aWordType = {};
					local sDamageRoll, sDamageTypes = string.match(aDamageAttrib[1], "^([d%d%+%-%s]+)([%w%s,]*)");
					if sDamageRoll then
						if sDamageTypes then
							table.insert(aWordType, sDamageTypes);
						end
						
						local sCrit;
						for nAttrib = 2, #aDamageAttrib do
							sCrit, sDamageTypes = string.match(aDamageAttrib[nAttrib], "^x(%d)([%w%s,]*)");
							if not sCrit then
								sDamageTypes = string.match(aDamageAttrib[nAttrib], "^%d+%-20%s?([%w%s,]*)");
							end
							
							if sDamageTypes then
								table.insert(aWordType, sDamageTypes);
							end
						end
						
						local aWordDice, nWordMod = StringManager.convertStringToDice(sDamageRoll);
						if #aWordDice > 0 or nWordMod ~= 0 then
							local rDamageClause = { dice = {} };
							for kDie, vDie in ipairs(aWordDice) do
								table.insert(rDamageClause.dice, vDie);
							end
							rDamageClause.modifier = nWordMod;

							if kClause == 1 then
								rDamageClause.mult = 2;
							else
								rDamageClause.mult = 1;
							end
							rDamageClause.mult = tonumber(sCrit) or rDamageClause.mult;

							local aDamageType = ActionDamage.getDamageTypesFromString(table.concat(aWordType, ","));
							if #aDamageType == 0 then
								for kType, sType in ipairs(aImplicitDamageType) do
									table.insert(aDamageType, sType);
								end
							end
							if bMagicAttack then
								table.insert(aDamageType, "magic");
							end
							if bEpicAttack then
								table.insert(aDamageType, "epic");
							end
							rDamageClause.dmgtype = table.concat(aDamageType, ",");
							
							table.insert(rDamage.clauses, rDamageClause);
						end
					end
				end
				
				if #(rDamage.clauses) > 0 then
					if bRanged then
						local nDmgBonus = rDamage.clauses[1].modifier;
						if nDmgBonus > 0 then
							local nStatBonus = ActorManager.getAttributeBonus(rActor, "strength");
							if (nDmgBonus >= nStatBonus) then
								rDamage.statmult = 1;
							end
						end
					else
						local nDmgBonus = rDamage.clauses[1].modifier;
						local nStatBonus = ActorManager.getAttributeBonus(rActor, "strength");
						
						if (nStatBonus > 0) and (nDmgBonus > 0) then
							if nDmgBonus >= math.floor(nStatBonus * 1.5) then
								rDamage.statmult = 1.5;
							elseif nDmgBonus >= nStatBonus then
								rDamage.statmult = 1;
							else
								rDamage.statmult = 0.5;
							end
						elseif (nStatBonus == 1) and (nDmgBonus == 0) then
							rDamage.statmult = 0.5;
						end
					end
				end

				-- Add to roll list
				table.insert(rAttackRolls, rAttack);
				table.insert(rDamageRolls, rDamage);

				-- Add to combo
				table.insert(aCurrentCombo, nAttackIndex);
				nAttackIndex = nAttackIndex + 1;
			end

			nLineIndex = nLineIndex + #sAND;
			nLineIndex = nLineIndex + aSkipOR[kOR][kAND];
		end

		-- Finish combination
		if #aCurrentCombo > 0 then
			table.insert(rAttackCombos, aCurrentCombo);
			aCurrentCombo = {};
		end
	end
	
	return rAttackRolls, rDamageRolls, rAttackCombos;
end

-- post init roll
function postInit(rSource)
	local passbase = NodeManager.get(rSource.nodeCT, "passbase", 1);
	passbase = passbase + NodeManager.get(rSource.nodeCT, "passbonus", 0);
	NodeManager.set(rSource.nodeCT, "ctpassleft", "number", passbase);
	DB.findNode("combattracker_props").getChild("bmaxip").setValue(0);
	return true;
end

