-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

-- AKA The Never Empty List

function onInit()
	-- NOTE: Disabled, since it causes infinite loop when powerlistitem deleted after drop when client connected
	--checkForEmpty();
	
	local localmenutext = "Add Item";
	if menutext then
		localmenutext = "Add " .. menutext[1];
	end
	registerMenuItem(localmenutext, "pointer", 2);
end

function addEntry(bFocus)
	local win = NodeManager.createWindow(self);
	if bFocus and win then
		win.label.setFocus();
	end
	return win;
end

function onEnter()
	addEntry(true);
	return true;
end

function onMenuSelection(selection)
	if selection == 2 then
		addEntry(true);
	end
end

function checkForEmpty()
	if not getNextWindow(nil) then
		addEntry(false);
	end
end

function onDrop(x, y, draginfo)
	if draginfo.isType("number") then
		if string.match(draginfo.getDescription(), "%[SAVE%]") then
			local wnd = getWindowAt(x,y);
			if wnd then
				local custom = draginfo.getCustomData();
				if not custom then
					custom = {};
				end
				custom["effect"] = wnd.getDatabaseNode();
				draginfo.setCustomData(custom);
			end
		end
	end
end

function deleteChild(child, checkforempty_flag)
	local nodeChild = child.getDatabaseNode();
	if nodeChild then
		nodeChild.delete();
	else
		child.close();
	end

	if checkforempty_flag then
		checkForEmpty();
	end
end

function reset(checkforempty_flag)
	for k,v in pairs(getWindows()) do
		local nodeWin = v.getDatabaseNode();
		if nodeWin then
			nodeWin.delete();
		else
			v.close();
		end
	end

	if checkforempty_flag then
		checkForEmpty();
	end
end
