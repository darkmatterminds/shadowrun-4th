-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

enableglobaltoggle = true;
enablevisibilitytoggle = true;

ct_active_name = "";
aHostTargeting = {};

function onInit()
	Interface.onHotkeyActivated = onHotkey;
	DB.findNode("combattracker_props").getChild("bmaxip").setValue(0)
	getMaxIniPass();
	-- Make sure all the clients can see the combat tracker
	for k,v in ipairs(User.getActiveUsers()) do
		NodeManager.addWatcher("combattracker", v);
		NodeManager.addWatcher("combattracker_props", v);
	end
	
	addOwnerShip();
	
	-- Create a blank window if one doesn't exist already
	if not getNextWindow(nil) then
		addEntry(true);
	end
	
	-- Register a menu item to create a CT entry
	registerMenuItem("Create Item", "insert", 5);

	-- Rebuild targeting information
	TargetingManager.rebuildClientTargeting();

	-- Initialize global buttons
	onVisibilityToggle();
	onEntrySectionToggle();
end

function addEntry(bFocus)
	local win = NodeManager.createWindow(self);
	if bFocus and win then
		win.name.setFocus();
	end
	return win;
end

function onSortCompare(w1, w2)
	local nValue1 = w1.ctinitresult.getValue();
	local nValue2 = w2.ctinitresult.getValue();
	if nValue1 ~= nValue2 then
		return nValue1 < nValue2;
	end
	
	nValue1 = w1.initbase.getValue();
	nValue2 = w2.initbase.getValue();
	if nValue1 ~= nValue2 then
		return nValue1 < nValue2;
	end
	
	local sValue1 = string.lower(w1.name.getValue());
	local sValue2 = string.lower(w2.name.getValue());
	if sValue1 ~= sValue2 then
		return sValue1 > sValue2;
	end

	return w1.getDatabaseNode().getNodeName() > w2.getDatabaseNode().getNodeName();
end

function onMenuSelection(selection)
	if selection == 5 then
		addEntry(true);
	end
end

function onHotkey(draginfo)
	local sDragType = draginfo.getType();
	if sDragType == "combattrackernextactor" then
		nextActor();
		return true;
	elseif sDragType == "combattrackernextround" then
		nextRound(1);
		return true;
	end
end

function deleteTarget(sNode)
	TargetingManager.removeTargetFromAllEntries("host", sNode);
end

function toggleVisibility()
	if not enablevisibilitytoggle then
		return;
	end
	
	local visibilityon = window.button_global_visibility.getState();
	for k,v in pairs(getWindows()) do
		if v.type.getValue() ~= "pc" then
			if visibilityon ~= v.show_npc.getState() then
				v.show_npc.setState(visibilityon);
			end
		end
	end
end

function toggleTargeting()
	if not enableglobaltoggle then
		return;
	end
	
	local targetingon = window.button_global_targeting.getValue();
	for k,v in pairs(getWindows()) do
		if targetingon ~= v.activatetargeting.getValue() then
			v.activatetargeting.setValue(targetingon);
			v.setTargetingVisible(v.activatetargeting.getValue());
		end
	end
end

function toggleActive()
	if not enableglobaltoggle then
		return;
	end
	
	local activeon = window.button_global_active.getValue();
	for k,v in pairs(getWindows()) do
		if activeon ~= v.activateactive.getValue() then
			v.activateactive.setValue(activeon);
			v.setActiveVisible(v.activateactive.getValue());
		end
	end
end

function toggleDefensive()
	if not enableglobaltoggle then
		return;
	end
	
	local defensiveon = window.button_global_defensive.getValue();
	for k,v in pairs(getWindows()) do
		if defensiveon ~= v.activatedefensive.getValue() then
			v.activatedefensive.setValue(defensiveon);
			v.setDefensiveVisible(v.activatedefensive.getValue());
		end
	end
end

function toggleSpacing()
	if not enableglobaltoggle then
		return;
	end
	
	local spacingon = window.button_global_spacing.getValue();
	for k,v in pairs(getWindows()) do
		if spacingon ~= v.activatespacing.getValue() then
			v.activatespacing.setValue(spacingon);
			v.setSpacingVisible(v.activatespacing.getValue());
		end
	end
end

function toggleEffects()
	if not enableglobaltoggle then
		return;
	end
	
	local effectson = window.button_global_effects.getValue();
	for k,v in pairs(getWindows()) do
		if effectson ~= v.activateeffects.getValue() then
			v.activateeffects.setValue(effectson);
			v.setEffectsVisible(v.activateeffects.getValue());
			v.effects.checkForEmpty();
		end
	end
end

function onVisibilityToggle()
	local anyVisible = false;
	for k,v in pairs(getWindows()) do
		if v.type.getValue() ~= "pc" and v.show_npc.getState() then
			anyVisible = true;
		end
	end
	
	enablevisibilitytoggle = false;
	window.button_global_visibility.setState(anyVisible);
	enablevisibilitytoggle = true;
end

function onEntrySectionToggle()
	local anyTargeting = false;
	local anyActive = false;
	local anyDefensive = false;
	local anySpacing = false;
	local anyEffects = false;

	for k,v in pairs(getWindows()) do
		if v.activatetargeting.getValue() then
			anyTargeting = true;
		end
		if v.activatespacing.getValue() then
			anySpacing = true;
		end
		if v.activatedefensive.getValue() then
			anyDefensive = true;
		end
		if v.activateactive.getValue() then
			anyActive = true;
		end
		if v.activateeffects.getValue() then
			anyEffects = true;
		end
	end

	enableglobaltoggle = false;
	window.button_global_targeting.setValue(anyTargeting);
	window.button_global_active.setValue(anyActive);
	window.button_global_defensive.setValue(anyDefensive);
	window.button_global_spacing.setValue(anySpacing);
	window.button_global_effects.setValue(anyEffects);
	enableglobaltoggle = true;
end

function addPc(source)

	-- Parameter validation
	if not source then
		return nil;
	end

	-- Create a new combat tracker window
	local win = addEntry(false);
	if not win then
		return nil;
	end
	
	-- Shortcut
	win.link.setValue("charsheet", source.getNodeName());

	-- Type
	-- NOTE: Set to PC after link set, so that fields are linked correctly
	win.type.setValue("pc");

	-- Token
	local tokenval = NodeManager.get(source, "combattoken", nil);
	if tokenval then
		win.token.setPrototype(tokenval);
	end

	-- FoF
	win.friendfoe.setStringValue("friend");
	return win;

end

function addOwnerShip()
	for k,v in ipairs(User.getActiveUsers()) do
		for x,y in ipairs (getWindows()) do
			if y.type.getValue() == "pc" then
				local nodeOwner = y.link.getTargetDatabaseNode();
				local sOwner = nodeOwner.getOwner();
				local sEntryNode = y.getDatabaseNode().getNodeName();
				if sOwner == v then
				
					NodeManager.addWatcher(sEntryNode, v, true);
				else
					NodeManager.addWatcher(sEntryNode, v, false);
				end
			end
		end
		NodeManager.addWatcher("combattracker_props", v);
	end	

end


function addBattle(source)
	-- Parameter validation
	if not source then
		return nil;
	end

	-- Cycle through the NPC list, and add them to the tracker
	local nodeList = source.getChild("npclist");
	if nodeList then
		for k,v in pairs(nodeList.getChildren()) do

			local nodeLink = v.getChild("link");
			if nodeLink then
				
				local aPlacement = {};
				local nodePlacementList = v.getChild("maplink");
				if nodePlacementList then
					for kPlacement, vPlacement in pairs(nodePlacementList.getChildren()) do
						local rPlacement = {};
						rPlacement.imagelink = NodeManager.get(vPlacement, "imagelink", "");
						rPlacement.imagex = NodeManager.get(vPlacement, "imagex", 0);
						rPlacement.imagey = NodeManager.get(vPlacement, "imagey", 0);
						table.insert(aPlacement, rPlacement);
					end
				end
				
				local nCount = NodeManager.get(v, "count", 0);
				for i = 1, nCount do
				
					local npcclass, npcnodename = nodeLink.getValue();
					local npcnode = DB.findNode(npcnodename);
					
					local wnd = addNpc(npcnode, NodeManager.get(v, "name", ""));
					if wnd then
						local npctoken = NodeManager.get(v, "token", "");
						if npctoken ~= "" then
							wnd.token.setPrototype(npctoken);
							
							if aPlacement[i] and aPlacement[i].imagelink ~= "" then
								local tokenAdded = Token.addToken(aPlacement[i].imagelink, npctoken, aPlacement[i].imagex, aPlacement[i].imagey);
								if tokenAdded then
									wnd.token.link(tokenAdded);
								end
							end
						end
					else
						ChatManager.SystemMessage("Could not add '" .. NodeManager.get(v, "name", "") .. "' to combat tracker");
					end
				end
			end
		end
	end
end

function addNpc(source, name)
	-- Parameter validation
	if not source then
		return nil;
	end

	-- Determine the options relevant to adding NPCs
	local sOptNNPC = OptionsManager.getOption("NNPC");
	local sOptRHPS = OptionsManager.getOption("RHPS");
	local sOptINIT = OptionsManager.getOption("INIT");

	-- Create a new NPC window to hold the data
	local win = addEntry(false);
	if not win then
		return nil;
	end
	
	-- SETUP
	local aEffects = {};
	local aAddDamageTypes = {};
	
	-- Shortcut
	win.link.setValue("npc", source.getNodeName());

	-- Type
	win.type.setValue("npc");

	-- Name
	local namelocal = name;
	if not namelocal then
		namelocal = NodeManager.get(source, "name", "");
	end
	local namecount = 0;
	local highnum = 0;
	local last_init = 0;
	win.name.setValue(namelocal);

	-- If multiple NPCs of same name, then figure out what initiative they go on and potentially append a number
	if string.len(namelocal) > 0 then
		for k, v in ipairs(getWindows()) do
			if win.name.getValue() == getWindows()[k].name.getValue() then
				namecount = 0;
				for l, w in ipairs(getWindows()) do
					local check = null;
					if getWindows()[l].name.getValue() == namelocal then
						check = 0;
					elseif string.sub(getWindows()[l].name.getValue(), 1, string.len(namelocal)) == namelocal then
						check = tonumber(string.sub(getWindows()[l].name.getValue(), string.len(namelocal)+2));
					end
					if check then
						namecount = namecount + 1;
						local cur_init = getWindows()[l].ctinitresult.getValue();
						if cur_init ~= 0 then
							last_init = cur_init;
						end
						if highnum < check then
							highnum = check;
						end
					end
				end 
				if sOptNNPC == "append" then
					getWindows()[k].name.setValue(win.name.getValue().." "..highnum+1); 
				elseif sOptNNPC == "random" then
					getWindows()[k].name.setValue(randomName(getWindows(), win.name.getValue())); 
				end
			end
		end
	end
	if namecount < 2 then
        win.name.setValue(namelocal);
	end

	-- Space/reach
	local spacereachstr = NodeManager.get(source, "spacereach", "");
	local space, reach = string.match(spacereachstr, "(%d+)%D*/?(%d+)%D*");
	if space then
		win.space.setValue(space);
		win.reach.setValue(reach);
	end

	-- Token
	local tokenval = NodeManager.get(source, "token", nil);
	if tokenval then
		win.token.setPrototype(tokenval);
	end

	-- FoF
	win.friendfoe.setStringValue("foe");

	-- HP
	if sOptRHPS == "on" then
		local nRandomHP = StringManager.evalDiceString(NodeManager.get(source, "hd", ""), true);
		win.hp.setValue(nRandomHP);
	else
		win.hp.setValue(NodeManager.get(source, "hp", 0));
	end

	-- Defensive properties
	local sAC = NodeManager.get(source, "ac", "10");
	win.ac_final.setValue(tonumber(string.match(sAC, "^(%d+)")) or 10);
	win.ac_touch.setValue(tonumber(string.match(sAC, "touch (%d+)")) or 10);
	local sFlatFooted = string.match(sAC, "flat%-footed (%d+)");
	if not sFlatFooted then
		sFlatFooted = string.match(sAC, "flatfooted (%d+)");
	end
	win.ac_flatfooted.setValue(tonumber(sFlatFooted) or 10);
		
	win.fortitudesave.setValue(NodeManager.get(source, "fortitudesave", 0));
	win.reflexsave.setValue(NodeManager.get(source, "reflexsave", 0));
	win.willsave.setValue(NodeManager.get(source, "willsave", 0));

	-- Active properties
	win.initbase.setValue(NodeManager.get(source, "init", 0));
	win.speed.setValue(NodeManager.get(source, "speed", 0));

	local sAttack = NodeManager.get(source, "atk", "");
	if sAttack ~= "" then
		local wndAttack = NodeManager.createWindow(win.attacks);
		if wndAttack then
			wndAttack.value.setValue(sAttack);
		end
	end
	local sFullAttack = NodeManager.get(source, "fullatk", "");
	if sAttack ~= "" then
		local wndAttack = NodeManager.createWindow(win.attacks);
		if wndAttack then
			wndAttack.value.setValue(sFullAttack);
		end
	end

	-- DECODE MONSTER TYPE QUALITIES
	local sType = string.lower(NodeManager.get(source, "type", ""));
	local sCreatureType, sSubTypes = string.match(sType, "([^(]+) %(([^)]+)%)");
	if not sCreatureType then
		sCreatureType = sType;
	end
	local aSubTypes = {};
	if sSubTypes then
		aSubTypes = StringManager.split(sSubTypes, ",", true);
	end

	if StringManager.contains(aSubTypes, "lawful") then
		table.insert(aAddDamageTypes, "lawful");
	end
	if StringManager.contains(aSubTypes, "chaotic") then
		table.insert(aAddDamageTypes, "chaotic");
	end
	if StringManager.contains(aSubTypes, "good") then
		table.insert(aAddDamageTypes, "good");
	end
	if StringManager.contains(aSubTypes, "evil") then
		table.insert(aAddDamageTypes, "evil");
	end

	-- DECODE SPECIAL QUALITIES
	local sSpecialQualities = string.lower(NodeManager.get(source, "specialqualities", ""));
	
	local aSQWords = StringManager.parseWords(sSpecialQualities);
	local i = 1;
	while aSQWords[i] do
		
		-- DAMAGE REDUCTION
		if StringManager.isWord(aSQWords[i], "dr") or (StringManager.isWord(aSQWords[i], "damage") and StringManager.isWord(aSQWords[i+1], "reduction")) then
			if aSQWords[i] ~= "dr" then
				i = i + 1;
			end
			
			if StringManager.isNumberString(aSQWords[i+1]) then
				i = i + 1;
				local sDRAmount = aSQWords[i];
				local aDRTypes = {};
				
				while aSQWords[i+1] do
					if StringManager.isWord(aSQWords[i+1], { "and", "or" }) then
						table.insert(aDRTypes, aSQWords[i+1]);
					elseif StringManager.isWord(aSQWords[i+1], { "epic", "magic" }) then
						table.insert(aDRTypes, aSQWords[i+1]);
						table.insert(aAddDamageTypes, aSQWords[i+1]);
					elseif StringManager.isWord(aSQWords[i+1], "cold") and StringManager.isWord(aSQWords[i+2], "iron") then
						table.insert(aDRTypes, "cold iron");
						i = i + 1;
					elseif StringManager.isWord(aSQWords[i+1], DataCommon.dmgtypes) then
						table.insert(aDRTypes, aSQWords[i+1]);
					else
						break;
					end

					i = i + 1;
				end
				
				local sDREffect = "DR: " .. sDRAmount;
				if #aDRTypes > 0 then
					sDREffect = sDREffect .. " " .. table.concat(aDRTypes, " ");
				end
				table.insert(aEffects, sDREffect);
			end

		-- SPELL RESISTANCE
		elseif StringManager.isWord(aSQWords[i], "sr") or (StringManager.isWord(aSQWords[i], "spell") and StringManager.isWord(aSQWords[i+1], "resistance")) then
			if aSQWords[i] ~= "sr" then
				i = i + 1;
			end
			
			if StringManager.isNumberString(aSQWords[i+1]) then
				i = i + 1;
				win.sr.setValue(tonumber(aSQWords[i]) or 0);
			end
		
		-- FAST HEALING
		elseif StringManager.isWord(aSQWords[i], "fast") and StringManager.isWord(aSQWords[i+1], { "healing", "heal" }) then
			i = i + 1;
			
			if StringManager.isNumberString(aSQWords[i+1]) then
				i = i + 1;
				table.insert(aEffects, "FHEAL: " .. aSQWords[i]);
			end
		
		-- REGENERATION
		elseif StringManager.isWord(aSQWords[i], "regeneration") then
		
			if StringManager.isNumberString(aSQWords[i+1]) then
				i = i + 1;
				local sRegenAmount = aSQWords[i];
				local aRegenTypes = {};
				
				while aSQWords[i+1] do
					if StringManager.isWord(aSQWords[i+1], { "and", "or" }) then
						table.insert(aRegenTypes, aSQWords[i+1]);
					elseif StringManager.isWord(aSQWords[i+1], "cold") and StringManager.isWord(aSQWords[i+2], "iron") then
						table.insert(aRegenTypes, "cold iron");
						i = i + 1;
					elseif StringManager.isWord(aSQWords[i+1], DataCommon.dmgtypes) then
						table.insert(aRegenTypes, aSQWords[i+1]);
					else
						break;
					end

					i = i + 1;
				end
				
				local sRegenEffect = "REGEN: " .. sRegenAmount;
				if #aRegenTypes > 0 then
					sRegenEffect = sRegenEffect .. " " .. table.concat(aRegenTypes, " ");
				end
				table.insert(aEffects, sRegenEffect);
			end
			
		-- RESISTANCE
		elseif StringManager.isWord(aSQWords[i], "resistance") and StringManager.isWord(aSQWords[i+1], "to") then
			i = i + 1;
		
			while aSQWords[i+1] do
				if StringManager.isWord(aSQWords[i+1], "and") then
					-- SKIP
				elseif StringManager.isWord(aSQWords[i+1], DataCommon.energytypes) and StringManager.isNumberString(aSQWords[i+2]) then
					i = i + 1;
					table.insert(aEffects, "RESIST: " .. aSQWords[i+1] .. " " .. aSQWords[i]);
				else
					break;
				end

				i = i + 1;
			end
			
		-- RESISTANCE
		elseif StringManager.isWord(aSQWords[i], "vulnerattribute") and StringManager.isWord(aSQWords[i+1], "to") then
			i = i + 1;
		
			while aSQWords[i+1] do
				if StringManager.isWord(aSQWords[i+1], "and") then
					-- SKIP
				elseif StringManager.isWord(aSQWords[i+1], DataCommon.energytypes) then
					table.insert(aEffects, "VULN: " .. aSQWords[i+1]);
				else
					break;
				end

				i = i + 1;
			end
			
		-- IMMUNITY
		elseif StringManager.isWord(aSQWords[i], "immunity") and StringManager.isWord(aSQWords[i+1], "to") then
			i = i + 1;
		
			while aSQWords[i+1] do
				if StringManager.isWord(aSQWords[i+1], "and") then
					-- SKIP
				elseif StringManager.isWord(aSQWords[i+1], DataCommon.immunetypes) then
					table.insert(aEffects, "IMMUNE: " .. aSQWords[i+1]);
				else
					break;
				end

				i = i + 1;
			end
			
		-- SPECIAL DEFENSES
		elseif StringManager.isWord(aSQWords[i], "uncanny") and StringManager.isWord(aSQWords[i+1], "dodge") then
			if StringManager.isWord(aSQWords[i-1], "improved") then
				table.insert(aEffects, "Improved Uncanny Dodge");
			else
				table.insert(aEffects, "Uncanny Dodge");
			end
			i = i + 1;
		
		elseif StringManager.isWord(aSQWords[i], "evasion") then
			if StringManager.isWord(aSQWords[i-1], "improved") then
				table.insert(aEffects, "Improved Evasion");
			else
				table.insert(aEffects, "Evasion");
			end
		
		-- TRAITS
		elseif StringManager.isWord(aSQWords[i], "traits") then
			if StringManager.isWord(aSQWords[i-1], "incorporeal") then
				table.insert(aEffects, "Incorporeal");
			elseif StringManager.isWord(aSQWords[i-1], "construct") then
				table.insert(aEffects, "Construct traits");
			elseif StringManager.isWord(aSQWords[i-1], "undead") then
				table.insert(aEffects, "Undead traits");
			elseif StringManager.isWord(aSQWords[i-1], "swarm") then
				table.insert(aEffects, "Swarm traits");
			end
		end
	
		-- ITERATE SPECIAL QUALITIES DECODE
		i = i + 1;
	end

	-- FINISH ADDING EXTRA DAMAGE TYPES
	if #aAddDamageTypes > 0 then
		table.insert(aEffects, "DMGTYPE: " .. table.concat(aAddDamageTypes, ","));
	end
	
	-- ADD DECODED EFFECTS
	if #aEffects > 0 then
		EffectsManager.addEffect("", "", win.getDatabaseNode(), { sName = table.concat(aEffects, "; "), nDuration = 0, nGMOnly = 1 }, false);
	end

	--Roll initiative and sort
	if sOptINIT == "group" then
		if (namecount < 2) or (last_init == 0) then
			win.ctinitresult.setValue(math.random(20) + win.initbase.getValue());
		else
			win.ctinitresult.setValue(last_init);
		end
		applySort();
	elseif sOptINIT == "on" then
		win.ctinitresult.setValue(math.random(20) + win.initbase.getValue());
		applySort();
	end

	return win;
end

function onDrop(x, y, draginfo)
	-- Capture certain drag types meant for the host only
	local dragtype = draginfo.getType();

	-- PC
	if dragtype == "playercharacter" then
		addPc(draginfo.getDatabaseNode());
		TargetingManager.rebuildClientTargeting();
		return true;
	end

	if dragtype == "shortcut" then
		local class, datasource = draginfo.getShortcutData();

		-- NPC
		if class == "npc" then
			addNpc(draginfo.getDatabaseNode());
			return true;
		end

		-- ENCOUNTER
		if class == "battle" then
			addBattle(draginfo.getDatabaseNode());
			return true;
		end
	end

	-- Capture any drops meant for specific CT entries
	local wnd = getWindowAt(x,y);
	if wnd then
		local nodeWin = wnd.getDatabaseNode();
		if nodeWin then
			return CombatCommon.onDrop("ct", nodeWin.getNodeName(), draginfo);
		end
	end
end

function getActiveEntry()
	for k, v in ipairs(getWindows()) do
		if v.isActive() then
			return v;
		end
	end
	
	return nil;
end

function requestActivation(entry)

	-- Make all the CT entries inactive
	for k, v in ipairs(getWindows()) do
		v.setActive(false);
	end
	
	-- Make the given CT entry active
	entry.setActive(true);

	-- Scroll to the CT window
	scrollToWindow(entry);

	-- If we created a new speaker, then remove it
	if ct_active_name ~= "" then
		GmIdentityManager.removeIdentity(ct_active_name);
		ct_active_name = "";
	end

	-- Check the option to set the active CT as the GM voice
	if OptionsManager.isOption("CTAV", "on") then
		-- Set up the current CT entry as the speaker if NPC, otherwise just change the GM voice
		if entry.type.getValue() == "pc" then
			GmIdentityManager.activateGMIdentity();
		else
			local name = entry.name.getValue();
			if GmIdentityManager.existsIdentity(name) then
				GmIdentityManager.setCurrent(name);
			else
				ct_active_name = name;
				GmIdentityManager.addIdentity(name);
			end
		end
	end
end

function checkInitroll()
	local bInitRolled = true;
	
	for k,win in ipairs(getWindows()) do
		if win.getDatabaseNode().getChild("ctinitrolled").getValue() == 0 then 
			bInitRolled = false;
		end
	end
	
	if bInitRolled == false then
	local msg = {font = "narratorfont", icon = "indicator_flag"};
	msg.text = "[ATTENTION, not all initiatives Rolled ";
	Comm.deliverChatMessage(msg);
	end
	return bInitRolled;
end

function nextActor()
	local bInitRolled = checkInitroll();
	local active = getActiveEntry();

	
 	local bMaxIP = DB.findNode("combattracker_props").getChild("bmaxip").getValue();
	if bMaxIP == 0 then
		getMaxIniPass();
	end
	
	if bInitRolled == true then	
		-- Check for stabilization
			-- Process dying state for this actor first (PC only)
		
		-- Find the next actor.  If no next actor, then start the next round
		local nextactor = getNextWindow(active);

		if nextactor then
			local isActive = NodeManager.get(nextactor.getDatabaseNode(), "ctpassleft", 0)
			while isActive == 0 do
				nextactor = getNextWindow(nextactor);
				if not nextactor then
				nextPass(1)
				else
				isActive = NodeManager.get(nextactor.getDatabaseNode(), "ctpassleft", 0)
				end
			end

			if active then
				EffectsManager.processEffects(getDatabaseNode(), active.getDatabaseNode(), nextactor.getDatabaseNode());
			else
				EffectsManager.processEffects(getDatabaseNode(), nil, nextactor.getDatabaseNode());
			end
			requestActivation(nextactor);
		else
			nextPass(1);
		end
	else
	end
end

function getMaxIniPass()
	local maxIP = 0;
	if DB.findNode("combattracker_props").getChild("bmaxip").getValue() == 0 then
		for k,win in ipairs(getWindows()) do
			if maxIP < win.ctpassleft.getValue() then 
				maxIP = win.ctpassleft.getValue()
			end

		end
		DB.findNode("combattracker_props").createChild("maxip", "number").setValue(maxIP);
		DB.findNode("combattracker_props").getChild("bmaxip").setValue(1);
		return maxIP;
	end
end

function resetAfterPass()
		for k,win in ipairs(getWindows()) do
			win.getDatabaseNode().getChild("ctaction1").setValue(0);
			win.getDatabaseNode().getChild("ctaction2").setValue(0);
			win.getDatabaseNode().getChild("ctactionfree").setValue(0);
			local passleft = win.getDatabaseNode().getChild("ctpassleft").getValue();
			if passleft > 0 then
			passleft = passleft - 1
			end
			win.getDatabaseNode().getChild("ctpassleft").setValue(passleft)
		end
end

function resetAfterRound()
		for k,win in ipairs(getWindows()) do
			win.getDatabaseNode().getChild("ctaction1").setValue(0);
			win.getDatabaseNode().getChild("ctaction2").setValue(0);
			win.getDatabaseNode().getChild("ctactionfree").setValue(0);
			local passleft = 0
			passleft = win.getDatabaseNode().getChild("passbase").getValue();
			passleft = passleft + win.getDatabaseNode().getChild("passbonus").getValue();
			win.getDatabaseNode().getChild("ctpassleft").setValue(passleft)
			win.setActive(false);
		end
	clearactive();
end



function nextPass(nPasses)
	-- IF ACTIVE ACTOR, THEN PROCESS EFFECTS
	local nStartCounter = 1;
	local active = getActiveEntry();
	if active then
		EffectsManager.processEffects(getDatabaseNode(), active.getDatabaseNode(), nil);
		active.setActive(false);
		nStartCounter = nStartCounter + 1;
	end
	for i = nStartCounter, nPasses do
		EffectsManager.processEffects(getDatabaseNode(), nil, nil);
	end

	-- ADVANCE ROUND COUNTER
	window.passcounter.setValue(window.passcounter.getValue() + nPasses);
	
	-- ANNOUNCE NEW ROUND
	local msg = {font = "narratorfont", icon = "indicator_flag"};
	msg.text = "[PASS " .. window.passcounter.getValue() .. "]";
	Comm.deliverChatMessage(msg);
	
	-- CHECK OPTION TO SEE IF WE SHOULD GO AHEAD AND MOVE TO FIRST ROUND
	if OptionsManager.isOption("RNDS", "off") and getNextWindow(nil) then
		nextActor();
	end
	
	resetAfterPass();
	
	if window.passcounter.getValue() > window.maxip.getValue() then
	window.passcounter.setValue(1);
	nextRound(1);
	end
	
end

function nextRound(nRounds)
	resetAfterRound();
	getMaxIniPass();
	-- IF ACTIVE ACTOR, THEN PROCESS EFFECTS
	local nStartCounter = 1;
	local active = getActiveEntry();
	if active then
		EffectsManager.processEffects(getDatabaseNode(), active.getDatabaseNode(), nil);
		active.setActive(false);
		nStartCounter = nStartCounter + 1;
	end
	for i = nStartCounter, nRounds do
		EffectsManager.processEffects(getDatabaseNode(), nil, nil);
	end

	-- ADVANCE ROUND COUNTER
	window.roundcounter.setValue(window.roundcounter.getValue() + nRounds);
	
	-- ANNOUNCE NEW ROUND
	local msg = {font = "narratorfont", icon = "indicator_flag"};
	msg.text = "[ROUND " .. window.roundcounter.getValue() .. "]";
	Comm.deliverChatMessage(msg);
	
	-- CHECK OPTION TO SEE IF WE SHOULD GO AHEAD AND MOVE TO FIRST ROUND
	if OptionsManager.isOption("RNDS", "off") and getNextWindow(nil) then
		nextActor();
	end
	-- check option to see if Initiative needs to be rolled again in the following round
	for k, v in ipairs(getWindows()) do
		v.setActive(false);
	end
	if OptionsManager.isOption("SHIN", "on") then
		for y,win in ipairs(getWindows()) do
			win.getDatabaseNode().getChild("ctinitrolled").setValue("0");
			win.setActive(false)
		end
	end
	-- reset bmaxIP so that max IP may be calculated in the new round
	DB.findNode("combattracker_props").getChild("bmaxip").setValue(0);
	getMaxIniPass();
	DB.findNode("combattracker_props").getChild("bmaxip").setValue(0);
	clearactive();
end

function stripCreatureNumber(s)
	local starts, ends, creature_number = string.find(s, " ?(%d+)$");
	if not starts then
		return s;
	end
	return string.sub(s, 1, starts), creature_number;
end

function rollEntryInit(ctentry)
	-- Start with the bsae initiative bonus
	if ctentry.getDatabaseNode().getChild("ctinitrolled").getValue() == 1 then
		rMessage = {}
		rMessage.text = ctentry.getDatabaseNode().getChild("name").getValue() .. " already rolled initiative";
		Comm.deliverChatMessage(rMessage)
		return
	end
	local nCTinitVal = ctentry.initbase.getValue();
	nCTinitVal = nCTinitVal + ctentry.initmod.getValue();
	-- Get the entry's database node name and creature name
	local nInitResult = nCTinitVal;
		for k = 1, nCTinitVal, 1 do
			local nInitDie = math.random(6);
			if nInitDie >= 5 then
				nInitResult = nInitResult + 1;
			end
		end
	ctentry.ctinitresult.setValue(nInitResult)
	local nPassLeft = ctentry.passbase.getValue() + ctentry.passbonus.getValue();
	ctentry.ctpassleft.setValue(nPassLeft)
	ctentry.getDatabaseNode().getChild("ctinitrolled").setValue(1)
	return;
end

function clearactive()
	for k, v in ipairs(getWindows()) do
		v.setActive(false);
	end
	return true;
end

function rollAllInit()
	for k, v in ipairs(getWindows()) do
		clearactive();
		rollEntryInit(v);
	end
end

function rollPCInit()
	for k, v in ipairs(getWindows()) do
		if v.type.getValue() == "pc" then
				clearactive();
			rollEntryInit(v);
		end
	end
end

function rollNPCInit()
	for k, v in ipairs(getWindows()) do
		if v.type.getValue() ~= "pc" then
				clearactive();
			rollEntryInit(v);
		end
	end
end

function resetInit()
	-- Set all CT entries to inactive and reset their init value
	for k, v in ipairs(getWindows()) do
		v.setActive(false);
		v.ctinitresult.setValue(0);
		v.ctpassleft.setValue(0);
		v.getDatabaseNode().getChild("ctinitrolled").setValue(0);
		if v.getDatabaseNode().getChild("ctaction1") then
		v.getDatabaseNode().getChild("ctaction1").setValue(0);
		end
		if v.getDatabaseNode().getChild("ctaction2") then
		v.getDatabaseNode().getChild("ctaction2").setValue(0);
		end
		if v.getDatabaseNode().getChild("ctactionfree") then
		v.getDatabaseNode().getChild("ctactionfree").setValue(0);
		end
		addOwnerShip();
end
	
	-- Remove the active CT from the speaker list
	if ct_active_name ~= "" then
		GmIdentityManager.removeIdentity(ct_active_name);
		ct_active_name = "";
	end

	-- Reset the round counter
	window.roundcounter.setValue(1);
	window.passcounter.setValue(1);
end

function clearExpiringEffects()
	for k, v in ipairs(getWindows()) do
		local effcount = #(v.effects.getWindows());

		-- Clear any effects that have an expiration value
		for k2, v2 in ipairs(v.effects.getWindows()) do
			if v2.duration.getValue() ~= 0 then
				v.effects.deleteChild(v2, false);
				effcount = effcount - 1;
			end
		end
		
		-- If no effects left, then clear the effects completely
		if effcount == 0 then
			v.effects.checkForEmpty();
			v.activateeffects.setValue(false);
			v.setEffectsVisible(false);
		end
	end
	
	-- Synch the global effects toggle
	onEntrySectionToggle();
end

function resetEffects()
	for k, v in ipairs(getWindows()) do
		-- Delete all current effects
		v.effects.reset(true);

		-- Hide the effects sub-section
		v.activateeffects.setValue(false);
		v.setEffectsVisible(false);
	end
	
	-- Synch the global effects toggle
	onEntrySectionToggle();
end

function deleteNPCs()
	for k, v in ipairs(getWindows()) do
		if v.type.getValue() ~= "pc" then
			v.delete();
		end
	end
end

function randomName(wintable, base_name)
	local new_name = base_name .. " " .. math.random(#wintable * 2) + 1	
	for l, w in ipairs(wintable) do
		if wintable[l].name.getValue() == new_name then
			new_name = randomName(wintable,base_name);
		end
	end
	return new_name
end
