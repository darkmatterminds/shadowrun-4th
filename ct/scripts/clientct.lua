-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onSortCompare(w1, w2)
	local nValue1 = w1.ctinitresult.getValue();
	local nValue2 = w2.ctinitresult.getValue();
	if nValue1 ~= nValue2 then
		return nValue1 < nValue2;
	end
	
	nValue1 = w1.initbase.getValue();
	nValue2 = w2.initbase.getValue();
	if nValue1 ~= nValue2 then
		return nValue1 < nValue2;
	end
	
	local sValue1 = string.lower(w1.name.getValue());
	local sValue2 = string.lower(w2.name.getValue());
	if sValue1 ~= sValue2 then
		return sValue1 > sValue2;
	end

	return w1.getDatabaseNode().getName() > w2.getDatabaseNode().getName();
end

function onFilter(w)
	if w.type.getValue() == "pc" then
		return true;
	end
	if w.show_npc.getValue() ~= 0 then
		return true;
	end
	return false;
end

function onDrop(x, y, draginfo)
	local wnd = getWindowAt(x,y);
	if wnd then
		local nodeWin = wnd.getDatabaseNode();
		if nodeWin then
			return CombatCommon.onDrop("ct", nodeWin.getNodeName(), draginfo);
		end
	end
end

function onClickDown(button, x, y)
	if (Input.isShiftPressed()) then
		return true;
	end
end

function onClickRelease(button, x, y)
	if (Input.isShiftPressed()) then
		local wnd = getWindowAt(x, y);
		if wnd then
			TokenManager.toggleClientTarget(wnd.getDatabaseNode());
		end

		return true;
	end
end
