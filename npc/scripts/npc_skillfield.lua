-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function getCompletion(str)
	if senses_only then
		if string.lower(str) == string.lower(string.sub("Listen", 1, #str)) then
			return string.sub("Listen", #str + 1);
		end
		if string.lower(str) == string.lower(string.sub("Spot", 1, #str)) then
			return string.sub("Spot", #str + 1);
		end
	else
		-- Find a matching completion for the given string
		for k, t in pairs(DataCommon.skilldata) do
			if string.lower(str) == string.lower(string.sub(k, 1, #str)) then
				return string.sub(k, #str + 1);
			end
		end
	end

	return "";
end

function parseComponents(s)
	local skills = {};
	
	-- Get the comma-separated strings
	local clauses, clauses_stats = StringManager.split(s, ",\r", true);
	
	-- Check each comma-separated string for a potential skill roll or auto-complete opportunity
	for i = 1, #clauses do
		local starts, ends, label_val, sign_val, mod_val = string.find(clauses[i], "([%w%s\(\)]*[%w\(\)]+)%s*([%+%-�]?)(%d*)");
		if starts then
			-- Calculate modifier based on mod value and sign value, if any
			local allow_roll_val = 0;
			local mod = 0;
			if mod_val ~= "" then
				allow_roll_val = 1;
				mod = tonumber(mod_val) or 0;
				if sign_val == "-" or sign_val == "�" then
					mod = 0 - mod;
				end
			end

			-- Insert the possible skill into the skill list
			table.insert(skills, {startpos = clauses_stats[i].startpos, labelendpos = clauses_stats[i].startpos + ends, endpos = clauses_stats[i].endpos, label = label_val, mod = mod, allow_roll = allow_roll_val });
		end
	end
	
	return skills;
end

function onChar(keycode)
	local nCursor = getCursorPosition();
	local sValue = getValue();
	
	-- If alpha character, then build a potential autocomplete
	if ((keycode >= 65) and (keycode <= 90)) or ((keycode >= 97) and (keycode <= 122)) then
		-- Parse the value string
		components = parseComponents(sValue);

		-- Build auto-complete for the current string
		for i = 1, #components, 1 do
			if nCursor == components[i].labelendpos then
				sCompletion = getCompletion(components[i].label);
				if sCompletion ~= "" then
					local sNewValue = string.sub(sValue, 1, getCursorPosition()-1) .. sCompletion .. string.sub(sValue, getCursorPosition());
					setValue(sNewValue);
					setSelectionPosition(nCursor + #sCompletion);
				end

				return;
			end
		end

	-- Or else if space character, then finish the autocomplete
	else
		if ((keycode == 32) and (nCursor >= 2)) then
			-- Parse the value string
			components = parseComponents(sValue);
			
			-- Find any string we may have just auto-completed
			local nLastCursor = nCursor - 1;
			for i = 1, #components, 1 do
				if nCursor - 1 == components[i].labelendpos then
					sCompletion = getCompletion(components[i].label);
					if sCompletion ~= "" then
						local sNewValue = string.sub(sValue, 1, nLastCursor - 1) .. sCompletion .. string.sub(sValue, nLastCursor);
						setValue(sNewValue);
						setCursorPosition(nCursor + #sCompletion);
						setSelectionPosition(nCursor + #sCompletion);
					end

					return;
				end
			end
		end
	end
end

function onHover(oncontrol)
	if dragging then
		return;
	end

	-- Reset selection when the cursor leaves the control
	if not oncontrol then
		dragLabel = nil;
		dragMod = nil;
		
		--setCursorPosition(0);
		setSelectionPosition(0);
	end
end

function onHoverUpdate(x, y)
	if dragging then
		return;
	end

	-- Hilight skill hovered on
	components = parseComponents(getValue());
	local index = getIndexAt(x, y);

	for i = 1, #components, 1 do
		if components[i].allow_roll == 1 then
			if components[i].startpos <= index and components[i].endpos > index then
				setCursorPosition(components[i].startpos);
				setSelectionPosition(components[i].endpos);

				dragLabel = components[i].label;
				dragMod = components[i].mod;

				setHoverCursor("hand");

				return;
			end
		end
	end
	
	-- The dragMod and dragLabel fields keep track of the entry under the cursor
	dragLabel = nil;
	dragMod = nil;
	
	setHoverCursor("arrow");
	
	--setCursorPosition(0);
end

function action(draginfo)
	if not dragLabel then
		return true;
	end

	local rActor = ActorManager.getActor("npc", window.getDatabaseNode());
	ActionSkill.performRoll(draginfo, rActor, dragLabel, dragMod);
	
	return true;
end

function onDoubleClick(x, y)
	return action();
end

function onDragStart(button, x, y, draginfo)
	action(draginfo);

	dragging = true;
	setCursorPosition(0);
	
	return true;
end

function onDragEnd(dragdata)
	dragging = false;
end

function onClickDown(button, x, y)
	-- Suppress default processing to support dragging
	return true;
end

function onClickRelease(button, x, y)
	-- Enable edit mode on mouse release
	setFocus();
	
	local n = getIndexAt(x, y);
	
	setSelectionPosition(n);
	setCursorPosition(n);
	
	return true;
end
