-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

local sSort = "value";
local sFocus = "value";

function onInit()
	if sortorder then
		sSort = sortorder[1];
	end
	
	if newfocus then
		sFocus = newfocus[1];
	end
end

function onSortCompare(w1, w2)
	if w1[sSort] and w2[sSort] then
		local sValue1 = string.lower(w1[sSort].getValue());
		local sValue2 = string.lower(w2[sSort].getValue());
		if sValue1 ~= sValue2 then
			return sValue1 > sValue2;
		end
	end

	return w1.getDatabaseNode().getName() > w2.getDatabaseNode().getName();
end

function onClickDown(button, x, y)
	if not readonly then
		return true;
	end
end

function onClickRelease(button, x, y)
 	if not readonly then
		if not getNextWindow(nil) then
			addEntry(true);
		end
		return true;
	end
end

function addEntry(bFocus)
	local win = NodeManager.createWindow(self);
	if bFocus and win and win[sFocus] then
		win[sFocus].setFocus();
	end
	return win;
end
