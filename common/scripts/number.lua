-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	if gmonly and not User.isHost() then
		setReadOnly(true);
	end
	
	if rollable then
		addBitmapWidget("indicator_fullattackdie").setPosition("bottomleft", -1, -4);
	end
end

function onDrop(x, y, draginfo)
	if draginfo.getType() ~= "number" then
		return false;
	end
end

function onWheel(n)
	if isReadOnly() then
		return false;
	end
	
	if not OptionsManager.isMouseWheelEditEnabled() then
		return false;
	end
	
	setValue(getValue() + n);
	return true;
end
