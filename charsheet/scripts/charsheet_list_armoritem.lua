-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	local nodename = getDatabaseNode().getParent().getParent().getName();
	registerMenuItem("Add Modifier", "pointer", 2);
	-- Build our own delete item menu and Add Modifier
	toggleDetail();
	toggleArmorModifier();
end	

function onMenuSelection(selection)
	if selection == 2 then
		local wnd = list_armormodifier.createWindow();
			if wnd then
				NodeManager.set(wnd.getDatabaseNode(), "armormodifiername", "string", "");
			end
	end
end


function toggleDetail()
	local status = activatearmordetail.getValue();

	-- Show the power details
	armordescription.setVisible(status);
end

function toggleArmorModifier()
	local status = activatearmormodifier.getValue();
		list_armormodifier.setVisible(status);
	
	for k,v in pairs(list_armormodifier.getWindows()) do
		v.updateDisplay();
	end

end

