-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	local nodename = getDatabaseNode().getParent().getParent().getName();
	registerMenuItem("Add Modifier", "pointer", 2);
	-- Build our own delete item menu and Add Modifier
	toggleDetail();
	toggleComlinkModifier();
end	

function onMenuSelection(selection)
	if selection == 2 then
		local wnd = list_comlinkmodifier.createWindow();
	end
end


function toggleDetail()
	local status = activatecomlinkdetail.getValue();

	-- Show the power details
	comlinkdescription.setVisible(status);
end

function toggleComlinkModifier()
	local status = activatecomlinkmodifier.getValue();
		list_comlinkmodifier.setVisible(status);
	
	for k,v in pairs(list_comlinkmodifier.getWindows()) do
		v.updateDisplay();
	end

end


function modifierProgramDrop(vmodifiername, vmodifier, vmodifiernode)
	local newitem = true
	for i,v in pairs(list_comlinkmodifier.getWindows()) do
		local a = v.getDatabaseNode().getChild("comlinkmodifiernode").getValue();
			if a == vmodifiernode then
				v.getDatabaseNode().getChild("comlinkmodifiername").setValue(vmodifiername);
				v.getDatabaseNode().getChild("comlinkmodifiercount").setValue(1);
				v.getDatabaseNode().getChild("comlinkmodifiernode").setValue(vmodifiernode);
				v.getDatabaseNode().getChild("comlinkmodifierinusebox").setValue(1);
				newitem = false
			end
	end
	if newitem == true then
		local wnd = list_comlinkmodifier.createWindow();
		if wnd then
			NodeManager.set(wnd.getDatabaseNode(), "comlinkmodifiername", "string", vmodifiername);
			wnd.getDatabaseNode().getChild("comlinkmodifiercount").setValue(1);
			wnd.getDatabaseNode().getChild("comlinkmodifiernode").setValue(vmodifiernode);
			wnd.getDatabaseNode().getChild("comlinkmodifierinusebox").setValue(1);
			end
	end
end

function modifierAgentDrop(vmodifiername, vmodifier, vmodifiernode)
	local newitem = true
	for i,v in pairs(list_comlinkmodifier.getWindows()) do
		local a = v.getDatabaseNode().getChild("comlinkmodifiernode").getValue();
			if a == vmodifiernode then
				v.getDatabaseNode().getChild("comlinkmodifiername").setValue(vmodifiername);
				v.getDatabaseNode().getChild("comlinkmodifiercount").setValue(vmodifier+1);
				v.getDatabaseNode().getChild("comlinkmodifiernode").setValue(vmodifiernode);
				v.getDatabaseNode().getChild("comlinkmodifierinusebox").setValue(1);
				newitem = false
			end
	end
	if newitem == true then
		local wnd = list_comlinkmodifier.createWindow();
		if wnd then
			NodeManager.set(wnd.getDatabaseNode(), "comlinkmodifiername", "string", vmodifiername);
			wnd.getDatabaseNode().getChild("comlinkmodifiercount").setValue(vmodifier+1);
			wnd.getDatabaseNode().getChild("comlinkmodifiernode").setValue(vmodifiernode);
			wnd.getDatabaseNode().getChild("comlinkmodifierinusebox").setValue(1);
			end
	end
end
function modifierIdDrop(vmodifiername, vmodifier, vmodifiernode)
	getDatabaseNode().getChild("comlinkid").setValue(vmodifiername);
	getDatabaseNode().getChild("comlinkidrating").setValue(vmodifier);
end
					
function updateColorframe()
	local a = getDatabaseNode().getChild("color").getValue();
	if not a or a == "" then
		getDatabaseNode().getChild("color").setValue("ffffffff");
	end
	local b = getDatabaseNode().getChild("frame").getValue();
	if not b or b == "" then
		getDatabaseNode().getChild("frame").setValue("dailyframe");
	end
end

function updateResponse()
	if not getDatabaseNode().getChild("comlinkisactive") then
		getDatabaseNode().createChild("comlinkisactive", "number")
	end
	if getDatabaseNode().getChild("comlinkisactive").getValue() == 1 then	
		if windowlist.window.getDatabaseNode().getChild("base.attribute.comlink.response") then
			local response = comlinkresponse.getValue() + comlinkresponsepenalty.getValue();
			windowlist.window.getDatabaseNode().getChild("base.attribute.comlink.response").setValue(response);
		end
	end
end