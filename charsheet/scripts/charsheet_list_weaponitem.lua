-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	local nodename = getDatabaseNode().getParent().getParent().getName();
	registerMenuItem("Add Modifier", "pointer", 2);
	-- Build our own delete item menu and Add Modifier
	toggleDetail();
	toggleAmmunition();
end	

function onMenuSelection(selection)
	if selection == 2 then
		local wnd = list_ammunition.createWindow();

	end
end


function toggleDetail()
	local status = activateweapondetail.getValue();

	-- Show the power details
	weapondescription.setVisible(status);
end

function toggleAmmunition()
	local status = activateammunition.getValue();
		list_ammunition.setVisible(status);
	
	for k,v in pairs(list_ammunition.getWindows()) do
		v.updateDisplay();
	end

end

function updateColorframe()
	local a = getDatabaseNode().getChild("color").getValue();
	if not a or a == "" then
		getDatabaseNode().getChild("color").setValue("ffffffff");
	end
	local b = getDatabaseNode().getChild("frame").getValue();
	if not b or b == "" then
		getDatabaseNode().getChild("frame").setValue("dailyframe");
	end
end
					
