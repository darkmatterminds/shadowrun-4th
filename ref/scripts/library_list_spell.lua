-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onSortCompare(w1, w2)
	local nValue1 = w1.level.getValue();
	local nValue2 = w2.level.getValue();
	if nValue1 ~= nValue2 then
		return nValue1 > nValue2;
	end

	local sValue1 = string.lower(w1.name.getValue());
	local sValue2 = string.lower(w2.name.getValue());
	if sValue1 ~= sValue2 then
		return sValue1 > sValue2;
	end

	return w1.getDatabaseNode().getNodeName() > w2.getDatabaseNode().getNodeName();
end

function onFilter(w)
	local f = getFilter(w);
	if f == "" then
		w.windowlist.window.description.setVisible(true);
		w.windowlist.window.grantedpower.setVisible(true);
		w.windowlist.window.spacer.setVisible(true);
		return true;
	end
		
	w.windowlist.window.description.setVisible(false);
	w.windowlist.window.grantedpower.setVisible(false);
	w.windowlist.window.spacer.setVisible(false);
	w.windowlist.setVisible(true);
	if string.find(string.lower(w.name.getValue()), f, 0, true) then
		return true;
	end
	
	return false;
end
