-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

local sSort = "name";

function onSortCompare(w1, w2)
	if w1[sSort] and w2[sSort] then
		local sValue1 = string.lower(w1[sSort].getValue());
		local sValue2 = string.lower(w2[sSort].getValue());
		if sValue1 ~= sValue2 then
			return sValue1 > sValue2;
		end
	end

	return w1.getDatabaseNode().getName() > w2.getDatabaseNode().getName();
end

function onFilter(w)
	if window.filter then
		local f = string.lower(window.filter.getValue());

		if f == "" then
			return true;
		end
		if string.find(string.lower(w.name.getValue()), f, 0, true) then
			return true;
		end
		
		return false;
	else
		return true;
	end
end
